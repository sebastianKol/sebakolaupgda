package pl.org.pfig.main;

import java.util.Random;

public class Rand {

	public static void main(String[] args) {
		
		Random r = new Random();
		// jezeli nie przekazujemy parametru, moze zostac zwrocona liczba ujemna
		System.out.println(r.nextInt()); 
		// jezeli przekazujemy argument, zostanie zwrocona liczba z zakresu <0, liczba)
		System.out.println(r.nextInt(15)); // <0, 15)
		
		// generowanie losowej liczby z zakesu
		// <10, 25) 25 - 10 = 15
		int a = 10;
		int b = 25;
		
		System.out.println(a + r.nextInt(b - a));
		
		char c = (char) (97 + r.nextInt(26));
		System.out.println(c + "");
	}

}
