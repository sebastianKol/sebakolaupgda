package pl.org.pfig.mainarrays;

public class TwoDArrays {
	public int addElementsDividedBySeven(int[][] arr) {
		// 1. Deklarujemy zmienna, ktora bedzie przetrzymywala sume
		//    (elementow podzielnych przez 7)
		int sum = 0;		
		// 2. Deklaruje zmienna, ktora reprezentuje ilosc wierszy w tablicy
		int rows = arr.length;
		// 3. Iteruje sie przez pierwsza tablice (zewnetrzna - jej elementami sa kolejne tablice) 
		for(int i = 0; i < rows; i++) {
			// a) deklaruje ilosc kolumn w kolejnej tablicy
			int cols = arr[i].length;
			// b) iteruje sie przez kolejny element tablicy zewnetrznej (ktory jest tablica)
			for(int j = 0; j < cols; j++) {
				// b.1) sprawdzam czy aktualnie iterowana liczba jest podzielna przez 7
				if(arr[i][j] % 7 == 0) {
					// jesli tak, dodaje ja do sumy
					sum += arr[i][j];
				}
			}
		}
		// 4. Zwracam sume
		return sum;
	}
}