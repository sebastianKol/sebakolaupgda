package student.res;


import java.util.*;

/**
 * Created by RENT on 2017-07-06.
 */
public class Main {
    private Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {
//        CityResolver cr = new CityResolver();
//        System.out.println(cr.get(1));
        Main main = new Main();
        main.menu();

    }

    private void menu() {
        StudentResolver studentResolver = new StudentResolver();

        boolean working = true;
        while (working) {
            System.out.println("1.Pokaż rekord");
            System.out.println("2.Pokaz liste rekordow");
            System.out.println("3.Dodaj rekord ");
            System.out.println("4.Usuń rekord[id=?]");
            System.out.println("5.Nadpisz rekord [id=]");
            System.out.println("6.Koniec");

            int select;
            try {
                select = scanner.nextInt();
                switch (select) {
                    case 1: // skonczone
                        System.out.println("Podaj ID ");
                        int id=scanner.nextInt();
                        System.out.println(studentResolver.get(id));

                        break;
                    case 2: // skonczone
                        System.out.println("Lista wszystkich rekordow :");
                      for (int i = 0; i< studentResolver.get().size(); i++){
                          System.out.println(studentResolver.get().get(i));
                }
                        break;
                    case 3:  // insert
                        Map<String, String> mapInsert = new HashMap<>();
                        mapInsert.put("Tomasz","Kowalski");
                        studentResolver.insert(mapInsert);

                        break;
                    case 4: //skonczone
                        System.out.println("Podaj ID ");
                        int id2=scanner.nextInt();
                        System.out.println(studentResolver.delete(id2));
                        break;
                    case 5: // update
                        Map<String, String> mapUpdate = new HashMap<>();
                        mapUpdate.put("Adam","Nowak");
                        studentResolver.update(mapUpdate);

                        break;
                    case 6: //skonczone
                        working =false;
                        break;
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }




        }
    }
}
