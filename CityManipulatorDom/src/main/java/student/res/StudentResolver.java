package student.res;

import com.sun.org.apache.bcel.internal.generic.NEW;
import connector.DatabaseConnector;
import pl.org.pfig.cities.model.City;

import java.security.KeyStore;
import java.sql.*;
import java.util.*;

/**
 * Created by RENT on 2017-07-06.
 */
public class StudentResolver extends AbstractResolver<Student> {

    @Override
    public List get() {
        List<Student> listOfStudents = new ArrayList<>();
        try {
            Connection connection = DatabaseConnector.getInstance().getConection();
            String query = "SELECT * FROM `student`";

            Statement s = connection.createStatement(); // zapyanie
            ResultSet rs = s.executeQuery(query);

            while(rs.next()){
                listOfStudents.add(new Student(rs.getInt("id"),rs.getString("name"),rs.getString("lastname")));

            }
            rs.close();
            s.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return listOfStudents;
    }

        @Override
        public Boolean delete ( int id2){
            boolean isDeleted = false;
            try {
                Connection c = DatabaseConnector.getInstance().getConection();
                Statement s = c.createStatement();
                s.execute("DELETE FROM `student` WHERE id=" + id2);
                s.close();
                isDeleted = true;
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            return isDeleted;
        }

        @Override
        public Boolean insert (Map params){

            boolean isInsert = false;
            try{
                Connection c =DatabaseConnector.getInstance().getConection();
                Statement s = c.createStatement();
                s.execute("INSERT INTO `student` VALUES("+params.hashCode()+","+params.keySet()+","+params.get(params.keySet())+")");
                s.close();
                isInsert = true;
            }catch (SQLException e){
                System.out.println(e.getMessage());
            }
            return isInsert;
        }

        @Override
        public Boolean update (Map params){
            boolean isUpdate = false;
            try{
                Connection c =DatabaseConnector.getInstance().getConection();
                Statement s = c.createStatement();
                s.execute("UPDATE `student` SET name="+params.keySet()+",lastname="+params.get(params.keySet())+" WHERE id="+params.hashCode()+")");
                s.close();
            }catch (SQLException e){
                System.out.println(e.getMessage());
            }




            return isUpdate;
        }

        @Override
        public Student get ( int id){
            Student student = null;
            try {
                Connection connection = DatabaseConnector.getInstance().getConection();
                String query = "SELECT * FROM `student` WHERE `id`=" + id;
                Statement s = connection.createStatement(); // zapyanie
                ResultSet rs = s.executeQuery(query);
                if (rs.next()) {
                    student = new Student().setId(id).setName(rs.getString("name")).setLastname(rs.getString("lastname"));
                }
                rs.close();
                s.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            return student;
        }
    }
