package pl.org.pfig.cities;

import connector.DatabaseConnector;
import pl.org.pfig.cities.model.City;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        try{
            Connection c =DatabaseConnector.getInstance().getConection();
            Statement s = c.createStatement();
            s.execute("INSERT INTO `city` VALUES(NULL,'Wronki');");
            s.close();
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }


//update worker set  age = 0 where age IS NULL;
        try{
            Connection c =DatabaseConnector.getInstance().getConection();
            Statement s = c.createStatement();
            s.execute("UPDATE `city` SET city='Szczecin' WHERE id=7");
            s.close();
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
//DELETE  from worker where age<50;
        try{
            Connection c =DatabaseConnector.getInstance().getConection();
            Statement s = c.createStatement();
            s.execute("DELETE FROM `city` WHERE city='Wronki';");
            s.close();
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }


        // inny sposob przekazania
        try{
            Connection c =DatabaseConnector.getInstance().getConection();
            String query = "UPDATE `city` SET city = ? WHERE `city` =?";
            PreparedStatement ps = c.prepareStatement(query);
                    ps.setString(1,"Sopot");
                    ps.setString(2,"Włocławek");

            int countRows = ps.executeUpdate();
            System.out.println("Nadpisalem "+countRows+" wierszy.");
            ps.close();
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }

        List<City> listOfCities = new ArrayList<>();
        try {
            Connection connection = DatabaseConnector.getInstance().getConection();
            String query = "SELECT * FROM `city`";

            Statement s = connection.createStatement(); // zapyanie
            ResultSet rs = s.executeQuery(query);

            while(rs.next()){
                listOfCities.add(new City(rs.getInt("id"),rs.getString("city")));

            }
            rs.close();
            s.close();

        }catch (SQLException e){
            System.out.println(e.getMessage());
    }
        for (City c : listOfCities){
            System.out.println(">" + c.getCity());
        }

    }
}
