package pl.sdacademy.moviedb.entity;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mori on 2017-07-12.
 */
@Entity
@Table(name="movie")
public class Movie {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY )
    @Column(name="id")
    private int id;
    @Column(name = "tilte")
    private String title;
    @Column(name = "year")
    private  int year;
    @Column(name = "duration")
    private double duration;
    @Column(name = "description")
    String description;
//    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    private List<Genre> genres = new LinkedList<Genre>();

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Genre genre;


    public Movie(String title, int year, double duration, String description, Genre genre) {
        this.title = title;
        this.year = year;
        this.duration = duration;
        this.description = description;
        this.genre = genre;
    }

    public Movie() {
    }

    public int getId() {
        return id;
    }

    public Movie setId(int id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Movie setTitle(String title) {
        this.title = title;
        return this;
    }

    public int getYear() {
        return year;
    }

    public Movie setYear(int year) {
        this.year = year;
        return this;
    }

    public double getDuration() {
        return duration;
    }

    public Movie setDuration(double duration) {
        this.duration = duration;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Movie setDescription(String description) {
        this.description = description;
        return this;
    }

    public Genre getGenre() {
        return genre;
    }

    public Movie setGenre(Genre genre) {
        this.genre = genre;
        return this;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", year=" + year +
                ", duration=" + duration +
                ", description='" + description + '\'' +
                ", genre=" + genre +
                '}';
    }
}
