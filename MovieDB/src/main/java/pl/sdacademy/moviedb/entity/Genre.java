package pl.sdacademy.moviedb.entity;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-07-13.
 */
@Entity
@Table(name="genre")
public class Genre {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name ="id")
    private int id;
    @Column(name = "name")
    private String name;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "genre")
    private List<Movie> movies = new LinkedList<Movie>();
//
//    OneToMany: LAZY
//    ManyToOne: EAGER
//    ManyToMany: LAZY
//    OneToOne: EAGER

    public Genre() {
    }

    public Genre(String name) {
        this.name = name;
    }

    public Genre(String name, List<Movie> movies) {
        this.name = name;
        this.movies = movies;
    }

    public int getId() {
        return id;
    }

    public Genre setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Genre setName(String name) {
        this.name = name;
        return this;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public Genre setMovies(List<Movie> movies) {
        this.movies = movies;
        return this;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", movies=" + movies +
                '}';
    }
}
