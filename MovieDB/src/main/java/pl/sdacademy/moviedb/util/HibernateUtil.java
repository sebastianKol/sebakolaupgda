package pl.sdacademy.moviedb.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by RENT on 2017-07-12.
 */
public class HibernateUtil {
    private static SessionFactory instance = null;
    protected HibernateUtil() {
    }
    public static Session openSession() {
        if(instance == null) {
            instance = new Configuration().configure().buildSessionFactory();
        }
        return instance.openSession();
    }
}
//public class HibernateUtil {
//    private static final SessionFactory sessionFactory = buildSessionFactory();  // nie nadpiszemy tego pola
//    private static SessionFactory buildSessionFactory(){   // singleton wykozystany w inny sposob
//        return new Configuration().configure().buildSessionFactory();
//    }
//    public static SessionFactory getSessionFactory(){
//        return sessionFactory;
//    }
//    public static Session openSession(){
//        return  getSessionFactory().openSession();
//    }
//}