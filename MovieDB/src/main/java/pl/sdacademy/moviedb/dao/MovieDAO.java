package pl.sdacademy.moviedb.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sdacademy.moviedb.entity.Movie;
import pl.sdacademy.moviedb.util.HibernateUtil;

import java.util.List;

/**
 * Created by Mori on 2017-07-12.
 */
public class MovieDAO implements AbstractDAO {
    public boolean insert(Movie type) {

        Session session = (Session) HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.save(type);
        t.commit();
        session.close();

        return true;
    }

    public boolean insert(Object type) {
        return false;
    }

    public boolean delete(Object type) {
        return false;
    }

    public boolean delete(int id) {
        return false;
    }

    public boolean update(Object type) {
        return false;
    }

    public Object get(int id) {
        return null;
    }

    public List<Movie> get() {
        Session session = HibernateUtil.openSession();
        List<Movie> movies;
        movies = session.createQuery("from Movie ").list();
        session.close();
        return movies;
    }
}
