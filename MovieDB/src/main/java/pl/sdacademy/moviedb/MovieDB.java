package pl.sdacademy.moviedb;

import pl.sdacademy.moviedb.crud.SimpleCRUD;
import pl.sdacademy.moviedb.dao.MovieDAO;
import pl.sdacademy.moviedb.entity.Genre;
import pl.sdacademy.moviedb.entity.Movie;

import java.util.List;
import java.util.Scanner;

/**
 * Created by Mori on 2017-07-12.
 */
public class MovieDB {
    public void start() {

//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Podaj tytul fiilmu");
//        String title = scanner.nextLine();
//
//        System.out.println("Podaj opis");
//        String description = scanner.nextLine();
//
//        System.out.println("Podaj gatunek filmu");
//        String genre = scanner.nextLine();
//
//        // skaner mozna mieszac ale trzeba parsowac na inta lub doubla
//
//        System.out.println("Podaj rok produkcji");
//        int year = scanner.nextInt();
//        System.out.println("Podaj czas trwania");
//
//        double duration = scanner.nextDouble();
//
//
//        MovieDAO movieDAO = new MovieDAO();
//
//        movieDAO.insert(new Movie(title,year,duration,description,genre));

        // sprawdzic czy jest obiekt juz w tabeli dopisac
        // dodac mozliwosc dodawania tylko gatunkow


        SimpleCRUD<Movie> cm = new SimpleCRUD(Movie.class);

        Genre g = new Genre("Komedia");

        // zapis
        Movie m = new Movie();
        m.setDescription("super filmik");
        m.setDuration(180);
        m.setTitle("Pierwszy film");
        m.setYear(2000);
        m.setGenre(g);

        cm.insertOrUpdate(m);
        cm.insertOrUpdate(new Movie("Drugi film", 2002, 190, "Opis drugiego filmu", g));


        // odczyt
        Movie mv = cm.select(1);
        System.out.println(mv.getTitle() + " (" + mv.getYear() + "), gatunek: " + mv.getGenre().getName());

        SimpleCRUD<Genre> sg = new SimpleCRUD(Genre.class);


        Genre newG = sg.select(1);

        System.out.println("aaa -- " + newG.getMovies().size());
        for (Movie newM : newG.getMovies()) {
            System.out.println("-- " + newM.getTitle());
        }

        System.exit(0);

    }

}



