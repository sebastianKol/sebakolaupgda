package pl.sdacademy.moviedb.crud;

import java.util.List;

/**
 * Created by RENT on 2017-07-13.
 */
public interface Crud<T> {
    void insertOrUpdate(T type);
    void delete (T type);
    T select (int id);
    List<T> select ();
}
