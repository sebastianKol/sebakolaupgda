package pl.sdacademy.moviedb.crud;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sdacademy.moviedb.entity.Movie;
import pl.sdacademy.moviedb.util.HibernateUtil;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-07-13.
 */
public class SimpleCRUD<T> implements Crud<T> {
    private Class<T> clazz;

    public SimpleCRUD(Class<T> clazz) {
        this.clazz = clazz;
    }

    public void insertOrUpdate(Object type) {  // dziala
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.saveOrUpdate(type);
        t.commit();
        session.close();
    }

    public void delete(Object type) {  // do sprawdzenia
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(type);
        t.commit();
    }

    public T select(int id) {  // pablo
        Session session = HibernateUtil.openSession();
        T obj = (T) session.get(clazz, id);
        session.close();
        return obj;
    }



    public List<T> select() {  // pablo
        List<T> ret;
        Session session = HibernateUtil.openSession();
        Transaction tx = session.beginTransaction();
        System.out.println("\t" + clazz.getSimpleName());
        ret = session.createQuery("from " + clazz.getSimpleName()).list();
        tx.commit();
        session.close();
        return ret;
    }
}
