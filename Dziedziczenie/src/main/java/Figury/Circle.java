package Figury;
public class Circle implements Figure {
	
	private double r;
	public Circle(double r) {
		super();
		this.r = r;
	}

	
	
	
	
	@Override
	public Double countArea() {
	
	return Math.PI*r*r;
	
	}
	@Override
	public Double countCircumference(){
	return Math.PI*2*r;
	}
	

}
