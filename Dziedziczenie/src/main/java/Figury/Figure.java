package Figury;

public interface Figure {

	
	Double countArea();
	Double countCircumference();

}
