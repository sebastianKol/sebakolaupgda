package Figury;
public class Square implements Figure {
	
	private double a;
	public Square(double a) {
		super();
		this.a = a;
	}
	
	
	@Override
	public Double countArea() {
	return a*a;
	
	
	}
	@Override
	public Double countCircumference(){
		return 4*a;
	}
	
	

}





//
//a.Square (double a)
//b.Circle (double r)
////c.*Triangle (double a, double b, double c)
//
// ka�dej klasy dodaj metod� countArea i countCircumference.
//
//W klasie FiguresTest przetestuj ich dzia�anie
//
//Dodaj interfejs Figure z metodami countArea i countCircumference. Zaimplementuj ten interfejs w utworzonych klasach i u�yj w klasie FiguresTest.

