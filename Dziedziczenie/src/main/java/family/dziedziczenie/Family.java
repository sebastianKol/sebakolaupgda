package family.dziedziczenie;

public class Family {

	public static void main(String[] args) {
		
		FamilyMember mother = new Mother("Gra�yna");
		FamilyMember father = new Father("Janusz");
		FamilyMember daugther = new Daugther("Ania");
		FamilyMember son = new Son("Adam");
		
		FamilyMember[] members = {mother,father,daugther,son};
		for (FamilyMember familyMember : members) {
			familyMember.introduce();
			
		}

	}


}