package family.dziedziczenie;

public abstract class FamilyMember {
	private String name;  
	
	
	public FamilyMember(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public  abstract void introduce();   // przeslanianie metod
		
		// to co bedzie w tej metodzie bedzie zaloniete
	
}