package animals.dziedziczenie;

public class Delfin extends ZwierzetaGrupa {
	
	public Delfin(String name, int age, int legs) {
		super(name,age,legs);
	}
	public Delfin(String name, int age) {
		super(name,age,0);
	}
	
	public Delfin(String name) {
		super(name, 0,0);
	}
	
	@Override
	public void introduce() {
		System.out.println("Im a Delfin. My name is " + this.getName());
	}
	@Override
	public void sayAge() {
		if(this.getAge()>0){
		System.out.println("Im: " + this.getAge());
		}
	}
	@Override
	public void sayLegs() {
		if(this.getLegs()>0){
		System.out.println("Ive: " + this.getLegs()+"legs");
		}
	}
}
