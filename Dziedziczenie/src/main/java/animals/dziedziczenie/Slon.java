package animals.dziedziczenie;

public class Slon extends ZwierzetaGrupa {
	
	public Slon(String name, int age, int legs) {
		super(name,age,legs);
	}
	public Slon(String name, int age) {
		super(name,age,0);
	}
	
	public Slon(String name) {
		super(name, 0,0);
	}
	
	@Override
	public void introduce() {
		System.out.println("Im a Slon. My name is " + this.getName());
	}
	@Override
	public void sayAge() {
		if(this.getAge()>0){
		System.out.println("Im: " + this.getAge());
		}
	}
	@Override
	public void sayLegs() {
		if(this.getLegs()>0){
		System.out.println("Ive: " + this.getLegs()+"legs");
		}
	}
}
