package animals.dziedziczenie;

public abstract class ZwierzetaGrupa {
	private String name;
	private int age;
	private int legs;
	
	public ZwierzetaGrupa(String name) {
		this(name, 0,0);
	}
	public ZwierzetaGrupa(String name, int age) {
		this(name,age,0);
	}
	
	public ZwierzetaGrupa(String name, int age, int legs){
		this.name=name;
		this.age=age;
		this.legs=legs;
	}
	public String getName() {
		return name;
	}
	public int getAge(){
		return age;
	}
	public int getLegs(){
		return legs;
		
	}
	public abstract void introduce();
	public abstract void sayAge();
	public abstract void sayLegs();
}
