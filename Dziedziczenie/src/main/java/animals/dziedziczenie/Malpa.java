package animals.dziedziczenie;

public class Malpa extends ZwierzetaGrupa {
	
	public Malpa(String name, int age, int legs) {
		super(name,age,legs);
	}
	public Malpa(String name, int age) {
		super(name,age,0);
	}
	
	public Malpa(String name) {
		super(name, 0,0);
	}
	
	@Override
	public void introduce() {
		System.out.println("Im a Malpa. My name is " + this.getName());
	}
	@Override
	public void sayAge() {
		if(this.getAge()>0){
		System.out.println("Im: " + this.getAge());
		}
	}
	@Override
	public void sayLegs() {
		if(this.getLegs()>0){
		System.out.println("Ive: " + this.getLegs()+"legs");
		}
	}
}
