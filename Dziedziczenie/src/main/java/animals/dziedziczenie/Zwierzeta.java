package animals.dziedziczenie;

import animals.dziedziczenie.ZwierzetaGrupa;

public class Zwierzeta {

	public static void main(String[] args) {
		ZwierzetaGrupa delfin = new Delfin("Nemo",15,0);
		ZwierzetaGrupa kot = new Kot("Mruczek",0,4);
		ZwierzetaGrupa malpa = new Malpa("Tarzan",10,2);
		ZwierzetaGrupa pies = new Pies("Burek",2);
		ZwierzetaGrupa slon = new Slon("Bali");

		ZwierzetaGrupa[] members = {delfin,kot,malpa,pies,slon};
		
		for (ZwierzetaGrupa zwierzetagrupa : members) {
			zwierzetagrupa.introduce();
			zwierzetagrupa.sayAge();
			zwierzetagrupa.sayLegs();
			System.out.println("");

	}
	}
}
