package students;

import java.util.LinkedList;
import java.util.List;

public class FileTest {
	
	public static void main(String[] args) {
		
		List<Student> students = new LinkedList<>();
		University u = new University();
		u.addStudent(100, "Jan", "Kol");
		u.addStudent(101, "Stas", "Lop");
		u.addStudent(102, "Ola", "Gol");
		
		TextFile textFile = new TextFile();
		
		textFile.save(u.getStudentsList());
		
		List<Student> studentsFromFile = textFile.load();
		University universityFromFile = new University(studentsFromFile);
		
		universityFromFile.showAll();
		
	}
}
