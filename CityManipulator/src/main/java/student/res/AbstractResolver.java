package student.res;

import java.util.List;
import java.util.Map;

/**
 * Created by RENT on 2017-07-06.
 */
public  abstract class AbstractResolver <T> {

    abstract public T get(int id);
    abstract public List<T> get();
    abstract public Boolean delete(int id2);
    abstract public Boolean insert(Map<String, String>params);
    abstract public Boolean update(int id,Map<String, String>params);


}
