package student.res;

import connector.DatabaseConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by RENT on 2017-07-06.
 */
public class StudentResolver extends AbstractResolver<Student> {

    @Override
    public List get() {
        List<Student> listOfCStudents = new ArrayList<>();
        Student student = null;
        try {
            Connection connection = DatabaseConnector.getInstance().getConection();
            String query = "SELECT * FROM `student`";

            Statement s = connection.createStatement(); // zapyanie
            ResultSet rs = s.executeQuery(query);
            int id = 0;
            while(rs.next()){

                int idGet = Integer.parseInt(rs.getString("id"));
                String name2 = rs.getString("name");
                String lastname2 = rs.getString("lastname");

                String fixname = name2.substring(1, name2.length()-1);
                String fixlastname = lastname2.substring(1, lastname2.length()-1);

                student = new Student(id,fixname,fixlastname);
                student.setId(idGet);
                student.setName(fixname);
                student.setLastname(fixlastname);

                listOfCStudents.add(id,student);
            id++;}
            rs.close();
            s.close();

        }catch (SQLException e){
            System.out.println(e.getMessage());
        }

        return listOfCStudents;
    }

    @Override
    public Boolean delete(int id2) {

        boolean isDeleted = false;
        try {
            isDeleted = true;
            Connection c = DatabaseConnector.getInstance().getConection();
            Statement s = c.createStatement();
            s.execute("DELETE FROM `student` WHERE id=" + id2);
            s.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return isDeleted;
    }
    @Override
    public Boolean insert(Map params) {  //fixname = subquery.substring(0, subquery.length() - 1);
        boolean isInsert = false;
        try{
            isInsert = true;
            Connection c =DatabaseConnector.getInstance().getConection();
            Statement s = c.createStatement();
            String name = params.keySet().toString();
            String fixname = name.substring(1, name.length()-1);
            String lastname = params.values().toString();
            String fixlastname = lastname.substring(1, lastname.length()-1);

            s.execute("INSERT INTO `student` VALUES(null"+",'"+fixname+"','"+fixlastname+"')");

            s.close();
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return isInsert;

    }

    @Override
    public Boolean update(int id, Map params) {   //Update worker set  age = 0 where age IS NULL;
        boolean isUpdate = false;
        try{
            isUpdate = true;
            Connection c =DatabaseConnector.getInstance().getConection();
            Statement s = c.createStatement();
            String name = params.keySet().toString();
            String fixname = name.substring(1, name.length()-1);
            String lastname = params.values().toString();
            String fixlastname = lastname.substring(1, lastname.length()-1);
            s.execute("UPDATE `student` SET name='"+fixname+"' WHERE id="+id );
            s.execute("UPDATE `student` SET lastname='"+fixlastname+"' WHERE id="+id );
            s.close();
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return isUpdate;

    }

    @Override
    public Student get(int id) {
        Student student = null;
        try {
            Connection connection = DatabaseConnector.getInstance().getConection();
            String query = "SELECT * FROM `student` WHERE `id`=" + id;
            Statement s = connection.createStatement(); // zapyanie
            ResultSet rs = s.executeQuery(query);
            if(rs.next()) {

                String name1 = rs.getString("name");
                String lastname1 = rs.getString("lastname");
                String fixname = name1.substring(1, name1.length()-1);
                String fixlastname = lastname1.substring(1, lastname1.length()-1);
                student = new Student(id,fixname,fixlastname);
                student.setId(id);
                student.setName(fixname);
                student.setLastname(fixlastname);
            }
            rs.close();
            s.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return student;
    }
}