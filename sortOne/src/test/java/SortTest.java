

import org.assertj.core.api.Assertions;
import org.junit.Test;


import java.util.Random;
import java.util.function.Consumer;

import static org.junit.Assert.*;

/**
 * Created by RENT on 2017-06-27.
 */
public class SortTest {

    interface  SortMethod{
        void sort(int[]data);
    }

    public  void  testSort(SortMethod sortMethod) {   // sposob na przekazanie metody jako parametru
        int[] data = {1, 7, 2, 4, 3, 8, 9, 6};                      // dowolny interfrjs z 1 metoda
        sortMethod.sort(data);
        Assertions.assertThat(data).isSorted();
    }


//    public  void  testSort(Consumer<int[]> sortMethod){   // sposob na przekazanie metody jako parametru
//        int[] data ={1,7,2,4,3,8,9,6};                      // z interfejsem generyczny
//        sortMethod.accept(data);
//        Assertions.assertThat(data).isSorted();
//
//    }
//
//
//    @Test                                             //podstawowy test sortowania
//    public void sort() throws Exception {
//        int[] data ={1,7,2,4,3,8,9,6};
//
//        Sort.sort(data);
//        Assertions.assertThat(data).isSorted();
//
    @Test
    public void sort() throws Exception {
    testSort(Sort::sort);
}

    @Test
    public void sortZegarski() throws Exception {
        testSort(Sort::sortZegarski);
    }

    @Test
    public void sortBodzio() throws Exception {
        testSort(Sort::sortBodzio);
    }


    @Test
    public void sortBFN() throws Exception {
        testSort(Sort::sortBFN);
    }

    @Test
    public void sortBySmall() throws Exception {
        testSort(Sort::sortBySmall);
    }

    @Test
    public void mergeSort() throws Exception {
        testSort(Sort::mergeSort);
    }


    public void speedTest(SortMethod sortMethod, int[] data) {
        System.out.println("Test of " + data.length + " elements.");
        long start = System.currentTimeMillis();
        sortMethod.sort(data);
        long end = System.currentTimeMillis();
        long time = end - start;
        System.out.println(time/1000 + "," + time%1000 + " sec.");
        Assertions.assertThat(data).isSorted();
    }

    public void speedTest(SortMethod sortMethod, String name) {
        System.out.println("Test method " + name);
        speedTest(sortMethod, generateData(8*1024));
        speedTest(sortMethod, generateData(14*8*1024));
    }

    private int[] generateData(int size) {
        Random random = new Random();
        int[] data = new int[size];
        for (int i = 0 ; i < size ;i++) {
            data[i] = random.nextInt();
        }
        return data;
    }

    @Test
    public void sortZegarskiSpeedTest() throws Exception {
        speedTest(Sort::sortZegarski, "sortZegarski");
        System.out.println("");
    }
    @Test
    public void sortSpeedTest() throws Exception {
        speedTest(Sort::sort, "sort");
        System.out.println("");
    }
    @Test
    public void sortBodzioSpeedTest() throws Exception {
        speedTest(Sort::sortBodzio, "sortBodzio");
        System.out.println("");
    }
    @Test
    public void sortBFNSpeedTest() throws Exception {
        speedTest(Sort::sortBFN, "sortBFN");
        System.out.println("");
    }
    @Test
    public void bubbleSortSpeedTest() throws Exception {
        speedTest(Sort::bubbleSort, "bubbleSort");
        System.out.println("");
    }

    @Test
    public void sortWstawSpeedTest() throws Exception {
        speedTest(Sort::sortWstaw, "sortWstaw");
        System.out.println("");
    }
    @Test
    public void selectionSortSpeedTest() throws Exception {
        speedTest(Sort::sortBySmall, "sortBySmall");
        System.out.println("");
    }
    @Test
    public void mergeSortSpeedTest() throws Exception {
        speedTest(Sort::mergeSort, "mergeSort");
        System.out.println("");
    }








    @Test
    public void testMerge() {
        int[] left = {1,4,8};
        int[] right = {2,3,5};
        int[] result = new int[6];
        Sort.merge(left, right, result);
        Assertions.assertThat(result).containsExactly(1,2,3,4,5,8);
    }
    @Test
    public void copyTest(){
        int[] data = {1,22,12,31};
        int[] result = Sort.copy(1,2, data);
        Assertions.assertThat(result).containsExactly(22,12);
    }



}