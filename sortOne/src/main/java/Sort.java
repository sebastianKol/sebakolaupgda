/**
 * Created by RENT on 2017-06-27.
 */
public class Sort {

    public static void sort(int[] data) {
        int buffor;
        int step = 1;

        while (step > 0) {
            step = 0;
            for (int i = 0; i < data.length - 1; i++) {
                if (data[i] > data[i + 1]) {
                    buffor = data[i + 1];
                    data[i + 1] = data[i];
                    data[i] = buffor;
                    step++;

                }
            }
        }
    }

    public static void sortZegarski(int[] tab) {
        int temp;
        boolean isTabSorted = false;

        do {
            isTabSorted = true;
            for (int i = 0; i < tab.length - 1; i++) {
                if (tab[i] > tab[i + 1]) {
                    temp = tab[i];
                    tab[i] = tab[i + 1];
                    tab[i + 1] = temp;
                    isTabSorted = false;
                }
            }
        } while (!isTabSorted);
    }

    public static void sortBodzio(int[] data) {
        int size = data.length;
        int count = 1;
        while (count > 0) {
            count = 0;
            size--;
            for (int i = 0; i < size; i++) {
                int left = data[i];
                int right = data[i + 1];
                if (left > right) {
                    data[i] = right;
                    data[i + 1] = left;
                    count++;
                }
            }
        }
    }

    public static void sortBFN(int[] tablica) {
        int n = tablica.length - 1;

        while (n > 0) {
            int last = 0;
            for (int j = 0; j < n; j++) {
                if (tablica[j] > tablica[j + 1]) {
                    swap(tablica, j, j + 1);
                    last = j;
                }
            }
            n = last;
        }
    }

    private static void swap(int[] tablica, int i, int j) {
        int temp = tablica[i];
        tablica[i] = tablica[j];
        tablica[j] = temp;
    }


    public static void bubbleSort(int[] tab) {
        for (int i = 0; i < tab.length; i++) {
            for (int j = tab.length - 1; j > i; j--) {
                int left = tab[j - 1];
                int right = tab[j];
                if (left > right) {
                    tab[j - 1] = right;
                    tab[j] = left;

                }
            }
        }
    }

    public static void sortWstaw(int[] tab) {

            for (int i = 0 ; i < tab.length ; i++) {
                int currentElement = tab[i];
                int position = 0;
                while (tab[position] < currentElement && position <= i) {
                    position++;
                }

                for (int j = i; j > position ; j--) {
                    tab[j] = tab[j-1];
                }

                tab[position] = currentElement;
            }

    }
    public static void sortBySmall(int[] tab) {
        for (int i = 0; i < tab.length; i++) {

            int min = tab[i];
            int minIndex = i;
            for (int j = i + 1; j < tab.length; j++) {
                if (tab[j] < min) {
                    min = tab[j];
                    minIndex = j;
                }
            }

            int buffer = tab[i];
            tab[i] = min;
            tab[minIndex] = buffer;


        }
    }






    public static void mergeSort(int[] data) {
        if (data.length > 1) {
            int middle = data.length / 2;
            int[] left = copy(0, middle-1, data);
            int[] right = copy(middle, data.length-1, data);
            mergeSort(left);
            mergeSort(right);
            merge(left,right,data);
        }
    }
    public static void merge(int[] left,int[]right, int[]tab) {

        int a = 0;
        int b = 0;
        int i = 0;


        while (a < left.length && b < right.length) {
            if(left[a]<right[b]){
                tab[i]=left[a];
                a++;
            }else{
                tab[i]=right[b];
                b++;
            }
            i++;

        }
        while (a < left.length) {
            tab[i] = left[a];
            a++;
            i++;
        }
        while (b < right.length) {
            tab[i] = right[b];
            b++;
            i++;
        }
    }







    public static int[] copy(int start, int end, int[] data) {
        int j = 0;
        int[] copy = new int[end - start + 1];
        for (int i = start ; i <= end ; i++) {
            copy[j++] = data[i];
        }
        return copy;
    }
    }

