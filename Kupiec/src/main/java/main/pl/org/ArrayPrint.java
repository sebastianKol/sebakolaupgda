package main.pl.org;

/**
 * Created by RENT on 2017-07-03.
 */
public class ArrayPrint {
    public static void printArray(Object[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println((i + 1) + ". " + array[i]);
        }
    }

}
