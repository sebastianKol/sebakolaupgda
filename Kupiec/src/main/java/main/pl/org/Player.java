package main.pl.org;

/**
 * Created by Mori on 2017-06-29.
 */
public class Player {
    private String name;
    private double money;
    private int goodInPocket;

    public Player(String name, double money, int goodInPocket) {
        this.name = name;
        this.money = money;
        this.goodInPocket = goodInPocket;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public int getGoodInPocket() {
        return goodInPocket;
    }

    public void setGoodInPocket(int goodInPocket) {
        this.goodInPocket = goodInPocket;
    }

}
