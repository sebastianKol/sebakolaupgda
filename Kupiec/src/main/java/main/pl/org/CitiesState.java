package main.pl.org;

/**
 * Created by Mori on 2017-06-29.
 */
public class CitiesState {
    private  Cities cityName;
    private  double tradeValueinCity;

    public CitiesState(Cities cityName, int tradeValueinCity) {
        this.cityName = cityName;
        this.tradeValueinCity = tradeValueinCity;
    }

    public Cities getCityName() {
        return cityName;
    }

    public void setCityName(Cities cityName) {
        this.cityName = cityName;
    }

    public double getTradeValueinCity() {
        return tradeValueinCity;
    }

    public void setTradeValueinCity(int tradeValueinCity) {
        this.tradeValueinCity = tradeValueinCity;
    }

    @Override
    public String toString() {
        return "CitiesProfile{" +
                "cityName=" + cityName +
                ", tradeValueinCity=" + tradeValueinCity +
                '}';
    }
}
