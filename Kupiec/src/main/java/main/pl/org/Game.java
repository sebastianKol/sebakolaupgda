package main.pl.org;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Game {

    //  public String cityProfile;
    private List<CitiesState> profileList = new LinkedList<>();
    private CitiesState currentCityProfile = null;
    private Player currentPlayer = null;

    boolean record = false;


//    public String getCityProfile() {
//        return cityProfile;
//    }
//
//    public void setCityProfile(String profile) {
//        this.cityProfile = profile;
//    }

    //metody
    public void Game() {
        boolean working = true;
        while (working) {

            double tradeValueFixed = getTradeValueFixed();
            double maxBuy = currentPlayer.getMoney() / tradeValueFixed;

            showMenu(tradeValueFixed);
            Scanner scanner = new Scanner(System.in);
            int select = scanner.nextInt();
            switch (select) {
                case 0:
                    working = false;
                    break;
                case 1:

                    buyGoods(maxBuy, tradeValueFixed);
                    break;
                case 2:

                    sellGoods(tradeValueFixed);
                    break;
                case 3:

                    travelToOtherCity();
                    break;

            }
        }
    }

    private void showMenu(double tradeValueFixed) {
        System.out.println("");
        System.out.println("0 - Wyjscie do menu");
        System.out.println("Witaj :" + currentPlayer.getName());
        System.out.println("Złoto : " + currentPlayer.getMoney());
        System.out.println("Towar :" + currentPlayer.getGoodInPocket());
        System.out.println("Obecnie znajdujesz sie w :" + currentCityProfile.getCityName());
        System.out.println("1 Sztuka Towaru kosztuje :" + tradeValueFixed);
        System.out.println("1. Kupuje");
        System.out.println("2. Sprzedaje");
        System.out.println("3. Wyjedz do innego miasta");
    }

    private void sellGoods(double tradeValueFixed) { /// poprawic
        double sellMax = currentPlayer.getGoodInPocket();
        System.out.println("Ile Towaru chcesz sprzedac.");
        System.out.println("Posiadasz : " + currentPlayer.getGoodInPocket() + " sztuk");
        System.out.println("Mozesz sprzedac max: " + sellMax);
        Scanner scanner = new Scanner(System.in);
        int sell = scanner.nextInt();

        if (sell <= sellMax) {
            double recivedMoney = sell * tradeValueFixed;
            currentPlayer.setGoodInPocket(currentPlayer.getGoodInPocket() - sell); // odejmuje towary
            currentPlayer.setMoney(currentPlayer.getMoney() + recivedMoney);
        } else if (sell > sellMax) {
            System.out.println("Nie masz tyle Towaru !!");
        }
    }

    private void buyGoods(double maxBuy, double tradeValueFixed) {  // porawic
        System.out.println("Ile Towaru chcesz kupic.");
        System.out.println("Posiadasz :" + currentPlayer.getMoney() + "zlota");
        System.out.println("Mozesz kupic max: " + (int) (maxBuy));
        Scanner scanner = new Scanner(System.in);
        int buy = scanner.nextInt();
        if (buy <= maxBuy) {
            double toPay = buy * tradeValueFixed;
            currentPlayer.setGoodInPocket(currentPlayer.getGoodInPocket() + buy); // dodaje towary
            currentPlayer.setMoney(currentPlayer.getMoney() - toPay);
        } else if (buy > maxBuy) {
            System.out.println("Nie masz tyle Zlota !!");
        }

    }

    private double getTradeValueFixed() {
        int extraRandom4CityTrade = 0;
        Random random = new Random();
        int plusORMinus = random.nextInt((1 - 0) + 1) + 0;
        if (plusORMinus == 0) {
            Random r = new Random();
            int randomNum = r.nextInt((20 - 0) + 1) + 0;
            extraRandom4CityTrade = randomNum;
        } else if (plusORMinus == 1) {
            Random r = new Random();
            int randomNum = r.nextInt((20 - 0) + 1) + 0;
            extraRandom4CityTrade = -randomNum;
        }

        return extraRandom4CityTrade + currentCityProfile.getTradeValueinCity();
    }

    private void travelToOtherCity() {
        boolean working = true;
        while (working) {
            int ticketPrice = (int) getTradeValueFixed() / 2;
            currentPlayer.setMoney(currentPlayer.getMoney() - ticketPrice);
            if (currentPlayer.getMoney() <= 0) {
                System.out.println("Jestes Bankrutem, nie stac cie nawet na bilet !!");
                System.out.println("GAME OVER !!!");
                break;
            } else if (currentPlayer.getMoney() > 0) {
                System.out.println("");
                System.out.println("Bilet kosztuje " + ticketPrice);
                System.out.println("Jestes na rozstaju drog gdze chcesz sie udac ?");
                ArrayPrint.printArray(Cities.values());
                Scanner scanner = new Scanner(System.in);
                int select = scanner.nextInt();
                currentCityProfile = profileList.get(select - 1);
            }
        }
    }

    public void startCity(CitiesState cityprofile) {   // utworzenie miasta startowego w menu nowa gra
        currentCityProfile = cityprofile;
    }

    public void startPlayer(Player player) {
        currentPlayer = player;
    }

    public void addCitiesToList(CitiesState citiesState) {
        profileList.add(citiesState);
    }


    // zakup i sprzezaz towarow nie moe przekraczac wartosci mxymalnej ilosci zlota
    // zapis i odczyt z pliku

    public void loadGameFromFile() throws FileNotFoundException {
        String loadedPlayerProfile;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj imie gracza ");
        String loadedName = scanner.nextLine();
        String lodedFile = loadedName + ".kupiec.txt";


        File f = new File(lodedFile);

        try {
            Scanner sc = new Scanner(f);
            loadedPlayerProfile = sc.next().toString();

            String[] parts = loadedPlayerProfile.split("_");
            System.out.println(parts[0]);
            System.out.println(parts[1]);
            System.out.println(parts[2]);

            Player loadedPlayer = new Player(parts[0], Double.parseDouble(parts[1]), Integer.parseInt(parts[2]));
            currentPlayer = loadedPlayer;
            Game();

        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        }


    }// wczytuje stan gry / z pliku

    public void saveGameToFile() {
        String filename;
        String c = currentPlayer.getName().toString();
        filename = c + ".kupiec.txt";
        File f = new File(filename);

        try {
            FileOutputStream fos = new FileOutputStream(f);
            PrintWriter pw = new PrintWriter(fos);

            pw.println(currentPlayer.getName() + "_" + currentPlayer.getMoney() + "_" + currentPlayer.getGoodInPocket());
            pw.close();

        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        }

    } //zapisuje current profile do pliku


}
