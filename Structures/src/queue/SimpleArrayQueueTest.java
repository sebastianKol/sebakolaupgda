package queue;

import org.junit.Test;

import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;


public class SimpleArrayQueueTest {

    SimpleQueue underTest = new SimpleQueueK();

    @Test
    public void offerAnPoll3Elements() {
        underTest.offer(2);
        underTest.offer(4);
        underTest.offer(6);

        assertThat(underTest.isEmpty()).isFalse();
        assertThat(underTest.poll()).isEqualTo(2);
        assertThat(underTest.poll()).isEqualTo(4);
        assertThat(underTest.poll()).isEqualTo(6);
        assertThat(underTest.isEmpty()).isTrue();
    }

    @Test
    public void offerPeekAndPool2Times() {
        underTest.offer(2);
        underTest.peek();
        underTest.poll();
        underTest.offer(6);

        assertThat(underTest.peek()).isEqualTo(6);
        assertThat(underTest.poll()).isEqualTo(6);
        assertThat(underTest.isEmpty()).isTrue();
    }

    @Test
    public void offerAnPoll100Elements() {
        for (int i = 0; i < 100; i++) {
            underTest.offer(i);
        }

        for (int i = 0; i < 100; i++) {
            assertThat(underTest.poll()).isEqualTo(i);
        }
    }
    @Test(expected = NoSuchElementException.class)
    public void pollFromEmptyqueue(){
        underTest = new SimpleQueueK();
        underTest.poll();
    }
    @Test
    public void peekFromEmptyqueue(){
        underTest = new SimpleQueueK();
        assertThat(underTest.peek()).isEqualTo(null);
    }
}