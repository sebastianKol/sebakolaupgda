package queue;

/**
 * Created by RENT on 2017-06-27.
 */
public interface SimpleQueue {

    boolean isEmpty();
    void offer(int value);
    int poll();
    int peek();

}
