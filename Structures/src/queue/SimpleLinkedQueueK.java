package queue;

import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */
public class SimpleLinkedQueueK implements SimpleQueue {

    private Element first;
    private Element last;

    private static class Element{
        int value;
        Element next;
        Element previous;

        public Element(int value) {
            this.value = value;
        }
    }

    @Override
    public boolean isEmpty() {
        return first==null;
    }

    @Override
    public void offer(int value) {
        Element element = new Element(value);
        if (isEmpty()){
            first = element;
        }else {
            last.next = element;
        }
        last=element;
    }

    @Override  // usowanie
    public int poll() {

        if (isEmpty()){
            throw new NoSuchElementException("pool");
        }else if(first==last){
            int removed = peek();
            first=null;
            last=null;
            return removed;
        }else{
            int removed = peek();
            first=first.next;
            return removed;
        }
    }

    @Override // podglad
    public int peek() {
        if (isEmpty()) {
            throw new NoSuchElementException("peek");
        } else {
            return first.value;
        }
    }

}
