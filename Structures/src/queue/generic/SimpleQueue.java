package queue.generic;

/**
 * Created by RENT on 2017-06-27.
 */
public interface SimpleQueue<T> {
    boolean isEmpty(); // sprawdza, czy kolejka jest pusta
    void offer(T value); // dodaje element do kolejki
    T poll(); // zwraca i usuwa element z kolejki
    T peek(); // zwraca element z kolejki
}