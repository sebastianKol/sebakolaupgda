package queue;

import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */
public class SimpleQueueK implements SimpleQueue {

    private int [] data = new int[8];
    private int start =0;
    private int end = 0;
    private  boolean isEmpty = true;

    @Override
    public boolean isEmpty() {
        return isEmpty;
    }

    @Override
    public void offer(int value) { /// poprawic
        if (isFull()) {

            createNewArray();
        }

        data[end] = value;
        end = (end + 1) % data.length;
        isEmpty = false;
    }

    private void createNewArray() {
        int[] newData = new int[data.length*2];
        int i = 0;
        while (!isEmpty) {
            newData[i] = poll();
            i++;
        }

        data = newData;
        start = 0;
        end = i;
    }

    private boolean isFull() {
        return start == end && !isEmpty;
    }

    @Override  // usowa
    public int poll() {
     if(!isEmpty){
         int result = data[start];
         start = (start+1)%data.length;
         isEmpty=(start==end);
         return  result;
     }else{
         throw new NoSuchElementException();
     }


    }

    @Override // podglada
    public int peek() {
        if(!isEmpty)
        return data[start];
        else{
            throw new NoSuchElementException();

        }
    }
}
