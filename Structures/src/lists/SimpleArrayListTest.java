package lists;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

/**
 * Created by Adrian on 2017-06-26.
 */
public class SimpleArrayListTest {

    private SimpleArrayList underList = new SimpleArrayList();

    @Test
    public void addToEmptyList() throws Exception {
        underList.add(4);

        assertThat(underList.contain(4)).isTrue();
        assertThat(underList.contain(10)).isFalse();
        assertThat(underList.get(0)).isEqualTo(4);
        assertThat(underList.size()).isEqualTo(1);
    }

    @Test
    public void addToNotEmptyList() throws Exception {
        underList.add(1);
        underList.add(2);

        underList.add(4);

        assertThat(underList.contain(4)).isTrue();
        assertThat(underList.contain(10)).isFalse();
        assertThat(underList.get(2)).isEqualTo(4);
        assertThat(underList.size()).isEqualTo(3);
    }

    @Test
    public void addInTheMiddleOfTheList() throws Exception {
        underList.add(4);
        underList.add(8);

        underList.add(2,1);

        assertThat(underList.contain(2)).isTrue();
        assertThat(underList.contain(4)).isTrue();
        assertThat(underList.contain(8)).isTrue();

        assertThat(underList.get(1)).isEqualTo(2);
        assertThat(underList.get(2)).isEqualTo(8);
        assertThat(underList.size()).isEqualTo(3);
    }

    @Test
    public void remove() throws Exception {

    }

    @Test
    public void removeValue() throws Exception {

    }

    @Test
    public void size() throws Exception {

    }


}
