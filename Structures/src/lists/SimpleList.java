package lists;

/**
 * Created by RENT on 2017-06-23.
 */
public interface SimpleList {
    void add (int value);
    void add (int value, int index);
    int get (int index);
    boolean contain(int value);
    void remove(int index);
    void removeValue(int value);
    int size();
}
