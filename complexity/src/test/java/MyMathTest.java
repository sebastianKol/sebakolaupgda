import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by RENT on 2017-06-26.
 */
public class MyMathTest {
    @org.junit.Test
    public void sum() throws Exception {
        assert  MyMath.sum(1,3)==4;
    }

    @org.junit.Test
    public void pow() throws Exception {
        assert  MyMath.pow(2,2)==4;
    }



    @Test
    public void find() throws  Exception{
        int[] data = {1,2,6,9,11,13,23,41,43,54,90};

        assert MyMath.find(data,11)==5;
    }
}