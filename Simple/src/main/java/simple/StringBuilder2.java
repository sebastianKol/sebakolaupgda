package simple;

public class StringBuilder2 {

	public static void main(String[] args) {
	
	
		long start = System.currentTimeMillis();
		for(int i = 0; i < 1000; i++) {

			generateByBuilder();
		}
		System.out.println(System.currentTimeMillis() - start);
		
		
		
		
	}
	private static void generateByBuilder() {
		StringBuilder builder = new StringBuilder();
		for(int i = 0 ; i < 10; i++) {
		builder = builder.append("*");
		}
		System.out.println(builder.toString());
	}
	
	
	
	

}
