package simple;

public class StringBuilderExample {


		public static void main(String[] args) {
			
			long start = System.currentTimeMillis();
			for(int i = 0; i < 1000; i++) {
				//generateA();
				generateByBuilder();
			}
			System.out.println(System.currentTimeMillis() - start);
			
			
		}

		private static void generateByBuilder() {
			StringBuilder builder = new StringBuilder();
			for(int i = 0 ; i < 3000; i++) {
				builder.append('a');
			}
			System.out.println(builder.toString());
		}
		
		private static void generateA() {
			String a = "";
			for(int i = 0 ; i < 3000; i++) {
				a += "a";
			}
			System.out.println(a);
			//System.out.println(a);
		}
	}