package pl.org.pfig.mytime;

public class MyDate {
	private int year=0;
	private int month=0;
	private int  day=0;
	
	private String strMonths[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
	private String[] strDays = {"Sunday", "Monday", "Tuesday", "Wensday", "Thursday", "Friday", "Saturday"};
    private int[] dayinmonth = {31,28,31,30,31,30,31,31,30,31,30,31};
    
    public MyDate(int year, int month, int day){
    	this.year=year;
    	this.month=month;
    	this.day=day;
    }
    
    @Override
	public String toString() {
		return leadZero(this.hour) + ":" 
			 + leadZero(this.minute) + ":" 
	  		 + leadZero(this.second);
	}
    
    private String leadZero(int num){
    	if(num<10){
    		return "0" + num;
    	}else{
    		return"" + num;
    	}
    }
   
    
    
    
    
    
}
