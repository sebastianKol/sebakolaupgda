package pl.org.pfig.mytime;

public class MyTime {
	private int hour = 0;
	private int minute = 0;
	private int second = 0;
	
//	public MyTime() { }
	
	public MyTime(int hour, int minute, int second) {
		setTime(hour, minute, second);
	}
	
	public void setTime(int hour, int minute, int second) {
		this.hour = hour;
		this.minute = minute;
		this.second = second;
	}

	@Override
	public String toString() {
		return leadZero(this.hour) + ":" 
			 + leadZero(this.minute) + ":" 
	  		 + leadZero(this.second);
	}
	
	private String leadZero(int num) {
		if(num < 10) {
			return "0" + num;
		} else {
			return "" + num; // dlaczego mamy "" ? - ze wzgl�du na to, aby zwr�ci� liczb� jako string, a nie int
		}
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		if(hour >= 0 && hour <= 23) {
			this.hour = hour;
		}
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		if(minute >= 0 && minute <= 59) {
			this.minute = minute;
		}
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		if(second >= 0 && second <= 59) {
			this.second = second;
		}
	}
	
	public MyTime nextHour() {
		int newHour = hour + 1;
		if(newHour == 24) {
			newHour = 0;
		}
		return new MyTime(newHour, minute, second);
	}
	
	public MyTime nextMinute() {
		int newMinute = minute +1;
		int newHour = hour;
		
		if(newMinute>59){
			newMinute =0;
			newHour++;
			
			if(newHour>23){
				newHour=0;
			}
			

		}	
		
		return new MyTime(newHour, newMinute,second);
	}
	
	public MyTime nextSecond(){
		int newSecond = second +1;
		int newMinute = minute;
		int newHour = hour;
		
		if(newSecond > 59){
			newSecond = 0;
			newMinute++;
		}
		if(newMinute>59){
			newMinute =0;
			newHour++;
			}
			
		if(newHour>23){
			newHour=0;
			}

		return new MyTime(newHour, newMinute,newSecond);
	}
	public MyTime previousHour(){
		int newHour = hour -1;
		if(newHour == -1){
			newHour = 23;
		
		}
		return new MyTime(newHour, minute, second);
	}
	public MyTime previousMinute(){
		int newMinute = minute -1;
		int newHour = hour;
		if(newMinute == -1){
			newMinute = 59;
			newHour = newHour-1;
			if(newHour==-1){
				newHour =23;
			}
		}
		return new MyTime(newHour, newMinute,second);
	}
	public MyTime previousSecond(){
		int newSecond = second -1;
		int newMinute = minute;
		int newHour = hour;
		
		if(newSecond==-1){
			newSecond=59;
			newMinute=minute-1;
			if(newMinute == -1){
				newMinute = 59;
				newHour = newHour-1;
				if(newHour==-1){
					newHour =23;
				}
			}
			
		}
		return new MyTime(newHour,newMinute,newSecond);
}
	
	
}
