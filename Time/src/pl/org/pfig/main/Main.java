package pl.org.pfig.main;

import pl.org.pfig.mytime.MyTime;

public class Main {
	public static void main(String[] args) {
		
		MyTime mt = new MyTime(0,0,0);
		
		MyTime mt2 = mt.nextHour();
		MyTime mt3 = mt.nextMinute(); 
		MyTime mt4 = mt.nextSecond();
		MyTime mt5 = mt.previousHour();
		MyTime mt6 = mt.previousMinute();
		MyTime mt7 = mt.previousSecond();
		
		System.out.println("MyTime:" +mt);
		
		System.out.println("Next Hour:"+mt2);
		
		System.out.println("Next Minute:"+mt3);
		
		System.out.println("Next Second:"+mt4);
		
		System.out.println("Previous Hour:"+mt5);
		
		System.out.println("Previous Minute:"+mt6);
		
		System.out.println("Previous Second:"+mt7);
		
		
	}
}