package Part2;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Srednie {

	public static void main(String[] args) {

		try {
			// consoleProgram1();
			// consoleProgram2();
			// consoleProgram3();
			// consoleProgram4();
			// consoleProgram5();
			consoleProgram6();
		} catch (InputMismatchException e) {
			System.out.println("Poda�e� z�y format znak�w");
		}

	}

	private static void consoleProgram1() {
		int[] tablica = { 1, 4, 65, 3, 5, 5, 43, 3, 43 };
		Scanner sc = new Scanner(System.in);
		int a;
		int suma = 0;
		System.out.println("Ile razy w tablicy wystepuje liczba:  ");
		a = sc.nextInt();

		for (int i = 0; i < tablica.length; i++) {
			if (tablica[i] == a) {
				suma = suma + 1;
			}
		}
		System.out.println("Liczba " + a + " wystepuje w tablicy " + suma);
	}

	private static void consoleProgram2() {
		Scanner sc = new Scanner(System.in);

		String text;
		String znaki;
		char znak;
		System.out.println("Podaj ciag znakow");
		text = sc.nextLine();

		System.out.println("Podaj znak");
		znaki = sc.nextLine();

		znak = znaki.charAt(0);

		char[] tabela2 = new char[text.length()];
		int suma2 = 0;

		for (int i = 0; i < tabela2.length; i++) {
			tabela2[i] = text.charAt(i);
		}
		for (int j = 0; j < tabela2.length; j++) {
			if (tabela2[j] == znak) {
				suma2 = suma2 + 1;
			}
		}
		System.out.println("Znak " + znak + " wystepuje w ciagu znakow " + suma2);
	}

	private static void consoleProgram3() {
		Scanner sc = new Scanner(System.in);
		int a;
		int b;
		System.out.println("podaj pierwsza liczbe ");
		a = sc.nextInt();
		System.out.println("podaj druga liczbe ");
		b = sc.nextInt();
		System.out.println("wszystkie wielokrotno�ci a mniejsze od b");
		int wiel = a;
		for (int i = 1; wiel < b; i++) {
			wiel = a * i;
			System.out.println(wiel);
		}
	}

	private static void consoleProgram4() {

		int[] tablica = { 1, 4, 65, 3, 5, 5, 43, 3, 43 };
		int[] kopia = new int[tablica.length];

		int j = 0;

		for (int i : tablica) {
			kopia[j] = i;
			j++;
		}

		for (int k : kopia) {
			System.out.println(k);
		}
	}

	private static void consoleProgram5() {
		int[] tablica = { 1, 4, 65, 3, 5, 5, 43, 3, 43 };
		List<Integer> lista = new ArrayList<Integer>();

		for (int i = 0; i < tablica.length; i++) {
			lista.add(tablica[i]);
		}

		System.out.println(lista);

		int[] kopia = new int[lista.size()];

		for (int i = 0; i < lista.size(); i++) {
			kopia[i] = lista.get(i);

		}
		for (int i : kopia) {
			System.out.println(i);
		}
	}

	private static void consoleProgram6() {
		int[] tablica = { 1, 4, 65, 3, 5, 5, 43, 3, 43 };
		double wynik = 0;
		for (int i = 0; i < tablica.length; i++) {
			wynik = Math.pow((tablica[i]), 2);
			System.out.println("Tablica podniesiona do kwadratu "+wynik);
		}
	}
	
	
}


