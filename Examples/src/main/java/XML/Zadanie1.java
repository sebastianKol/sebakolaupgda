package XML;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Zadanie1 {

	public static void main(String[] args) {
		final String FILE_NAME = "src/main/resources/staff.xml";

		try {
			Document doc = readFile(FILE_NAME);

			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("staff");
			System.out.println("----------------------------");

			double srednia;
			double suma = countAverageSalary(nList);
			srednia = suma / nList.getLength();
			System.out.println("");
			System.out.println("srednia pensja " + srednia + " " + nList.getLength());
			System.out.println(getNames(nList));

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}

	public static Document readFile(final String FILE_NAME)
			throws ParserConfigurationException, SAXException, IOException {
		File file = new File(FILE_NAME);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(file);
		doc.getDocumentElement().normalize();
		return doc;
	}

	public static double countAverageSalary(NodeList nList) {
		double suma = 0;
		for (int i = 0; i < nList.getLength(); i++) {
			Node nNode = nList.item(i);
			System.out.println("\nCurrent Element :" + nNode.getNodeName());
			suma = suma + getSalary(nNode);
			staffParse(nNode);
		}
		return suma;
	}

	public static double getSalary(Node staffNode) {
		Element eElement = (Element) staffNode;
		double salary = Double.parseDouble(eElement.getElementsByTagName("salary").item(0).getTextContent());
		return salary;
	}

	public static void staffParse(Node staffNode) {
		Element eElement = (Element) staffNode;
		System.out.println("Staff id : " + eElement.getAttribute("id"));
		System.out.println("First Name : " + eElement.getElementsByTagName("firstname").item(0).getTextContent());
		System.out.println("Last Name : " + eElement.getElementsByTagName("lastname").item(0).getTextContent());
		System.out.println("Salary : " + eElement.getElementsByTagName("salary").item(0).getTextContent());

	}

	public static List<String> getNames(NodeList nList) {
		List<String> lista = new ArrayList<String>();
		for (int i = 0; i < nList.getLength(); i++) {
			Node nNode = nList.item(i);
			lista.add(GetName(nNode) + " " + getSurnames(nNode));
		}
		return lista;
	}

	public static String GetName(Node n) {
		Element eElement = (Element) n;
		String name;
		name = eElement.getElementsByTagName("firstname").item(0).getTextContent();
		return name;
	}

	public static String getSurnames(Node n) {
		Element eElement = (Element) n;
		String surname;
		surname = eElement.getElementsByTagName("lastname").item(0).getTextContent();
		return surname;
	}

}
// �wiczenie 2
// Napisz metod�, kt�ra odczyta plik XMl, sparsuje go, a nast�pnie zwr�ci list�
// imion i nazwisk.
// Wykorzystaj plik staff.xml:
// +getNames():ArrayList<String>
// W metodzie main() wypisz do konsoli wszystkie odczytane imiona i nazwiska.
