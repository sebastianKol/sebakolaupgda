package connector.baSe.pl;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

public class DatabaseConnector {

    private static DatabaseConnector _instance = null;
    private Connection _conection;
    private DatabaseConnector(DatabaseConnectorBuilder dcb) throws SQLException {

        String url ="jdbc:mysql://"+dcb.hostname()+":"+dcb.port()+"/"+dcb.database();

        _conection = DriverManager.getConnection(url,dcb.username(),dcb.password());
    }

    public  static DatabaseConnector getInstance() throws SQLException {
        File f = new File("src/main/resources/db.config");
        String[] parts = new String[5];
        try {
            String db;
            int i=0;
            Scanner sc = new Scanner(f);
            while (sc.hasNextLine()) {
                db = sc.nextLine();
                parts[i] = db.substring(db.indexOf("-")+1,db.length());
                i++;
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        }

        if(_instance == null){
            _instance = new DatabaseConnector(new DatabaseConnectorBuilder()
                    .hostname(parts[0])
                    .port(Integer.parseInt(parts[1]))
                    .username(parts[2])
                    .password(parts[3])
                    .database(parts[4]));
        }
        return  _instance;
    }
    public Connection getConection(){
        return  _conection;
    }
}





