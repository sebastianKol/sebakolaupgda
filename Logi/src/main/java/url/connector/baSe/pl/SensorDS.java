package url.connector.baSe.pl;

/**
 * Created by Mori on 2017-07-11.
 */
public class SensorDS {
    int IDSensor;
    String addres;

    public SensorDS(int IDSensor, String addres) {
        this.IDSensor = IDSensor;
        this.addres = addres;
    }

    public SensorDS() {
    }

    public int getIDSensor() {
        return IDSensor;
    }

    public void setIDSensor(int IDSensor) {
        this.IDSensor = IDSensor;
    }

    public String getAddres() {
        return addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    @Override
    public String toString() {
        return "SensorDS{" +
                "IDSensor=" + IDSensor +
                ", addres='" + addres + '\'' +
                '}';
    }
}
