package Main.baSe.pl;

import temperatureResolver.baSe.pl.LoggerResolver;
import temperatureResolver.baSe.pl.User;

import java.util.InputMismatchException;
import java.util.Scanner;

public class LogReg {
    Scanner scanner = new Scanner(System.in);
    LoggerResolver loggerResolver = new LoggerResolver();
    ProgramMenu programMenu = new ProgramMenu();


    public void loginRegistryMenu(){
        System.out.println("1. Logowanie");
        System.out.println("2. Rejestracja");
        System.out.println("3. Exit");
        boolean working = true;
        while (working) {
            int select;
            try {
                select = scanner.nextInt();
                switch (select) {
                    case 1:
                        System.out.println("Podaj login");
                        String podanylogin = scanner.next();
                        LoggerResolver loggerResolver = new LoggerResolver();
                        loggerResolver.logging(podanylogin);
                        break;
                    case 2:
                        System.out.println("Rejestracja");
                        registry();
                        break;
                    case 3:
                        working =false;
                        break;
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
    public void registry() {
        String loggin;
        String pass;
        String dslink;
        String name;
        boolean working = true;

        while (working) {
            try {
                System.out.println("Podaj Imie");
                name = scanner.next();
                System.out.println("e-mail");
                loggin = scanner.next();
                System.out.println("Podaj haslo");
                pass = scanner.next();
                System.out.println("Podaj link do servera DS");
                dslink = scanner.next();
                System.out.println("");
                System.out.println("Imie : " + name);
                System.out.println("e-mail : " + loggin);
                System.out.println("Link DS :" + dslink);
                System.out.println("");
                System.out.println("1. Stworz profil");
                System.out.println("2. Wprowadz ponownie");
                System.out.println("3. Powrot");
                int select = scanner.nextInt();

                switch (select){
                    case 1:
                        System.out.println("Utworzono profil");
                        loggerResolver.registry(name, loggin, pass, dslink);
                        working=false;
                        programMenu.menu();
                        break;
                    case 2:
                        registry();
                        break;
                    case 3:
                        working=false;
                        loginRegistryMenu();
                        break;
                }

            } catch (InputMismatchException e) {
                System.out.println("Podaj poprawny format danych");
                working=false;
            }
        }
    }
}
