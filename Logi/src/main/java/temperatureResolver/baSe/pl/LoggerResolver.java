package temperatureResolver.baSe.pl;

import Main.baSe.pl.LogReg;
import Main.baSe.pl.ProgramMenu;
import connector.baSe.pl.DatabaseConnector;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Mori on 2017-07-10.
 */
public class LoggerResolver extends AbstractResolverUser {
    @Override
    public Boolean registry(String name, String login, String pass, String dslink) {
        boolean isInsert = false;
        try{
            isInsert = true;
            Connection c = DatabaseConnector.getInstance().getConection();
            Statement s = c.createStatement();

            s.execute("INSERT INTO `user` VALUES(null"+",'"+login+"','"+pass+"','"+dslink+"','"+name+"')");
            s.close();
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return isInsert;    // dopisac potwierdzenie rejestracji zalezne od TRUE
    }

    @Override
    public Boolean logging(String login) {
            boolean isInsert = false;
            User loggedUser = null;
        try {
            Connection connection = DatabaseConnector.getInstance().getConection();
            // dodac sprawdzenie czy login istnieje
            String query = "SELECT * FROM `user` WHERE `login` ='" + login+"';";
            Statement s = connection.createStatement(); // zapyanie
            ResultSet rs = s.executeQuery(query);

            if(rs.next()) {
                String nameLoaded = rs.getString("name");
                String passLoaded = rs.getString("pass");
                String dslinkLoaded = rs.getString("dslink");

                loggedUser = new User();
                loggedUser.setName(nameLoaded);
                loggedUser.setLoggin(login);
                loggedUser.setDslink(dslinkLoaded);
                loggedUser.setPass(passLoaded);

                System.out.println("");
                System.out.println("Witaj : "+loggedUser.getName());
                System.out.println("Twoj login to : "+loggedUser.getLoggin());
                System.out.println("Jestes podlaczony do : "+loggedUser.getDslink());

                // dodac mozliwosc uaktalniania danych - zmiana hasla oraz linku

                // wprowadzic moliwosc obslugi kilku serwerow- kilka linkow

                ProgramMenu programMenu = new ProgramMenu();   // poprawny login
                programMenu.menu();
                isInsert=true;
            }
            if (isInsert==false) {
                System.out.println("niepoprawny login"); // niepprawny login
                LogReg logReg = new LogReg();
                logReg.loginRegistryMenu();
            }

            rs.close();
            s.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return isInsert;
    }
}
