package temperatureResolver.baSe.pl;

/**
 * Created by Mori on 2017-07-10.
 */
public class User {
    int IDUser;
    String loggin;
    String pass;
    String dslink;
    String name;

    public User() {
    }

    public User(int IDUser, String loggin, String pass, String dslink, String name) {
        this.IDUser = IDUser;
        this.loggin = loggin;
        this.pass = pass;
        this.dslink = dslink;
        this.name = name;
    }

    public int getIDUser() {
        return IDUser;
    }

    public void setIDUser(int IDUser) {
        this.IDUser = IDUser;
    }

    public String getLoggin() {
        return loggin;
    }

    public void setLoggin(String loggin) {
        this.loggin = loggin;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getDslink() {
        return dslink;
    }

    public void setDslink(String dslink) {
        this.dslink = dslink;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "IDUser=" + IDUser +
                ", loggin='" + loggin + '\'' +
                ", pass='" + pass + '\'' +
                ", dslink='" + dslink + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
