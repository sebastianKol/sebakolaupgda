package decorator;

/**
 * Created by RENT on 2017-06-23.
 */
public class Main {
    public static void main(String[] args) {
        PersonPrinter personPrinter = new FileWriter(new HeightPrinter(new AgePrinter(new BasicDataPrinter())));

        Person person = new Person("Jan ", "Kowalski", 20, 81.5, 175, Person.EyesColor.BLUE);
        personPrinter.print(person, System.out);



    }
}
