package decorator;

import java.io.PrintStream;

/**
 * Created by RENT on 2017-06-23.
 */
public interface PersonPrinter {

    void print(Person person, PrintStream out );


}
