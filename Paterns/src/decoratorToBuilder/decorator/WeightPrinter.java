package decoratorToBuilder.decorator;

import java.io.PrintStream;

/**
 * Created by RENT on 2017-06-23.
 */
public class WeightPrinter implements PersonPrinter {
    private final PersonPrinter personPrinter;

    public WeightPrinter(PersonPrinter personPrinter) {
        this.personPrinter = personPrinter;
    }

    @Override
    public void print(Person person, PrintStream out) {
        personPrinter.print(person, out);
        out.print("waga" + person.getWeight());
    }
}
