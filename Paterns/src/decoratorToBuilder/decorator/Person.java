package decoratorToBuilder.decorator;

/**
 * Created by RENT on 2017-06-23.
 */

    public class Person {
        private String name;
        private String surname;
        private int age;
        private double weight;
        private double height;
        private EyesColor eyesColor;

        public Person() {
        }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                ", height=" + height +
                ", eyesColor=" + eyesColor +
                '}';
    }

    public Person(String name, String surname, int age, double weight, double height, EyesColor eyesColor) {
            this.name = name;
            this.surname = surname;
            this.age = age;
            this.weight = weight;
            this.height = height;
            this.eyesColor = eyesColor;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public double getWeight() {
            return weight;
        }

        public void setWeight(double weight) {
            this.weight = weight;
        }

        public double getHeight() {
            return height;
        }

        public void setHeight(double height) {
            this.height = height;
        }

        public EyesColor getEyesColor() {
            return eyesColor;
        }

        public void setEyesColor(EyesColor eyesColor) {
            this.eyesColor = eyesColor;
        }

        public enum EyesColor {
            BROWN, GREEN, DARK, BLUE;
        }
}
