package decoratorToBuilder.decorator;

import java.io.*;

/**
 * Created by RENT on 2017-06-23.
 */
public class FileWriter implements PersonPrinter {
    private PersonPrinter personPrinter;

    public FileWriter(PersonPrinter personPrinter) {
        this.personPrinter = personPrinter;
    }

    @Override
    public void print(Person person, PrintStream out) {

        File ff = new File("decorator.txt");
        try{
            FileOutputStream fos = new FileOutputStream(ff);
            PrintWriter pw = new PrintWriter(fos);

            pw.print(person);
            pw.close();

        }catch (FileNotFoundException e) {
            System.out.println("File not found.");
        }





    }
}
