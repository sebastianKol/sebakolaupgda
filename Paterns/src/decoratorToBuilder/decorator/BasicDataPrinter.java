package decoratorToBuilder.decorator;

import java.io.PrintStream;

/**
 * Created by RENT on 2017-06-23.
 */
public class BasicDataPrinter implements PersonPrinter {
    @Override
    public void print(Person person, PrintStream out) {
        out.println("Imie: "+person.getName()+ "Nazwisko: "+person.getSurname());
        //return person.getName() + +person.getSurname();
    }
}
