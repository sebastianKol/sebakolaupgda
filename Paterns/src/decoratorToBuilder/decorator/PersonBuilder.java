package decoratorToBuilder.decorator;

public class PersonBuilder {
    private String name;
    private String surname;
    private int age;
    private double weight;
    private double height;
    private Person.EyesColor eyesColor;

    public PersonBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public PersonBuilder setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public PersonBuilder setAge(int age) {
        this.age = age;
        return this;
    }

    public PersonBuilder setWeight(double weight) {
        this.weight = weight;
        return this;
    }

    public PersonBuilder setHeight(double height) {
        this.height = height;
        return this;
    }

    public PersonBuilder setEyesColor(Person.EyesColor eyesColor) {
        this.eyesColor = eyesColor;
        return this;
    }

    public Person createPerson() {
        return new Person(name, surname, age, weight, height, eyesColor);
    }
}