package decoratorToBuilder.decorator;

/**
 * Created by RENT on 2017-06-23.
 */
public class Main {
    public static void main(String[] args) {
        PersonPrinter personPrinter = new FileWriter(new HeightPrinter(new AgePrinter(new BasicDataPrinter())));

        Person person = new PersonBuilder().setName("Jan ").setSurname("Kowalski").setAge(20).setWeight(81.5).setHeight(175).setEyesColor(Person.EyesColor.BLUE).createPerson();
        personPrinter.print(person, System.out);



    }
}
