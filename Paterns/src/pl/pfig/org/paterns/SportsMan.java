package pl.pfig.org.paterns;

/**
 * Created by RENT on 2017-06-23.
 */
public interface SportsMan {

    public void prepare();
    public void doPumps(int number);
    public void doSquats(int number);
    public void doCrunches(int number);

}
