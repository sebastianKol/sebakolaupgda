package pl.pfig.org.paterns;

/**
 * Created by RENT on 2017-06-23.
 */
public class DoubleSeries implements  SportsMan {
    private  SportsMan sportsMan;

    public DoubleSeries(SportsMan sportsMan){
        this.sportsMan = sportsMan;
    }

    @Override
    public void prepare() {
        sportsMan.prepare();


    }

    @Override
    public void doPumps(int number) {
        sportsMan.doPumps(number);
        sportsMan.doPumps(number);


    }

    @Override
    public void doSquats(int number) {
        sportsMan.doSquats(number);
        sportsMan.doSquats(number);


    }

    @Override
    public void doCrunches(int number) {
        sportsMan.doCrunches(number);
        sportsMan.doCrunches(number);

    }
}
