package pl.pfig.org.paterns;

/**
 * Created by RENT on 2017-06-23.
 */
public class WaterDrinking implements SportsMan {
    private  SportsMan sportsMan;

    public WaterDrinking(SportsMan sportsMan){
        this.sportsMan = sportsMan;
    }

    @Override
    public void prepare() {
        sportsMan.prepare();
        System.out.println("Pije wode");
    }

    @Override
    public void doPumps(int number) {
        sportsMan.doPumps(number);
        System.out.println("Pije wode");
    }

    @Override
    public void doSquats(int number) {
        sportsMan.doPumps(number);
        System.out.println("Pije wode");

    }

    @Override
    public void doCrunches(int number) {
        sportsMan.doCrunches(number);
        System.out.println("Pije wode");
    }
}
