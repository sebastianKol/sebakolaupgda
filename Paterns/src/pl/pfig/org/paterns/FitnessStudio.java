package pl.pfig.org.paterns;

/**
 * Created by RENT on 2017-06-23.
 */
public class FitnessStudio {

    public void train (SportsMan sportsMan){

        sportsMan.prepare();
        sportsMan.doPumps(2);
        sportsMan.doSquats(5);
        sportsMan.doCrunches(5);
    }
}
