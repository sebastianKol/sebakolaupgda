package pl.pfig.org.paterns;

/**
 * Created by RENT on 2017-06-23.
 */
public class BasicsSportsman implements  SportsMan {
    @Override
    public void prepare() {
        System.out.println("Rozgrzewka");
    }

    @Override
    public void doPumps(int number) {
        int counter =0;
        for(int i=1; i<=number; i++){
            counter++;
            System.out.println("Pompka :"+counter);
        }
    }

    @Override
    public void doSquats(int number) {
        int counter =0;
        for(int i=1; i<=number; i++){
            counter++;
            System.out.println("Przysiady :"+counter);
        }
    }

    @Override
    public void doCrunches(int number) {
        int counter =0;
        for(int i=1; i<=number; i++){
            counter++;
            System.out.println("Brzuszki :"+counter);
        }
    }
}
