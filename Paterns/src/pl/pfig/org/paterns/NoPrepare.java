package pl.pfig.org.paterns;

/**
 * Created by RENT on 2017-06-23.
 */
public class NoPrepare implements SportsMan {
    private  SportsMan sportsMan;

    public NoPrepare(SportsMan sportsMan){
        this.sportsMan = sportsMan;
    }

    @Override
    public void prepare() {
        System.out.println("Brak rozgrzewki");
    }

    @Override
    public void doPumps(int number) {
        sportsMan.doPumps(number);


    }

    @Override
    public void doSquats(int number) {
        sportsMan.doSquats(number);

    }

    @Override
    public void doCrunches(int number) {
        sportsMan.doCrunches(number);

    }
}
