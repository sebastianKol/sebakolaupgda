package students;

import static xml.ParseXML.parseFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import xml.ParseXML;

public class XMLFile implements IFile {

	@Override
	public void save(List<Student> studentsList) {

		try {
			Document doc = ParseXML.newDocument();
			createContent(doc, studentsList);
			ParseXML.saveFile(doc, "src/main/resources/xml/students.xml");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createContent(Document doc, List<Student> studentsList) {
		Element students = createStudents(doc);
		for (Student student : studentsList) {
			add(doc, students, student);
		}
	}

	private Element createStudents(Document doc) {
		Element rootElement = doc.createElement("students");
		doc.appendChild(rootElement);
		return rootElement;
	}

	private void add(Document doc, Element students, Student student) {
		Element studentElement = doc.createElement("student");

		studentElement.setAttribute("index", student.getIndexNumber() + "");

		Element name = doc.createElement("name");
		Element surname = doc.createElement("surname");
		name.appendChild(doc.createTextNode(student.getName()));
		surname.appendChild(doc.createTextNode(student.getSurname()));
		studentElement.appendChild(name);
		studentElement.appendChild(surname);

		students.appendChild(studentElement);
	}

	@Override
	public List<Student> load() {
		
		List<Student> studentsList = new ArrayList<>();
		final String FILE_NAME = "src/main/resources/xml/students.xml";

		try {
			Document doc = parseFile(FILE_NAME);

			NodeList students = doc.getElementsByTagName("student");

			for (int i = 0; i < students.getLength(); i++) {

				Node studentNode = students.item(i);
				Student s = parse(studentNode);
				studentsList.add(s);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		return studentsList;
	}


	static  Student parse (Node studentNode) {
		//System.out.println("\nCurrent Element :" + studentNode.getNodeName());
		if (studentNode.getNodeType() == Node.ELEMENT_NODE) {
			Element student = (Element) studentNode;
			
		//	System.out.println(student.getAttribute("index"));
		//	System.out.println(studentNode.getFirstChild().getNodeName());
	
		int index = Integer.parseInt(student.getAttribute("index"));
		String name = ParseXML.getText(student, "name");
		String surname = ParseXML.getText(student, "surname");
		return new Student(index, name, surname);
			
		
			
			
//			studentElement.appendChild
//			System.out.println("Index : " + getText(student, "index"));
//			System.out.println("Name : " + getText(student, "name"));
//			System.out.println("Surname : " + getText(student, "surname"));
		}
	return null;
	}
}
