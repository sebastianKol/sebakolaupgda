package students;

import java.util.List;

public interface IFile {

	List<Student> load();

	void save(List<Student> studentsList);

}