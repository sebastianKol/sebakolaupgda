package students;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class University {
	
	private Map<Integer, Student> students = new HashMap<>();
	
	public University(List<Student> students){    // wpisanie listy studentow do mapy
		for(Student student: students    ){
			this.students.put(student.getIndexNumber(), student);
		}
	}
	
	public University(){
	}
	
	
	public void addStudent(int indexNumber, String name, String surname) {
		Student student = new Student(indexNumber, name, surname);
		students.put(student.getIndexNumber(), student);
	}
	
	public boolean studentExists(int indexNumber) {
		// Delegacja
		return students.containsKey(indexNumber);
	}
	
	public Student getStudent(int indexNumber) {
		// Delegacja
		return students.get(indexNumber);
	}
	
	public int studentsNumber() {
		return students.size();
	}
	
	public void showAll() {
		for (Student s : students.values()) {
			System.out.println(s.getIndexNumber() + " : " + s.getName() + " " + s.getSurname());
		}
	}
	
	public List<Student> getStudentsList(){   // z mapy na liste
		List<Student> sList = new LinkedList<>();
		
		for (Student s: students.values()){
			sList.add(s);
			
		}
		
		// students.forEach(sList::add);   programowanie funkcyjne
		// new LinkedList<>(students.value());
		
		
		
		return sList;
	}
}