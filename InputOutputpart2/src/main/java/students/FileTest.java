package students;

import java.util.LinkedList;
import java.util.List;

public class FileTest {
	
	public static void main(String[] args) {
		
		List<Student> students = new LinkedList<>();
		University u = new University();
		u.addStudent(100, "Jan", "Koleeee2");
		u.addStudent(101, "Stas", "Loprrrr");
		u.addStudent(102, "Ola", "Golttt");
		
	//	IFile textFile = new TextFile();
	//	IFile textFile = new BinaryFile();
	//	IFile textFile = new SerializedFile();
	//	IFile textFile = new JsonFile();
		IFile textFile = new XMLFile();
		
		textFile.save(u.getStudentsList());
		
		List<Student> studentsFromFile = textFile.load();
		University universityFromFile = new University(studentsFromFile);
		
		universityFromFile.showAll();
		
	}
}
