package xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CreatorCarsXmlFile {
	
	private Document doc;
	
   public static void main(String argv[]) {

     new CreatorCarsXmlFile().create();
     
   }
   
   public void create(){
	   try {
	         doc = ParseXML.newDocument();
	         createContent();
	         ParseXML.saveFile(doc, "src/main/resources/xml/cars.xml");
	         
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	   
   }

private void createContent() {
	 Element rootElement = createCars();
	 Element supercars = addSuperCars(rootElement);
	 ParseXML.setAttribute(doc, supercars, "company", "Ferrari");
	 addCar(supercars, "formula one", "Ferrari 101");
	 addCar(supercars, "sports", "Ferrari 202");
}

private void addCar(Element supercars, String type, String name) {
	Element carname = doc.createElement("carname");
	ParseXML.setAttribute(doc, carname, "type", type);
	 carname.appendChild(
	 doc.createTextNode(name));
	 supercars.appendChild(carname);
}


private Element addSuperCars(Element rootElement) {
	Element supercar = doc.createElement("supercars");
	 rootElement.appendChild(supercar);
	return supercar;
}

private Element createCars() {
	Element rootElement = doc.createElement("cars");
	 doc.appendChild(rootElement);
	return rootElement;
}
}