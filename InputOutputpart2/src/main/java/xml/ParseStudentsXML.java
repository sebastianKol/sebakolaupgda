package xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import static xml.ParseXML.*; // importujemy wszystkie metody i pola z klasy

public class ParseStudentsXML {

	private static final String FILe_NAME = "src/main/resources/xml/class.xml";
	public static void main(String[] args) {
		
		try {
			Document doc = parseFile(FILe_NAME);

			printStudentsInfo(doc);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static void printStudentsInfo(Document doc) {
		NodeList students = doc.getElementsByTagName("student");
	
		System.out.println("----------------------------");
	
		for (int i = 0; i < students.getLength(); i++) {
	
			Node studentNode = students.item(i);
			ParseStudentsXML.printStudentInfo(studentNode);
		}
	}

	public static void printStudentInfo(Node studentNode) {
		System.out.println("\nCurrent Element :" + studentNode.getNodeName());
		if (studentNode.getNodeType() == Node.ELEMENT_NODE) {
			Element student = (Element) studentNode;
			
			System.out.println("Student roll no : " + student.getAttribute("rollno"));
			
			
			System.out.println(student.getFirstChild().getNodeName() + getText(student, "firstname"));
			System.out.println("Last Name : " + getText(student, "lastname"));
			System.out.println("Nick Name : " + getText(student, "nickname"));
			System.out.println("Marks : " + getText(student, "marks"));
		}
	}

}
