package pl.baSe.repository;

import org.springframework.data.repository.CrudRepository;
import pl.baSe.entity.User;

/**
 * Created by Mori on 2017-07-26.
 */
public interface UserRepository extends CrudRepository<User,Long> {
}
