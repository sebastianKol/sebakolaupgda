
public class MyMath {

	public static int mniejsza(int a, int b) {  // mniejsza
		if (a<=b) {
			return a;
		}else {
			return b;
		}
	}
	public static double mniejsza(double a, double b) {
		if (a<=b) {
			return a;
		}else {
			return b;
		}
	}
	
	public static  float mniejsza(float a, float b) {
		if (a<=b) {
			return a;
		}else {
			return b;
		}
	}
	
	public static long mniejsza(long a, long b) {
		if (a<=b) {
			return a;
		}else {
			return b;
		}
	}
	
	
	
	public static int wieksza(int a, int b) { // wieksza
		if (a<=b) {
			return b;
		}else {
			return a;
		}
	}
	public static double wieksza(double a, double b) {
		if (a<=b) {
			return b;
		}else {
			return a;
		}
	}
	
	public static  float wieksza(float a, float b) {
		if (a<=b) {
			return b;
		}else {
			return a;
		}
	}
	
	public static long wieksza(long a, long b) {
		if (a<=b) {
			return b;
		}else {
			return a;
		}
	}
	
	
	
	public static long abs(int a) {
		if (a>0) {
			return a;
		}else {
			return -a;
		}
	}
	
	public static double abs(double a) {
		if (a>0) {
			return a;
		}else {
			return -a;
		}
	}
	public static long abs(long a) {
		if (a>0) {
			return a;
		}else {
			return -a;
		}
	}
	public static float abs(float a) {
		if (a>0) {
			return a;
		}else {
			return -a;
		}
	}
	
	public static long power (int a,int b){
		
		long wynik = 1;
		for(int i=1; i<b; b++){
			wynik=wynik*a;
		}
		return wynik;
		
	}
	
	public static double power (double a,int b){
		
		double wynik =1;
		for(int i=1; i<b; b++){
		 wynik =wynik*a;
		}
		return wynik;
		
	}
	
	public static double power (float a,int b){
		
		double wynik = 1;
		for(int i=1; i<b; b++){
			wynik =wynik*a;
		}
		return wynik;
		
	}
	
	public static long power (long a,int b){
		long wynik =1;
		for(int i=1; i<b; b++){
			wynik =wynik*a;
		}
		return wynik;
		
	}
	
	
}
