import org.junit.Test;

public class MyMathTest {
	
	@Test
	public void mniejszaTest(){
		assert MyMath.mniejsza(1,4)==1;        // int
		assert MyMath.mniejsza(1d,1.4)==1.0;    // double
		assert MyMath.mniejsza(1f,10f)==1;    // float
		assert MyMath.mniejsza(1l,177l)==1; // long
	}
	
	@Test
	public void wiekszaTest(){
		assert MyMath.wieksza(1,4)==4;        // int
		assert MyMath.wieksza(1d,1.4)==1.4;    // double
		assert MyMath.wieksza(1f,10f)==10;    // float
		assert MyMath.wieksza(1l,177l)==177; // long
	}
	
	@Test
	public void absTest(){
		assert MyMath.abs(1)==1;        // int
		assert MyMath.abs(-2d)==2;    // double
		assert MyMath.abs(0f)==0;    // float
		assert MyMath.abs(-999l)==999; // long
	}
	
	@Test
	public void powerTest(){
		assert MyMath.power(1,3)==1;        // int
		assert MyMath.power(2d,1)==2.0;    // double // sprawdzic
		assert MyMath.power(3f,2)==27;    // float
		assert MyMath.power(2l,1)==21; // long
	}
	
}
