import org.junit.Test;

public class GeoMathTest {
	
	
	
	@Test
	public void squareTest(){
		assert GeoMath.squareArea(2.0) ==4.0; // pole kwadratu
		assert GeoMath.squareArea(3.0) ==9.0; // pole kwadratu
		assert GeoMath.squareArea(1.0) ==1.0; // pole kwadratu
		
	}
	@Test
	public void cubeAreaTest(){
		assert GeoMath.cubeArea(1) == 6.0;   // pole szescianu
		assert GeoMath.cubeArea(2) == 24;   // pole szescianu
	}
	@Test
	public void circleAreaTest(){
		assert GeoMath.circleArea(2.0) == Math.PI*4; // pole kola
		assert GeoMath.circleArea(1.0) > 3.1415; // pole kola
		assert GeoMath.circleArea(1.0) <3.1516; // pole kola
	}
	@Test
	public void cylinderVolumeTest(){
		assert GeoMath.cylinderVolume(2.0, 3.0) == Math.PI*4*3; // pole walca o wysokosci3
		assert GeoMath.cylinderVolume(1.0, 3.0) > 3 * 3.1415; // pole walca o wysokosci3
		assert GeoMath.cylinderVolume(1.0, 3.0) < 3 * 3.1516; // pole walca o wysokosci3
		
	}
	@Test
	public void coneAreaTest(){
		assert GeoMath.coneArea(2.0, 3) == Math.PI*4*3/3; // pole stozka o wysokosci
		assert GeoMath.coneArea(1.0, 3) > 3.1415 *1*3/3; // pole stozka o wysokosci
		assert GeoMath.coneArea(1.0, 3) < 3.1516 *1*3/3; // pole stozka o wysokosci
		
		
	}
	@Test
	public void cubeVolumeTest(){
		assert GeoMath.cubeVolume(2) == 8.0;   // objetosc szescianu
		assert GeoMath.cubeVolume(1) == 1.0;   // objetosc szescianu
		assert GeoMath.cubeVolume(3) == 27.0;   // objetosc szescianu
	}
	@Test
	public void OstroslupVolumeTest(){
		
		assert GeoMath.OstroslupVolume(2.0, 3) == Math.PI*4*3/3; // objetosc ostroslupa o podstawoe kwadratu
		assert GeoMath.OstroslupVolume(1.0, 6) > 3.1415 *1*1* 6/3; // objetosc ostroslupa o podstawoe kwadratu
		assert GeoMath.OstroslupVolume(1.0, 6) < 3.1516 *1*1* 6/3; // objetosc ostroslupa o podstawoe kwadratu
		
		
	}
	

}
