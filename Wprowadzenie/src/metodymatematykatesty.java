import org.junit.Test;

public class metodymatematykatesty {
	
	@Test
	public void testIloczynu(){
		assert 3<4;
		
	}
	@Test
	public void testIloczynu2(){
		assert metodymatematyka.iloczyn(1,1)==1;
		assert metodymatematyka.iloczyn(0,10)==0;
		assert metodymatematyka.iloczyn(1,10)==10;
		assert metodymatematyka.iloczyn(3,4)==12;
		
	}
	
	@Test
	public void testszukaj(){
		int[] liczby = {1,2,3,5,3};
		
		
		assert metodymatematyka.szukaj(liczby, 3)==2; // liczba 3 na pozycji 2(czyli 3 liczac od zera
		assert metodymatematyka.szukaj(liczby, 4)==-1;
		assert metodymatematyka.szukaj(liczby, 5)==3;
	}
	
	@Test
	public void testsuma(){
		
		assert metodymatematyka.suma(1,1)==2;
		assert metodymatematyka.suma(1123,0)==1123;
		assert metodymatematyka.suma(44,5)==49;
	}
	
	@Test
	public void testmniejsza(){
		assert metodymatematyka.mniejsza(2,56)==2;
		assert metodymatematyka.mniejsza(0,56,8)==0; // kozystam z innej metody z 3 int
		assert metodymatematyka.mniejsza(2,-6)==-6;
	}
	
	
	}
	


