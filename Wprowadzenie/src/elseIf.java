public class elseIf {

	public static void main(String[] args) {

		System.out.println("2 > 3");
		
		if (2 > 3) {
			System.out.println(":)");
		}else {
			System.out.println(":(");
		}
		
		System.out.println("9%2 == 0");
		
		if (9%2 == 0) {
			System.out.println(":)");
		}else {
			System.out.println(":(");
		}
	}

}