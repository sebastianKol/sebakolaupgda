
public class GeoMath {

	
	public static double squareArea(double a){ // pole kwadrat 
		
		return a*a;
	}
	
	public static double cubeArea(double a){ // pole szescian
		return squareArea(a)*6;
	}
	
	public static double circleArea(double r){ // pole kola
		return Math.PI *r*r;
	}
	
	public static double cylinderVolume(double r, double h){ // pole walca
		return circleArea(r)*h;
	}
	
	public static double coneArea(double r, double h){ // pole stozka
		return circleArea(r)*h/3;
	}
	
	public static double cubeVolume(double a){ // objetosc szescianu
		return squareArea(a)*a;
	}

	public static double OstroslupVolume(double a, double h){ // objetosc ostroslupa o podstawie kwadratu
		return circleArea(a)*h/3;
	}



}
