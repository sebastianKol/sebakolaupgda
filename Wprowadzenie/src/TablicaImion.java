
public class TablicaImion {

	public static void main(String[] args) {

		String[] imiona = {"Zbigniew", "Franciszek", "Adrian", "Adam"};
		System.out.println("Wszystkie imiona (p�tla forEach):");
		for(String imie : imiona){
			System.out.println(imie);
			
		}
		
		System.out.println("Co drugie imi� (p�tla for):");
		for(int i = 0; i < imiona.length; i+=2)
		{
			
			System.out.println(imiona[i]);
		}
		
		System.out.println("Wypisuje imiona na litere A:");
		for(String imie : imiona){
			
			if (imie.charAt(0)=='A'){
				System.out.println(imie);
			}
		}
		System.out.println("Wypisuje imiona na litere A (alternatywnie):");
		for(String imie : imiona){
			
			if (imie.startsWith("A")){
				System.out.println(imie);
			}
		}
	}
	
	

}