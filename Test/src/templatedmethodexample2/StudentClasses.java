package templatedmethodexample2;

/**
 * Created by RENT on 2017-06-22.
 */
public abstract class StudentClasses {
    public void Classes(){
        Mathematic();
        Phisic();
        ExtraSubject();
        ExtraSubjectTwo();
        System.out.println();
    }
    public void Mathematic(){
        System.out.println("Matematyka");
    }
    public void Phisic(){
        System.out.println("Fizyka");
    }

    public abstract void ExtraSubject();
    public abstract void ExtraSubjectTwo();


}
