package singleton;

/**
 * Created by RENT on 2017-06-22.
 */
public class SingletonExample {

    private static SingletonExample _instance;
    private String name;

    private SingletonExample(){}

    public static SingletonExample get_instance() { // lazy initialisation

        if (_instance == null){
            System.out.println("Tworze instancje");
            _instance = new SingletonExample();
        }
        System.out.println("Zwracam instancje, bo jest już utworzona.");
        return _instance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
