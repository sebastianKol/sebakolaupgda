package factoryThree;

/**
 * Created by RENT on 2017-06-22.
 */
public class VechiclesFactory {
    public static VechiclesInterface getVechicle (String vechicle) {
        vechicle = vechicle.toLowerCase();
        switch (vechicle) {
            case "truck":
                return new Truck();
            case "car":
                return new Car();
            case "buss":
                return  new Buss();
        }
    throw new IllegalArgumentException("Unknown Vechicle");
    }

}
