import factory.AnimalFactory;
import factory.AnimalInterface;
import factoryThree.VechiclesFactory;
import factoryThree.VechiclesInterface;
import factoryTwo.Coupon;
import factoryTwo.CouponFactory;
import fluidinterface.Beer;
import singleton.SingletonExample;
import templatedmethodexample2.Algebra;
import templatedmethodexample2.Chemistry;
import templatedmethodexample2.Electronic;
import templatedmethodexample2.Programing;
import templatemethod.Laptop;
import templatemethod.MidiTowerComputer;
import templatemethod.PersonalComputer;

import fluidinterface.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by RENT on 2017-06-22.
 */
public class Main {
    public static void main(String[] args) {
        Singleton
        SingletonExample se1 = SingletonExample.get_instance();
        se1.setName("seba");
        SingletonExample se2 = SingletonExample.get_instance();
        System.out.println(se2.getName());

        CaptinSelection captin1 = CaptinSelection.get_Captin();  // captin1 i captin2 wskazuja
        CaptinSelection captin2 = CaptinSelection.get_Captin();  // na to samo miejsce w pamieci


        //Templated Method
//
//        Laptop laptop = new Laptop();
//        System.out.println("Laptop:");
//        laptop.devices();
//
//        MidiTowerComputer mtc = new MidiTowerComputer();
//        System.out.println("midiTowerComputer :");
//        mtc.devices();
//
//        PersonalComputer pc = new PersonalComputer();
//        System.out.println("Pc :");
//        pc.devices();

//        // Templated Method ex2
//
//        Algebra algebra = new Algebra();
//        System.out.println("Studenci Algebry maja zajecia:");
//        algebra.Classes();
//
//        Chemistry ch = new Chemistry();
//        System.out.println("Studenci Chemii maja zajecia:");
//        ch.Classes();
//
//        Programing pro = new Programing();
//        System.out.println("Studenci Programowania maja zajecia:");
//        pro.Classes();
//
//        Electronic elec = new Electronic();
//        System.out.println("Studenci Elektroniki maja zajecia:");
//        elec.Classes();

        // Factroy Patern

//        AnimalInterface cat = AnimalFactory.getAnimal("cat");
//        cat.getSound();
//        AnimalInterface dog = AnimalFactory.getAnimal("dog");
//        dog.getSound();
//        AnimalInterface frog = AnimalFactory.getAnimal("frog");
//        frog.getSound();
//
//        //AnimalInterface tiger = AnimalFactory.getAnimal("tiger");

        // Factory Patern Two

//        int[] numbers = new int[6];
//        for(int i=0; i<numbers.length; i++){
//            numbers[i] = new Random().nextInt(48)+1;
//        }
//        Coupon c = CouponFactory.getcoupon(numbers);
//        System.out.println(c.getA());


//        VechiclesInterface truck = VechiclesFactory.getVechicle("truck");
//        truck.getType();
//
//        VechiclesInterface car = VechiclesFactory.getVechicle("car");
//        car.getType();
//
//        VechiclesInterface bus = VechiclesFactory.getVechicle("buss");
//        bus.getType();

        //Chaining

//        Beer piwo = new Beer();
//        piwo
//                .setName("Specjal")
//                .setType("lager")
//                .setTaste("Bitter")
//                .setPrice(2.19);
//        System.out.println(piwo);

            //FluidInterface


//        List<Person> people = new LinkedList<>();
//
//        people.add(new Person("Pawel","Testowy",30));
//        people.add(new Person("Piotr","Testowy",42));
//        people.add(new Person("Mirek","Przykladowy",22));
//        people.add(new Person("Pawel","Testowy",44));
//
//        People pp = new People().addGroup("staff",people);
//
//        for(Person pers: pp.from("staff").lastname("Testowy").name("Pawel").get()){
//            System.out.println(pers);
//        }

    }
}
