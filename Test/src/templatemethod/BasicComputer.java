package templatemethod;

/**
 * Created by RENT on 2017-06-22.
 */
public abstract class BasicComputer {
    public  void devices(){
        motherboard();
        procesor();
        externalDevices();
        System.out.println();
    }
    public void motherboard(){
        System.out.println("Motherboard");
    }
    public void procesor(){
        System.out.println("Procesor");
    }
    public abstract void externalDevices();

}
