package fluidinterface;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-06-22.
 */
public class Beer {
    private String taste;
    private String name;
    private String type;
    private double price;

    public Beer() {
    }

    public String getTaste() {
        return taste;
    }

    public Beer setTaste(String taste) {
        this.taste=taste;
        return this;
    }

    public String getName() {
        return name;
    }

    public Beer setName(String name) {
        this.name=name;
        return this;
    }

    public String getType() {
        return type;
    }

    public Beer setType(String type) {
        this.type=type;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public Beer setPrice(double price) {
        this.price=price;
        return this;
    }

    @Override
    public String toString() {
        return "Beer{" +
                "taste='" + taste + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", price=" + price +
                '}';



    }
}
