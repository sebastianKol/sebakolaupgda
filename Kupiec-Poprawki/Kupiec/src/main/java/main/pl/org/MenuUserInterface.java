package main.pl.org;

/**
 * Created by Mori on 2017-06-29.
 */
import java.util.Random;
import java.util.Scanner;

import static main.pl.org.ArrayPrint.printArray;

/**
 * Created by RENT on 2017-06-29.
 */
public class MenuUserInterface {

    private Scanner scanner = new Scanner(System.in);
    public final Random random = new Random();
    private Game game;

    public static void main(String[] args) {
        MenuUserInterface menuUserInterface = new MenuUserInterface();
        menuUserInterface.showStartMenu();
    }

    private void showStartMenu() {
        game = new Game();

        boolean working = true;
        while (working) {

            generateCity();
            menuList();

            int select;
            try {
                select = getSelect();

                switch (select) {
                    case 1:
                        firstPlayerGenerator();
                        firstCityCreator();
                        game.Game();

                        break;
                    case 2:
                        firstCityCreator();
                        game.loadGameFromFile();

                        break;
                    case 3:
                        game.saveGameToFile();
                        break;
                    case 4:
                        working =false;
                        break;
                }
            } catch (Exception e) {
                //print the exception
                System.out.println(e.getMessage());
            }
        }
    }

    private void generateCity() {
        for (int i=0; i <Cities.values().length;i++    ) {  // enum zawiera 6 miast
            int tradeValure = randomValueSelect();
            Cities cities = Cities.values()[i];
            CitiesState citiesState = new CitiesState(cities, tradeValure);
            game.addCitiesToList(citiesState);
        }
    }


    private void firstPlayerGenerator() {
        String playerName = UserGiveName();
        int gameLevel = UserSelectLevel();
        Player player = new Player(playerName,gameLevel,0);
        game.startPlayer(player);
    }

    private void firstCityCreator() {
        Cities startCity = UserSelectCity();
        int tradeValure = randomValueSelect();
        CitiesState citiesState = new CitiesState(startCity,tradeValure);
        game.startCity(citiesState);
    }

    private Cities UserSelectCity() {
        System.out.println("Wybierz mniejsce rozpoczecia gry.");
        printArray(Cities.values());
        int ext = getSelect();
        return Cities.values()[ext-1];
    }


    private int UserSelectLevel() {
        System.out.println("Wybierz poziom trudności");
        printArray(Levels.values());
        int ext = getSelect();
        int [] startCash = {3000,2000,1000};

        return startCash[ext-1];
    }

    private String UserGiveName() {
        System.out.println("Podaj imie");
        String  playerName = scanner.next();
        return playerName;
    }

    private void menuList() {
        System.out.println("1. New Game");
        System.out.println("2. Load");
        System.out.println("3. Save");
        System.out.println("4. Exit");
    }

    private int getSelect() {
        return scanner.nextInt();
    }
    private int randomValueSelect(){
        int randomNum = rand(100, 80);
        return randomNum;
    }

    private int rand(int max, int min) {
        return random.nextInt((max - min) + 1) + min;
    }
}