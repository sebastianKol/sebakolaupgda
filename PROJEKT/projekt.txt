Technologia: Java EE, JSP, JDBC, HTML, CSS, Hibernate, Javascript, jQuery, Spring, Angular, Arduino

Temat: Rejestrator temperatury

Funkcjonalność: System webowy do zarzadzania pomiarem temperatury, umożliwia zdalny podglad w czasie rzeczywistym oraz rejestracje pomiarow w bazie danych.
		Wszyscy uzytkownicy maja dostep do podgladu w czasie rzeczywistym. Po zalogowaniu otrzymuje sie dostep do rejestratora.
		
		W Systemie mozna wykonywac nastepujace czynnosci:
		- Podglad bez logowania po wklejeniu linka z odpowiednio przygotowanym modelem wyswietlania JSON
		
		- Po zalogowaniu:
			- mozliwosc ustawienia częstotliwosci automatycznych zapisow do bazy danych,
			- prezentacje danych z bazy w tabeli, sortowanie po czujniku, sortowanie po czasie kiedy byla dokonywana rejestracja,
			- prezentacja danych z bazy danych na wykresach i w tabelach



