var app = angular.module('RESTApp', ['ngRoute']);
var url = 'http://localhost:8088/';



app.config(function($routeProvider) {
    var path = './views/';
    $routeProvider
        .when('/', {
            templateUrl: path + 'main.html'
        })
        .when('/try', {
            templateUrl: path + 'try.html',
            controller: 'showController'
        })
         .when('/tryChart', {
            templateUrl: path + 'tryChart.html',
        })
        .when('/show', {
            templateUrl: path + 'show.html',
            
        });
});

var link;
app.controller('showController', function($scope) {
    $scope.add = function(){
        link = document.getElementById('userInput').value;
    }
});


 app.controller('fifthC', ['$scope', '$http', function ($scope, $http) {
        $scope.showName = setInterval(function () {
            $http({
                url: link,
                method: 'GET',
                dataType: 'json',
                params: {}
            }).then(function (res) {
                sensors = res.data.sensors;
                tt1 = sensors[0].temp;
                tt2 = sensors[1].temp;
                tt3 = sensors[2].temp;
            }, function (err) {
                console.log(err);
            });
            console.log("other");
        },1000);
    }]);



var gaugeOptions = {
    chart: {
        type: 'solidgauge'
    },
    title: null,
    pane: {
        center: ['50%', '85%'],
        size: '140%',
        startAngle: -90,
        endAngle: 90,
        background: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
            innerRadius: '60%',
            outerRadius: '100%',
            shape: 'arc'
        }
    },
    tooltip: {
        enabled: false
    },
    // the value axis
    yAxis: {
        stops: [
            [0.1, '#3942ef'], // green
            [0.3, '#55BF3B'], // green
            [0.7, '#DDDF0D'], // yellow
            [0.9, '#DF5353'] // red
        ],
        lineWidth: 0,
        minorTickInterval: null,
        tickAmount: 2,
        title: {
            y: -70
        },
        labels: {
            y: 16
        }
    },

    plotOptions: {
        solidgauge: {
            dataLabels: {
                y: 5,
                borderWidth: 0,
                useHTML: true
            }
        }
    }
};

var sensor2 = Highcharts.chart('container-sensor2', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: 35,
        title: {
            text: 'Czujnik 2'
        }
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'sensor2',
        data: [0],
        dataLabels: {
            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
            ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y:.1f}</span><br/>' +
            '<span style="font-size:12px;color:silver">Temp</span></div>'
        },
        tooltip: {
            valueSuffix: ' temp'
        }
    }]
}));

var sensor1 = Highcharts.chart('container-sensor1', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: 35,
        title: {
            text: 'Czujnik1'
        }
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'sensor1',
        data: [0],
        dataLabels: {
            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
            ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y:.1f}</span><br/>' +
            '<span style="font-size:12px;color:silver">temp</span></div>'
        },
        tooltip: {
            valueSuffix: ' temp'
        }
    }]
}));

var sensor3 = Highcharts.chart('container-sensor3', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: 35,
        title: {
            text: 'Czujnik3'
        }
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'sensor3',
        data: [0],
        dataLabels: {
            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
            ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y:.1f}</span><br/>' +
            '<span style="font-size:12px;color:silver">temp</span></div>'
        },
        tooltip: {
            valueSuffix: 'temp'
        }
    }]
}));


setInterval(function () {

    var point;

    if (sensor1) {
        point = sensor1.series[0].points[0];
        point.update(tt1);
    }
    if (sensor2) {
        point = sensor2.series[0].points[0];
        point.update(tt2);
    }
    if (sensor3) {
        point = sensor3.series[0].points[0];
        point.update(tt3);
    }
}, 500);


