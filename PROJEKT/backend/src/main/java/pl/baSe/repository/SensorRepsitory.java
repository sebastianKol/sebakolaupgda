package pl.baSe.repository;

import org.springframework.data.repository.CrudRepository;
import pl.baSe.entity.Sensor;

/**
 * Created by Mori on 2017-07-26.
 */
public interface SensorRepsitory extends CrudRepository<Sensor,Long> {
}
