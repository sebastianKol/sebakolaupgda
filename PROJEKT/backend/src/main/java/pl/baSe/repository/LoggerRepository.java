package pl.baSe.repository;

import org.springframework.data.repository.CrudRepository;
import pl.baSe.entity.Logger;

/**
 * Created by Mori on 2017-07-26.
 */
public interface LoggerRepository extends CrudRepository<Logger,Long> {
}
