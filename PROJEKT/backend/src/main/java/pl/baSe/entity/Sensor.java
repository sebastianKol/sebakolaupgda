package pl.baSe.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Mori on 2017-07-26.
 */
@Entity
public class Sensor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String macAddress;
    private int numberOnURL;
    private float tempNow;

    @OneToMany(mappedBy = "sensor")
    private List<Logger> loggers;

    public Sensor() {
    }

    public Sensor(String macAddress, int numberOnURL, float tempNow, List<Logger> loggers) {
        this.macAddress = macAddress;
        this.numberOnURL = numberOnURL;
        this.tempNow = tempNow;
        this.loggers = loggers;
    }

    public int getId() {
        return id;
    }

    public Sensor setId(int id) {
        this.id = id;
        return this;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public Sensor setMacAddress(String macAddress) {
        this.macAddress = macAddress;
        return this;
    }

    public int getNumberOnURL() {
        return numberOnURL;
    }

    public Sensor setNumberOnURL(int numberOnURL) {
        this.numberOnURL = numberOnURL;
        return this;
    }

    public List<Logger> getLoggers() {
        return loggers;
    }

    public Sensor setLoggers(List<Logger> loggers) {
        this.loggers = loggers;
        return this;
    }

    public float getTempNow() {
        return tempNow;
    }

    public Sensor setTempNow(float tempNow) {
        this.tempNow = tempNow;
        return this;
    }
}
