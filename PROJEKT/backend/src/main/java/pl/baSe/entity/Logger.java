package pl.baSe.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;

/**
 * Created by Mori on 2017-07-26.
 */
@Entity
public class Logger {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Time timeNow;
    private Date dateNow;

    @JsonManagedReference
    @ManyToOne(cascade = CascadeType.ALL)
    private User user;

    @ManyToOne(cascade = CascadeType.ALL)
    private Sensor sensor;

    public Logger() {
    }

    public Logger(Time timeNow, Date dateNow) {
        this.timeNow = timeNow;
        this.dateNow = dateNow;
    }

    public long getId() {
        return id;
    }

    public Logger setId(long id) {
        this.id = id;
        return this;
    }

    public Time getTimeNow() {
        return timeNow;
    }

    public Logger setTimeNow(Time timeNow) {
        this.timeNow = timeNow;
        return this;
    }

    public Date getDateNow() {
        return dateNow;
    }

    public Logger setDateNow(Date dateNow) {
        this.dateNow = dateNow;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Logger setUser(User user) {
        this.user = user;
        return this;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public Logger setSensor(Sensor sensor) {
        this.sensor = sensor;
        return this;
    }
}
