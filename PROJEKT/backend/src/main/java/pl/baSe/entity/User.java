package pl.baSe.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;


/**
 * Created by Mori on 2017-07-26.
 */
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String email;
    @JsonIgnore
    private String pass;
    private String dsLink;
    private int frequency;

    @OneToMany(mappedBy = "user")
    private List<Logger> loggers;

    public User() {
    }

    public User(String name, String email, String pass, String dsLink, int frequency, List<Logger> loggers) {
        this.name = name;
        this.email = email;
        this.pass = pass;
        this.dsLink = dsLink;
        this.frequency = frequency;
        this.loggers = loggers;
    }

    public long getId() {
        return id;
    }

    public User setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPass() {
        return pass;
    }

    public User setPass(String pass) {
        this.pass = pass;
        return this;
    }

    public String getDsLink() {
        return dsLink;
    }

    public User setDsLink(String dsLink) {
        this.dsLink = dsLink;
        return this;
    }

    public int getFrequency() {
        return frequency;
    }

    public User setFrequency(int frequency) {
        this.frequency = frequency;
        return this;
    }

    public List<Logger> getLoggers() {
        return loggers;
    }

    public User setLoggers(List<Logger> loggers) {
        this.loggers = loggers;
        return this;
    }
}
