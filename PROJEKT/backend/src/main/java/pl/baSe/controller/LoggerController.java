package pl.baSe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.baSe.entity.Logger;
import pl.baSe.entity.Sensor;
import pl.baSe.entity.User;
import pl.baSe.repository.LoggerRepository;
import pl.baSe.repository.SensorRepsitory;
import pl.baSe.repository.UserRepository;

import java.sql.Time;
import java.util.Date;
import java.util.List;

/**
 * Created by RENT on 2017-07-28.
 */
@CrossOrigin
@RestController
@RequestMapping("/logger")
public class LoggerController {
    Date d = new Date(System.currentTimeMillis());
    Time t = new Time(System.currentTimeMillis());


    @Autowired
    UserRepository userRepository;

    @Autowired
    SensorRepsitory sensorRepsitory;

    @Autowired
    LoggerRepository loggerRepository;

    @RequestMapping("/")
    public String logger() {
        return "";
    }


    // czy czas i data ma byc przekazywana z frontu czy generowana tutaj
    @RequestMapping("/add")
    public Logger addLogg(@RequestParam(name = "user") String user,
                          @RequestParam(name = "sensor") String sensor){
        long userId = Long.valueOf(user);
        long sensorId = Long.valueOf(sensor);
        Logger logger = new Logger();

        User u = userRepository.findOne(userId);
        Sensor s = sensorRepsitory.findOne(sensorId);

        logger.setSensor(s);
        logger.setUser(u);
        logger.setTimeNow(t);
        logger.setDateNow((java.sql.Date) d);
        return loggerRepository.save(logger);
    }
    @RequestMapping("/show")
    public List<Logger> listLoggs(){
        return (List<Logger>) loggerRepository.findAll();
    }


}
