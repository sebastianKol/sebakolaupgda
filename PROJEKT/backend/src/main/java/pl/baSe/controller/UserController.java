package pl.baSe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.baSe.entity.User;
import pl.baSe.repository.UserRepository;

/**
 * Created by RENT on 2017-07-28.
 */
@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping("/add")
    public User add(@RequestParam(name = "name") String name,
                    @RequestParam(name = "email") String email,
                    @RequestParam(name = "pass") String pass,
                    @RequestParam(name = "dslink") String dslink) {
        User user = new User();
        user.setName(name);
        user.setEmail(email);
        user.setPass(pass);
        user.setDsLink(dslink);
        // w czasie rejestracji uzytkownikowy zostaje przypisana czestotliwosc 24h
        user.setFrequency(24);
        return userRepository.save(user);
    }
    @RequestMapping("/show/{id}")
    public User showById(@PathVariable(name = "id") String id){
        long myId = Long.valueOf(id);
            return userRepository.findOne(myId);
    }
    @RequestMapping("/delete/{id}")
    public boolean deleteUser(@PathVariable(name="id") String id){
        long myId = Long.valueOf(id);
        User user = userRepository.findOne(myId);
        user.setLoggers(null);
        userRepository.delete(user);
        return true;
    }
    @RequestMapping("edit")
    public User editUser(@RequestParam(name = "id") String id,
                         @RequestParam(name = "name") String name,
                         @RequestParam(name = "email") String email,
                         @RequestParam(name = "pass") String pass,
                         @RequestParam(name = "dslink") String dsLink
                         ){
        long myId = Long.valueOf(id);
        User user = userRepository.findOne(myId);
        user.setId(myId);
        user.setName(name);
        user.setEmail(email);
        user.setDsLink(dsLink);
        user.setPass(pass);
        user.setFrequency(24);
        // czy moze tu byc null?? czy nie skasuje on wszystkich danyc z DB
        user.setLoggers(null);

        return userRepository.save(user);
    }
    @RequestMapping("editFreq")
    public User editFreq(@PathVariable(name = "id") String id, @RequestParam(name="frequency") int frequency){
        long myId = Long.valueOf(id);
        User user = userRepository.findOne(myId);
        user.setFrequency(frequency);
        return userRepository.save(user);
    }



}
