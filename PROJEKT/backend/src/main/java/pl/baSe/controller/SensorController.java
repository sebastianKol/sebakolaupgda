package pl.baSe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.baSe.entity.Logger;
import pl.baSe.entity.Sensor;
import pl.baSe.repository.SensorRepsitory;

import java.util.List;

/**
 * Created by RENT on 2017-07-28.
 */
@CrossOrigin
@RestController
@RequestMapping("/sensor")
public class SensorController {

    @Autowired
    private SensorRepsitory sensorRepsitory;

    @RequestMapping("/add")
    public Sensor add (@RequestParam(name = "macAddress") String macAddres,
                       @RequestParam(name = "numberOnURL") String numberOnUrl,
                       @RequestParam(name = "tempNow") Float tempNow){
        Sensor sensor = new Sensor();
        sensor.setMacAddress(macAddres);
        sensor.setNumberOnURL(Integer.parseInt(numberOnUrl));
        sensor.setTempNow(tempNow);
        return sensorRepsitory.save(sensor);
    }
    @RequestMapping("/show{id}")
    public  Sensor showById(@PathVariable(name = "id") String id){
        long myId = Long.valueOf(id);
        return sensorRepsitory.findOne(myId);
    }

}
