package UserGen;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.Scanner;

	public class UserGenerator {
		private final String path = "src/main/resources/";
	
		public User getRandomUser() {
			UserSex us = UserSex.SEX_MALE;
			if(new Random().nextInt(2)==0){
				us = UserSex.SEX_FEMALE;
			}
			return getRandomUser(us);
		
		}	
		
		
		public User getRandomUser(UserSex sex) {
			User currentUser = new User();
			currentUser.setSex(sex);
			String name = "";
			if(sex.equals(UserSex.SEX_MALE)){
				name=getrandomLineFromFile("name_m.txt");
			}else{
				name=getrandomLineFromFile("name_f.txt");
			}
				
			currentUser.setName(name);
			currentUser.setSecondname(getrandomLineFromFile("lastname.txt"));
			currentUser.setAddress(getRandomAddress());
			currentUser.setPhone(""+random1to9()+random0to9()+random0to9()+" "
								   +random0to9()+random0to9()+random0to9()+" "
								   +random0to9()+random0to9()+random0to9());
			currentUser.setCCN(""+random0to9()+random0to9()+random0to9()+random0to9()+" "
								 +random0to9()+random0to9()+random0to9()+random0to9()+" "
								 +random0to9()+random0to9()+random0to9()+random0to9()+" "
								 +random0to9()+random0to9()+random0to9()+random0to9()+" ");
			String dt = getRandomBirthDate(); // dd.mm.yyyy
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
			
			
			try {
				currentUser.setBirthDate(sdf.parse(dt));
			} catch (ParseException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		
			currentUser.setPESEL(getPesel2(dt,currentUser));
			
			return currentUser;
		}
		
		private String getPesel2(String dt,User currentUser){
			String pesel = "";
	
			int numerPlec = 0;
	
			String[] dropped =dt.split("");
			String day = dropped[0]+dropped[1];
			String month = dropped[3]+dropped[4];
			String year = dropped[8]+dropped[9];
			int number7 =  random0to9();
			int number8 =  random0to9();
			int number9 =  random0to9();
			
		
			
			if(currentUser.getSex().equals(UserSex.SEX_FEMALE)){
				numerPlec=(new Random().nextInt(5))*2;
			}else if(currentUser.getSex().equals(UserSex.SEX_MALE)){
				numerPlec = 1+(new Random().nextInt(5))*2;
			}
				int sumaKont =	9*Integer.parseInt(dropped[0])+ 
							7*Integer.parseInt(dropped[1])+ 
							3*Integer.parseInt(dropped[3])+ 
							1*Integer.parseInt(dropped[4])+ 
							9*Integer.parseInt(dropped[8])+
							7*Integer.parseInt(dropped[9])+ 
							3*number7+ 
							1*number8+ 
							9*number9+ 
							7*numerPlec;
			
				int numerKontrol=sumaKont%10;
				
			pesel = ""+year+month+day+number7+number8+number9+numerPlec+numerKontrol;
		//	System.out.println(numerPlec);
			
			return pesel;
		}
	
		public int random0to9() {
			return new Random().nextInt(10);
		}
		public int random1to9() {
			int ran= 1+ new Random().nextInt(9);
			return ran;
		}
		
		private String getRandomBirthDate(){
			int[] daysInMonths ={31,28,31,30,31,30,31,31,30,31,30,31};
			int year = 1890 + new Random().nextInt(120);
			int month = new Random().nextInt(12)+1;
			
			if((year % 4==0 && year % 100!=0) || year % 400==0){
				daysInMonths[1]++;
			}
			int day = new Random().nextInt(daysInMonths[month-1])+1;
			return leadZero(day)+"."+leadZero(month)+"."+year;
		}
		
		
		private String leadZero(int arg){
			if(arg<10)
				return "0" + arg;
			 return "" +arg;
		}
		
	private Address getRandomAddress() {
		Address adr = new Address();
		String[] cityData = getrandomLineFromFile("city.txt").split("\t");
		adr.setCountry("Poland");
		adr.setCity(cityData[0]);
		adr.setZipcode(cityData[1]);
		adr.setStreet("ul."+getrandomLineFromFile("street.txt"));
		adr.setNumber(""+(new Random().nextInt(100)+1));
		return adr;
	}
	
	

	private int countLines(String filename){
		int lines = 0;
		try(Scanner sc = new Scanner(new File(path + filename))){
			while(sc.hasNextLine()){
				lines++;
				sc.nextLine();
			}
		}catch(FileNotFoundException e){
			System.out.println(e.getMessage());
		}
		return lines;
		}

	public String getrandomLineFromFile(String filename) {
		int allLines = countLines(filename);
		int line = new Random().nextInt(allLines) + 1;

		int currLine = 1;
		String currentLineContent = "";
		try (Scanner sc = new Scanner(new File(path + filename))) {
			while (sc.hasNextLine()) {
				currLine++;
				currentLineContent = sc.nextLine();
				if (line == currLine) {
					return currentLineContent;
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		return null;
	}

}
