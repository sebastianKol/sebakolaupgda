package UserGen;

public enum UserSex {
	SEX_MALE("Mezczyzna"),
	SEX_FEMALE("Kobieta"),
	SEX_UNDEFINED("Nieokreslono");
	
	private String name;
	
	UserSex(String name){
		this.name=name;
	}
	public String getName(){
		return name;
	}
}
