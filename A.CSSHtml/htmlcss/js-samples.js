var obiektPawla = {
  klucz: "wartość",
  imie: "pawel",
  nazwisko: "testowy"
};

if(obiektPawla['klucz']) {
  console.log("klucz isntieje w obiekcie");
}

if(obiektPawla['superInnyKluczDeLuxe']) {
  console.log("mamy superklucz ");
} else {
  console.log("nie mamy superklucza");
}

var myKeys = Object.keys(obiektPawla);

for(let i = 0; i < myKeys.length; i++) {
  console.log(myKeys[ i ], obiektPawla[ myKeys[ i ] ]  );
}

var Translator = function() {
  this.pairs = {};
  
  this.addPair = function(pair) {
    if(pair instanceof Array) {
      this.pairs[ pair[0] ] = pair[1];
    } else if(pair instanceof Object) {
      var keys = Object.keys(pair);
      for(let elem of keys) {
        this.pairs[ elem ] = pair[ elem ];
      }
    }
  }
  
  this.showPairs = function() {
    var keys = Object.keys(this.pairs);
    for(let e of keys) {
      console.log(e + " = " + this.pairs[e]);
    }
  }
  
  this.translate = function(arg) {
    var keys = Object.keys(this.pairs);
    for(let i = 0; i < keys.length; i++) {
      arg = arg.split(keys[i]).join(this.pairs[keys[i]]);
    }
    return arg;
  }
}

var trans = new Translator();
trans.addPair(['poszedł', 'went']);
trans.addPair({ 'do': 'to'});
trans.addPair({ 'lasu': 'forest', latać : 'fly' });
console.log(trans.translate("Paweł poszedł do lasu."));




imie = "pawel"; // zmienna globalna
var number = 5; // zmienna lokalna
let anotherNumber = 123.12;

console.log(typeof number);

number = "test";

console.log(typeof number);

var a = "5";
var b = 5;

if(a == b) {
  console.log("Liczby są takie same!");
}

// operator === porównuje także typy danych
if(a === b) {
  console.log("Liczby są takie same co do wartości i typu danych.");
}

for(let i = 0; i < 5; i++) {
  console.log(i, i * 2);
}

//console.log(i);

var arr = [ 1, 5, 10 ];

arr.push(15);

for(let i in arr) {
  console.log(arr[i]);
}

for(let elem of arr) {
  console.log(elem);
}

// komentarz liniowy

/* komentarz blokowy */

function add(a, b) {
  return a + b;
}

console.log( "3 + 4 = " + add(3, 4) );

// przypisanie f-cji anonimowej do zmiennej
var substract = function(a, b) {
  return a - b;
};

console.log("10 - 3 = " + substract(10, 3) );

var myString = 'mój st\'\'\'\'\'\'\'ring';
var myStr2 = "asdasd'''''''''asdas";

var c = 2;

switch(c) {
  case 1: 
    console.log("jedynka");
    break;
  case 2: 
    console.log("dwójka");
    break;
}

var diff = substract(20, 5);

console.log( diff % 3 );

function isEven(number) {
  if(typeof number == "number" && number % 2 === 0 ) {
    return true;
  }
  return false;
}

var ret = isEven(15);

console.log(ret);

var myArray = [ true, 1, "pawel", [ "test", 3, [ 5 ], 14 ], 4 ];

/* Napisz funkcje, ktora doda wszystkie zelementy tablicy typu number i zwróci ich sumę - reszta elementów powinna zostać pominięta. Zwróć uwagę na zagnieżdzenie tablic. */

var obj = {};

console.log(typeof myArray, typeof obj);
console.log(myArray instanceof Array);
console.log(obj instanceof Array);

function sumNumbers(arg) {
  var sum = 0;
  if(arg instanceof Array) {
    for(let elem of arg) {
      if(typeof elem === "number") {
        sum += elem;
      } else if(elem instanceof Array) {
        sum += sumNumbers(elem);
      }
    } 
  }
  return sum;
}
console.log("Suma wynosi: " + sumNumbers(myArray) ); 
/* Napisz funkcję, która sprawdzi, czy wszystkie elementy
 * tablicy (oprócz ostatniego) po zsumowaniu są równe 
 * ostatniemu elmentowi w tablicy */
 
var arrA = [ 1, 12.4, 2.6, 16 ] // true, bo: 1 + 12.4 + 2.6 == 16 === 16 
var arrB = [ 5, 4, 10 ] // false, 5 + 4 == 9 <> 10

function checkArray(arg) {
  var sum = 0;
  if(arg instanceof Array) {
    
    for(let elem of arg) {
      if(typeof elem === "number")
        sum += elem;
    }
  }
  return (sum - arg[arg.length - 1] === arg[arg.length - 1]);
}

console.log(checkArray(arrA), checkArray(arrB));

// var x = 0;
// var z = 1;

// console.log(!!x, !!z);

var simpleArray = [ 1, 2, 3 ];
var simpleStr = "przykladowy ciag znakow";

console.log( simpleStr.split("").join("_"));

/* Napisz funkcję, w której dla zadanego łańcucha 
 * znaków, wszystkie znaki - takie same jak
 * pierwsza litera ciągu znaków zostaną zamienione 
 * na znak '_', wyjątkiem jednak jest pierwszy
 * znak. 

 * Dla przykładu:
 * Wejście: oksymoron
 * Wyjście: oksym_r_n 
*/
console.log( simpleStr.charAt(0) );
arrA.shift();


var jakisString = "pawel";
jakisString[0] = "g";
console.log(jakisString);

function replaceChars(arg) {
  var firstLetter = arg.charAt(0);
  var ret = arg.split(firstLetter);
  console.log(ret);
  ret.shift();
  return firstLetter + ret.join("_");
}

console.log(replaceChars("oksymoron"));

function multiply() {
  var sum = 1;
  for(let elem of arguments) {
    sum *= elem;
  }
  return sum;
}

console.log( multiply(2, 5, 10) );

function simpleFunction(arg) {
  if(arg !== undefined)
    console.log(arg);
}

simpleFunction(2);

var xx = "a123";

var zz = parseInt(xx, 10);

console.log(isNaN(zz));

var myObject = {
  imie: "pawel",
  "nazwisko" : "testowy",
  tablica : [ 1, 2, 3 ],
  obj : { name: "innyName" },
  klucz: 'magic key',
  make: function() {
    console.log("Witaj " + this.imie);
  }
};

var objKeys = Object.keys(myObject);
console.log(objKeys);

var klucz = "imie";
console.log("-- " + myObject.klucz);
console.log(myObject.nazwisko, myObject['tablica']);
console.log(myObject.obj.name);
console.log(myObject['obj'].name);
console.log(myObject.obj['name']);
console.log(myObject[klucz]);
myObject.make();

var Dice = function(arg) {
  this.rolls = arg || 10;
  this.roll = function(max) {
    max = max || 6;
    // <0, 1)
    return Math.floor(Math.random() * max) + 1;
  }
  this.test = function(max) {
    for(let i = 0; i < this.rolls; i++) {
      console.log("Wypadlo: " + this.roll(max));
    }
  }
}

var myDice = new Dice(3);
myDice.test();
console.log("--------------");
var myDice2 = new Dice();
myDice2.test(0);
