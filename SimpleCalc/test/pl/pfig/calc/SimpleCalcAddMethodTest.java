package pl.pfig.calc;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;
import pl.org.pfig.calc.SimpleCalc;

public class SimpleCalcAddMethodTest {
	
	private SimpleCalc sc;
	
	@Before
	public void doStuff(){
		sc = new SimpleCalc();
	}

	@Test
	public void whenToPositiveNumbersAreGivenPositiveNumberAreSum() {
		int a = 3;
		int b = 6;
		//SimpleCalc sc = new SimpleCalc(); // dodane private SimpleCalc
		// sc.add(a,b);
		assertEquals(9, sc.add(a, b));
	}
	
	@Test(expected=Exception.class)
	public void whenExThrowMethodIsUsedExceptionIsThrown() throws Exception{
	//	SimpleCalc sc = new SimpleCalc();
		sc.exThrow();
	}
	
	@Test
	public void whenToNegativeNumbersAreGivenNumberAreSum() {
		int a = -3;
		int b = -6;
	//	SimpleCalc sc = new SimpleCalc();
		// sc.add(a,b);
		assertEquals(-9, sc.add(a, b));
	}
	
	@Test
	public void whenFirstPositiveNumberIsGivenAndSecondNegativeNumberIsGivenNumberAreSum() {
		int a = 3;
		int b = -6;
	//	SimpleCalc sc = new SimpleCalc();
		// sc.add(a,b);
		assertEquals(-3, sc.add(a, b));
	}
	
	@Test
	public void whenFirstNegativeNumberIsGivenAndSecondPositiveNumberIsGivenNumberAreSum() {
		int a = -3;
		int b = 6;
	//	SimpleCalc sc = new SimpleCalc();
		// sc.add(a,b);
		assertEquals(3, sc.add(a, b));
	}
	
	@Test
	public void whenTwoMaxIntegersAreGivenPositiveNumberAreExpected() {
	//	SimpleCalc sc = new SimpleCalc();
		Assert.assertTrue("Out of Range", sc.add(Integer.MAX_VALUE, Integer.MAX_VALUE)+5>0 ); // po dodaniu ponad max , wychodzi poza zakres i zmienia znak na minus.
	}
}
