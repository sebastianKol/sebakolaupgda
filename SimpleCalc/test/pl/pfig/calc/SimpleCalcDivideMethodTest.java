package pl.pfig.calc;

import static org.junit.Assert.*;

import org.junit.Test;

import pl.org.pfig.calc.SimpleCalc;

public class SimpleCalcDivideMethodTest {

	@Test
	public void whenDivideBigerNumberBySmallers() {
		double a = 10;
		double b = 5;
		SimpleCalc sc = new SimpleCalc();
		assertEquals(2.0, sc.divide(a, b));
	}

	@Test
	public void whenDivideSmallerrNumberByBiger() {
		double a = 5;
		double b = 10;
		SimpleCalc sc = new SimpleCalc();
		assertEquals(0.5, sc.divide(a, b));
	}
}
