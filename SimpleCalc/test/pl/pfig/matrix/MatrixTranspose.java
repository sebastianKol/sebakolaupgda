package pl.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import pl.org.matrix.Matrix;

public class MatrixTranspose {
	
	Matrix m;

	@Before
	public void init() {
		m = new Matrix();
	}


	
	
	@Test
	public void whenArrayGivenTransposeIt() {
		int[][] actual = { { 1, 0, 3, 7 }, { 0, 1, 0, 0} };
	
		int[][] expected = {{1,0},{0,1},{3,0},{7,0}};
		
				assertArrayEquals(expected, m.transpose(actual));
	}

	
	
	
}
