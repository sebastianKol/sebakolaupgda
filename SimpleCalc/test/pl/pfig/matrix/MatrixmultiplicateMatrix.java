package pl.pfig.matrix;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import pl.org.matrix.Matrix;

public class MatrixmultiplicateMatrix {
	Matrix m;

	@Before
	public void init() {
		m = new Matrix();
	}
	
	
	@Test
	public void whenArrayAndIntGivenMultiplyByInt() {
		int[][] actual = { { 1, 0 }, { 0, 1 } };
		int a = 3;
		int[][] expected = {{3,0},{0,3}};
		
				assertArrayEquals(expected,m.multiplicateMatrix(actual, a));
	}

}
