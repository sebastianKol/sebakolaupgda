package pl.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pl.org.matrix.Matrix;

public class MatrixSubstractMatrix {
	Matrix m;

	@Before
	public void init() {
		m = new Matrix();
	}

	@Test
	public void whenTwoArrayGivenSubstractArrays(){
	
	int[][] actual1 = { { 1, 0 }, { 0, 1 } };
	int[][] actual2 = { { 1, 2 }, { 0, 2 } };
	int[][] expected = {{0,-2},{0,-1}};
	
			assertArrayEquals(expected, m.substractMatrix(actual1, actual2));
}
	@Test(expected=Exception.class)
	public void whenTwoArrayGivenSubstractArrayExceptionIsThrown(){
		int[][] actual1 = { { 1, 0 }, { 0, 1 , 0} };
		int[][] actual2 = { { 1, 2 }, { 0, 2 } };
		
		m.addMatrix(actual1 , actual2);
	
	}
	
}
