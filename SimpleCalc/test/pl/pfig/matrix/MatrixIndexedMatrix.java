package pl.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pl.org.matrix.Matrix;

public class MatrixIndexedMatrix {
	Matrix m;

	@Before
	public void init() {
		m = new Matrix();
	}

	@Test
	public void whenIndexedMatrixExpected() {
		int[][] expected = { { 1, 2 }, { 3, 4 } };
		int[][] actual = new int[2][2];
		assertArrayEquals(expected, m.indexedMatrix(actual));
	}

}
