package pl.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pl.org.matrix.Matrix;

public class MatrixEqualDimensionMatrix {
	Matrix m;

	@Before
	public void init() {
		m = new Matrix();
	}

	@Test
	public void whenEmptyArrayGivenCheckDimensionsOfArrays() {
		int[][] expected = new int[2][2];
		int[][] actual = new int[2][2];
	
		assertTrue(m.isEqualDimension(actual, expected));
	}

}
