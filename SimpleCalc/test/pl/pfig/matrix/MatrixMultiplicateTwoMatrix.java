package pl.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import pl.org.matrix.Matrix;

public class MatrixMultiplicateTwoMatrix {
	
	Matrix m;

	@Before
	public void init() {
		m = new Matrix();
	}

	@Test
	public void whenTwoArraysIsGivenMultiplyThem() {
		int[][] actual1 = { { 1, 0 }, { 0, 1 } };
		int[][] actual2 = { { 1, 2 }, { 0, 2 } };
		
		int[][] expected= { { 1, 2 }, { 0, 2 } };
		
		assertArrayEquals(expected, m.multiplicateTwoMatrix(actual1, actual2));
	}

	@Test
	public void whenTwoArraysIsGivenMultiplyThemExceptionIsThrown(){
		int[][] actual1 = { { 1, 0 }, { 0, 1 } };
		int[][] actual2 = { { 1, 2,}, { 0, 2 } };
		
		
		IllegalArgumentException iae = null;
		
		try {
			m.multiplicateTwoMatrix(actual1, actual2);
		} catch(IllegalArgumentException e) {
			iae = e;
		}
		
		
		assertNull("Wyjatek wystapil", iae);
	
	}
}
