package pl.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import pl.org.matrix.Matrix;

public class MatrixIsSymetric {
	
	Matrix m;

	@Before
	public void init() {
		m = new Matrix();
	}

	@Test
	public void whenArrayGivenCheckIfSymetric() {
		int[][] actual = { { 1, 0 }, { 0, 1, 0} };
	
	
		//assertTrue(m.isSymetric(actual));
		assertFalse(m.isSymetric(actual));
		
	}

}
