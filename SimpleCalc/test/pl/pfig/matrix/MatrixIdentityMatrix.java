package pl.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pl.org.matrix.Matrix;

public class MatrixIdentityMatrix {
	Matrix m;

	@Before
	public void init() {
		m = new Matrix();
	}

	@Test
	public void whenEmptyArrayGivenIdentityMatrixExpected() {
		int[][] expected = { { 1, 0 }, { 0, 1 } };
		int[][] actual = new int[2][2];
		assertArrayEquals(expected, m.identityMatrix(actual));
	}

	@Test
	public void whenEmptyArrayGivenIdentityMatrixExpectedByValue() {
		int[][] expected = { { 1, 0 }, { 0, 1 } };
		int[][] actual = new int[2][2];
		actual = m.identityMatrix(actual);

		for (int i = 0; i < actual.length; i++) {
			for (int j = 0; j < actual[i].length; j++) {
				assertEquals(expected[i][j], actual[i][j]);
			}
		}
	}

	// sprawdzanie wymiarow tablicy
	@Test
	public void whenEmptyArrayGivenCheckDimensionsOfArrays() {
		int[][] expected = { { 1, 0 }, { 0, 1} };
		int[][] actual = new int[2][2];
		actual = m.identityMatrix(actual);
		
		assertTrue(actual.length == expected.length);

		for (int i = 0; i < actual.length; i++) {
				assertTrue(actual[i].length == expected[i].length);
			
		}
	}
}
