package pl.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pl.org.matrix.Matrix;

public class MatrixAddMatrix {
	Matrix m;

	@Before
	public void init() {
		m = new Matrix();
	}


	@Test
	public void whenTwoArrayGivenAddArrays(){
	
	int[][] actual1 = { { 1, 0 }, { 0, 1 } };
	int[][] actual2 = { { 1, 2 }, { 0, 2 } };
	int[][] expected = {{2,2},{0,3}};
	
			assertArrayEquals(expected, m.addMatrix(actual1, actual2));
}
	
	
	@Test(expected=Exception.class)
	public void whenTwoArrayGivenAddArrayExceptionIsThrown(){
		int[][] actual1 = { { 1, 0 }, { 0, 1 } };
		int[][] actual2 = { { 1,  }, { 0, 2 } };
		
		m.addMatrix(actual1 , actual2);
	
	}
}
