package connector;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by RENT on 2017-07-14.
 */
public class HTTPConnector {
    public static void main(String[] args) throws IOException {

        HTTPConnector hc = new HTTPConnector();

        System.out.println(hc.sendGET("http://palo.ferajna.org/sda/wojciu/json.php"));
        System.out.println(hc.POST("http://palo.ferajna.org/sda/wojciu/json.php","login=admin"));


    }

    private URL obj;
    private HttpURLConnection con;
    private String ua;

    public String sendGET (String url) throws IOException {
        obj = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", ua);

        int responseCode = connection.getResponseCode();
        String result = "";
        if (true) {                           // mozna sprawdzac czy inne kody 2XX sa ok
            InputStream res = connection.getInputStream();
            InputStreamReader reader = new InputStreamReader(res);
            Scanner sc = new Scanner(reader);

            while (sc.hasNextLine()) {
                result += sc.nextLine();
            }
            sc.close();
        }

        return result;
    }


    public String POST(String url, String params) throws IOException {

        obj = new URL(url);
        HttpURLConnection conPOST = (HttpURLConnection) obj.openConnection();
        String postUA = "Pawel/2.0"; // typ przegladarki - nie ma odwzorowania w rzeczywistosci

        conPOST.setRequestMethod("POST");
        conPOST.setRequestProperty("User-Agent", postUA);

        /*  tylko dla POST */
        conPOST.setDoOutput(true);

        DataOutputStream dos = new DataOutputStream(conPOST.getOutputStream());
        dos.writeBytes(params);
        dos.flush();
        dos.close();
        /* koniec tylko dla POST */

        Scanner sc = new Scanner(new InputStreamReader(conPOST.getInputStream()));
        String lines = "";
        while(sc.hasNextLine()) {
            lines += sc.nextLine();
        }
        sc.close();

        return lines;
    }

}


