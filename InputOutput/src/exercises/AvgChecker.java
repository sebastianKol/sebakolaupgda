package exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;

public class AvgChecker {

	private String filename;
	
	public  AvgChecker(String filename){
		this.filename = filename;
	}
	
	public void process(){
		String currentLine = "";
		File f = new File("Resources/" + filename);
		double avg=0;
		
		LinkedList<String> fileContent = new LinkedList<>();
		
		try {
			Scanner sc = new Scanner(f);
			
			while(sc.hasNextLine()) {
				currentLine = sc.nextLine();
			
					if(!currentLine.equals("")){  // linia nie moze byc pustym ciagniem znakow
						fileContent.add(currentLine);
						avg+=countAvgFromLines(currentLine);	
						
					}
			}
			avg/=fileContent.size();    // dzielenie     avd/=4    to avd = avd/4
			sc.close(); // przez zapisem po odczycie zamknac
			
			
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			
			
			
			for(String line: fileContent){
				if(countAvgFromLines(line)>avg){
					pw.println(line);
				}
				
			
			}
			
			
			
			pw.close();
			} catch (FileNotFoundException e) {
			System.out.println("File not found.");
		}
	
		
	}
	private double countAvgFromLines(String line){

		String[] marks = line.split("\t");
		double marksSum=0;
		for(int i=1; i<marks.length; i++){
			marksSum+= Double.parseDouble(marks[i]);
		}
		return marksSum / (marks.length-1);	
	}
	
}

// Metoda process()powinna odczyta� wszystkie warto�ci z pliku przekazanego jako
// parametr konstruktora,
// a nast�pnie nadpisa� plik w taki spos�b, aby znalaz�y si� w nim warto�ci
// powy�ej �redniej.
//�redni� nale�y liczy� poziomo. wed�ug kolumn, kt�re s� rozdzielone znakiem tabulacji.
//Przyk�ad:Pawe� Testowy562
//Daje �redni� 4,33(3). Je�eli �rednia wszystkich �rednich z pliku b�dzie mniejsza ni� 4,33(3)
//taki wpis powinien pozosta� w pliku.