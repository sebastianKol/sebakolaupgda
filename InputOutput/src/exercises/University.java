package exercises;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

public class University {
	
	private HashMap<Integer, Student> students = new HashMap<>();
	
	private boolean isStudentExists(int index) {
		return students.containsKey(index);
	}
	
	public Student getStudent(int index) throws FileNotFoundException {
		Scanner sc = new Scanner(new File("resources/students.txt"));
		String currentLine;
		while(sc.hasNextLine()) {
			currentLine = sc.nextLine();
			String[] userFileds = currentLine.split("\t");
			//13	Paulina	Nowak	Pielegniarstwo	5.0
			if(Integer.parseInt(userFileds[0]) == index)
				return new Student(index, userFileds[1], userFileds[2], userFileds[3], Double.parseDouble(userFileds[4]));
		}
		return null;
	}
	
	public void putStudent(Student student) {
		
	}
}
