package exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MoneyConverter {
	
	private final String filename = "Resources/currency.txt";
	
	public double readCourse(String currency){
		File f = new File(filename);
		currency = currency.toUpperCase();
		
		try {
			String currentLine="";
			Scanner sc = new Scanner(f);
			while(sc.hasNextLine()){
				currentLine = sc.nextLine();
				String[] temp = currentLine.split("\t"); // dodac slascha
				String[] values = temp[0].split(" ");
				if(values[1].equalsIgnoreCase(currency)){
					return Double.parseDouble(temp[1].replace(",", "."))/Integer.parseInt(values[0]);
				}
			}
			
			
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		return 0;
		
		
	}
	
	
	public double convert(double money, String to) {

	
	return money/readCourse(to);
	
	}
	
	
	public double convert(double money, String from, String to){
		
		return money*((money/readCourse(from))/(money/readCourse(to)));
		
	}

	
}
