package exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class LengthChecker {

	private final String path = "Resources/";

	public String[] readFile(String filename) {
		String[] ret = new String[countLines(filename)];
		File f = new File(path + filename);

		try {
			String currentLine;
			Scanner sc = new Scanner(f);
			int i = 0;
			while (sc.hasNextLine()) {
				currentLine = sc.nextLine();
				ret[i++] = currentLine;
			}
			sc.close();

		} catch (FileNotFoundException e) {
			System.out.println("File not found.");
		}

		return ret;
	}

	private void writeFile(String[] fileContent, int len) {
		File f = new File("Resources/words_" + len + ".txt");

		try {
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			for (int i = 0; i < fileContent.length; i++) {
				if (isProperLength(fileContent[i], len)) {
					pw.println(fileContent[i]);
				}
			}
			pw.close();

		} catch (FileNotFoundException e) {
			System.out.println("File not found.");
		}
	}

	public void make(String fileInput, int len){
		
		writeFile(readFile(fileInput), len);
		
	}

	private boolean isProperLength(String arg, int len) {
		return arg.length() > len;
	}

	private int countLines(String filename) { // liczenie ilosci lini w pliku
		File f = new File(path + filename);
		int lines = 0;
		try {
			Scanner sc = new Scanner(f);
			while (sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return lines;
	}
}
