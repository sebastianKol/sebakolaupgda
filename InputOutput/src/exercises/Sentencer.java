package exercises;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class Sentencer extends Sentence{

	// +writeSentence(filename:String, sentence:String):void
	
	public void writeSentence(String filename, String sentence) {
		File f = new File("Resources/" + filename);
		
		try {
			// Domyslnie FOS nadpisuje ca�y plik
			FileOutputStream fos = new FileOutputStream(f);
			// Jezeli chcemy dopisywac do pliku (tryb append), to musimy przekazac drugi parametr 
			// do konstruktora FOS np.
			// FileOutputStream fos = new FileOutputStream(f, true);
			
			PrintWriter pw = new PrintWriter(fos);
			
			pw.println(sentence);
			pw.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("File not found.");
		}
	}
}
//
//Operacje wej�cia / wyj�cia�wiczenie 1Napisz klas� Sentence,
//kt�ra b�dzie zawiera�a metod�:+readSentence(filename:String):String
//odczytuj�c� kolejne linie z pliku tekstowego i zwracaj�c� je jako zdanie 
//(pierwsza litera du�a, na ko�cu kropka). Wykorzystaj plik test.txtz pakietu zad1.
//Obs�u� wyj�tek FileNotFoundExceptionw metodzie readSentence()
//�wiczenie 2Napisz klas� Sentencerb�d�c� rozszerzeniem klasy Sentencei dostarczaj�c� metod�
//:+writeSentence(filename:String, sentence:String):voidkt�ra zapisze do pliku tekstowego 
//przekazany ci�g znak�w. 
//Plik zapisz w pakiecie zad2pod nazw� mySentence.txt