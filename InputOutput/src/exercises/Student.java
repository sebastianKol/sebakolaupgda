package exercises;

public class Student {
	int index;
	String name;
	String secondName;
	String course;
	double averageMark;
	
	public Student(){
		
	}
	
	public Student(int index, String name, String secondName, String course, double averageMark) {
		super();
		this.index = index;
		this.name = name;
		this.secondName = secondName;
		this.course = course;
		this.averageMark = averageMark;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public double getAverageMark() {
		return averageMark;
	}

	public void setAverageMark(double averageMark) {
		this.averageMark = averageMark;
	}

	@Override
	public String toString() {
		return "Student [index=" + index + ", name=" + name + ", secondName=" + secondName + ", course=" + course
				+ ", averageMark=" + averageMark + "]";
	}
	
	
	

}




//kt�ra b�dzie dostarcza�a pola:-index:int-name:
//	String-secondName:String-course:String-averageMark:double
//oraz konstruktory(bez i sparametryzowany), getteryi settery.
//Nast�pnie utw�rz klas� University, kt�ra b�dzie zawiera�a nast�puj�ce 
//metody:-isStudentExists(index:int):boolean+getStudent(index:int):Student+putStudent(student:Student)
//:voidKlasa Univeristypowinna dzia�a� na pliku students.txtznajduj�cym si� w pakiecie zad6.
//Format pliku powinien by� nast�puj�cy (bez u�ywania serializacji obiektu)
//:123Pawe�TestowyAlgorytmy4.12Warto�ci powinny by� rozdzielone znakiem tabulacji "\t".
//Metoda getStudent()powinna zwraca� now� instancj� klasy Studentz uzupe�nionymi danymi studenta wed�ug indeksu,
//a metoda putStudent()dodawa� studenta na ko�cu listy, w przypadku, gdy taki student nie istnieje.
//Do sprawdzenia wyst�powania studenta o zadanym indeksie wykorzystaj prywatn� metod� isStudentExists()