package exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class TempConverter {

	private final String path = "Resources/";

	private int countLines(String filename) {
		File f = new File(path + filename);
		int lines = 0;
		try {
			Scanner sc = new Scanner(f);
			while (sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return lines;
	}

	public double[] readTemp(String filename) {

		double[] ret = new double[countLines(filename)];
		File f = new File(path + filename);

		try {
			String currentLine;
			Scanner sc = new Scanner(f);
			int i = 0;
			while (sc.hasNextLine()) {
				currentLine = sc.nextLine();
				ret[i++] = Double.parseDouble(currentLine.replace(",", "."));
			}

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		return ret;
	}

	public double toKelvin(double temp) {
		temp = temp + 273.15;
		return temp;
	}

	public double toFarenheit(double temp) {
		temp = (temp * 1.8) + 32;
		return temp;
	}

	
	
	
	
	
	public void writeTemp(double[] temp) { // zapis do plikow farencheit i pliku
											// kelvin
		//File far = new File("Resources/tempF.txt");
		//File kal = new File("Resources/tempK.txt");

		try {

			FileOutputStream fosF = new FileOutputStream("Resources/tempF.txt");
			FileOutputStream fosK = new FileOutputStream("Resources/tempK.txt");
			
			PrintWriter pwF = new PrintWriter(fosF);
			PrintWriter pwK = new PrintWriter(fosK);
			
			for(double c: temp){
				pwK.println(toKelvin(c));
				pwF.println(toFarenheit(c));
				
			}
			pwF.close();
			pwK.close();
			
		
	

		} catch (FileNotFoundException e) {
			System.out.println("File not found.");
		}
	}
}

// +toFahrenheit(temp:double):temp+readTemp(filename:String):double[]+writeTemp(temp:double[]):
// voidW pliku tekstowym tempC.txtw pakiecie zad4znajduj� si� poszczeg�lne
// temperatury dla danych miesi�cy
// zapisane jako temperatura w Celciusach.
// Do plik�w tempK.txtoraz tempF.txtzapisz (w pakiecie zad3) odpowiednio wyniki
// przekonwertowanej temperatury
// w Kelvinach i Fahrenheitach.