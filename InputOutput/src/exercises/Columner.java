package exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;

public class Columner {

	private final String path = "Resources/";
	private String filename;
	private double[] currentColumn;
	private LinkedList<String> fileContent = new LinkedList<>();

	public Columner() {

	}

	public Columner(String filename) {
		this.filename = filename;
	}

	private void readFile() {
		if (fileContent.size() == 0) {
			File f = new File(path + filename);
			try {
				Scanner sc = new Scanner(f);
				while (sc.hasNextLine()) {
					fileContent.add(sc.nextLine());
				}
				sc.close();
			} catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public double sumColumn(int column) throws WrongColumnException {
		readColumn(column);
		double sumCol = 0;
		for (int i = 0; i < currentColumn.length; i++) {
			sumCol += currentColumn[i];
		}
		return sumCol;

	}

	public double avgColumn(int column) throws WrongColumnException {
		readColumn(column);
		double avgCol = sumColumn(column) / currentColumn.length;

		return avgCol;
	}

	public int countColumn(int column) throws WrongColumnException {
		readColumn(column);
		int countCol = currentColumn.length;

		return countCol;
	}

	public double maxColumn(int column) {
		double maxCol = currentColumn[0];
		for (int i = 1; i < currentColumn.length; i++) {
			if (maxCol < currentColumn[i]) {
				maxCol = currentColumn[i];
			}
		}
		return maxCol;
	}

	public double minColumn(int column) {
		double maxCol = currentColumn[0];
		for (int i = 1; i < currentColumn.length; i++) {
			if (maxCol > currentColumn[i]) {
				maxCol = currentColumn[i];
			}
		}
		return maxCol;
	}

	public double[] readColumn(int column) throws WrongColumnException {
		readFile();
		int maxColNum = fileContent.get(0).split("\t").length;
		if (column >= 1 && column <= maxColNum) {
			currentColumn = new double[fileContent.size()];
			int i = 0;
			for (String line : fileContent) {
				String[] cols = line.split("\t");
				currentColumn[i++] = Double.parseDouble(cols[column - 1].replace(",", "."));
			}
		} else {
			throw new WrongColumnException("Bad column value");
		}
		return currentColumn;
	}

	public void writeColumn (String filename, int numberC) throws IllegalArgumentException{
		readFile();
		File f = new File("Resources/" + filename);
		try {
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
//dopisac zapis pliku
			
			
			
		} catch(FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
}

//
// Metoda writeColumn()powinna zapisywa� warto�ci wybranej kolumny do pliku.
// W przypadku braku danej kolumny (ilo�� kolumn w pliku mniejsza ni� przekazany
// argument)
// wyrzu� wyj�tek IllegalArgumentException
//
