import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

/**
 * Created by Mori on 2017-07-04.
 */
public class Game {
    LinkedList<Card> allCards = new LinkedList<>();
    Queue<Card> playerCardsinHand = new LinkedList();
    Queue<Card> oponentCardsinHand = new LinkedList();
    Queue<Card> playerTakenCards = new LinkedList();
    Queue<Card> oponentTakenCards = new LinkedList();
    Queue<Card> warQueue = new LinkedList();

    public void run() {
        Card currentPlayerCard = playerCardsinHand.peek();
        Card currentOponent = oponentCardsinHand.peek();

        // pokazuje wartosi kart graczy
        showCurrentCards(currentPlayerCard, currentOponent);

        int PlayerCardValue = currentPlayerCard.getCardValue();
        int OponentCardValue = currentOponent.getCardValue();

        war(PlayerCardValue, OponentCardValue);
    }

    private void war(int playerCardValue, int oponentCardValue) {
        boolean working = true;
        while (working) {

            if (playerCardValue < oponentCardValue) {
                oponentTakenCards.offer(playerCardsinHand.poll());
                oponentTakenCards.offer(oponentCardsinHand.poll());

                playerCheckIfHaveCards(playerCardsinHand,playerTakenCards);
                while (warQueue.size()!=0){
                for (int i = 0; i < warQueue.size(); i++) {
                    oponentTakenCards.offer(warQueue.poll());
                }
                }
                System.out.println("oponent ma wieksza karte");
                working=false;

            } else if (playerCardValue > oponentCardValue) {
                playerTakenCards.offer(playerCardsinHand.poll());
                playerTakenCards.offer(oponentCardsinHand.poll());

                oponentCheckIfHaveCards(oponentCardsinHand,oponentTakenCards);
                while (warQueue.size()!=0){
                for (int i = 0; i < warQueue.size(); i++) {
                    playerTakenCards.offer(warQueue.poll());
                }
                }
                System.out.println("player ma wieksza karte");
                working = false;

            } else if (playerCardValue == oponentCardValue) {
                System.out.println("wojna!!");
                //1 karta dodana do bitki
                warQueue.offer(playerCardsinHand.poll());
                warQueue.offer(oponentCardsinHand.poll());
                //2 zakryta karta dodana do bitki
                warQueue.offer(playerCardsinHand.poll());
                warQueue.offer(oponentCardsinHand.poll());
                playerCheckIfHaveCards(playerCardsinHand,playerTakenCards);
                oponentCheckIfHaveCards(oponentCardsinHand,oponentTakenCards);
                // 3 karta sprawdzone
                int thirthCardPlayerValue = playerCardsinHand.peek().getCardValue();
                int thirthCardOponentValue = oponentCardsinHand.peek().getCardValue();
                System.out.println(thirthCardOponentValue);
                System.out.println(thirthCardPlayerValue);
                //war again
                war(thirthCardPlayerValue, thirthCardOponentValue);

                // dodac licznik max 26 - remis

            }
        }
        run();
    }

    private Queue playerCheckIfHaveCards(Queue playerCardsinHand, Queue playerTakenCards) {
        if (playerCardsinHand == null && playerTakenCards != null) {
            playerCardsinHand = shuffleCards(playerTakenCards);
            playerTakenCards.clear();
        } else if (playerCardsinHand == null && playerTakenCards == null) {
            System.out.println("Player Przegral Gre");
            playerCardsinHand.clear();
            playerTakenCards.clear();
            oponentTakenCards.clear();
            oponentCardsinHand.clear();
//            Game game = new Game();
//            game.generateCards();
//            game.shuffleCardsFirstTime();
        }
        return playerCardsinHand;
    }

    private Queue oponentCheckIfHaveCards(Queue oponentCardsinHand, Queue oponentTakenCards) {
        if (oponentCardsinHand == null && oponentTakenCards != null) {
            oponentCardsinHand = shuffleCards(oponentTakenCards);
            oponentTakenCards.clear();
        } else if (oponentCardsinHand == null && oponentTakenCards == null) {
            System.out.println("Oponent Przegral Gre");
            playerCardsinHand.clear();
            playerTakenCards.clear();
            oponentTakenCards.clear();
            oponentCardsinHand.clear();
//            Game game = new Game();
//            game.generateCards();
//            game.shuffleCardsFirstTime();
        }
        return oponentCardsinHand;
    }



    private void showCurrentCards(Card currentPlayerCard, Card currentOponent) {
        CardDisplayer cardDysplier = new CardDisplayer();
        cardDysplier.cardDysplier(currentPlayerCard, "Karta Playera");
        cardDysplier.cardDysplier(currentOponent, "Karta Oponenta");
    }


    public void generateCards() {
        CardGenerator cardGenerator = new CardGenerator();
        cardGenerator.cardGenerate(allCards);

    }

    public Queue shuffleCards(Queue cardsToShuffle) {
        List<Card> shuffledCarts = new LinkedList();
        for (int i = 0; i < cardsToShuffle.size(); i++) {
            shuffledCarts.set(i, (Card) cardsToShuffle.poll());
        }
        Queue<Card> shuffledCartsQueue = new LinkedList();
        int size = shuffledCarts.size();
        Random random = new Random();
        for (int i = 0; i < cardsToShuffle.size(); i++) {
            int random4Shuffle = random.nextInt((size));
            shuffledCartsQueue.offer(shuffledCarts.get(random4Shuffle));
            size--;
        }
        return shuffledCartsQueue;
    }

    public void shuffleCardsFirstTime() {
        Random random = new Random();
        int iterate = allCards.size() / 2;
        int sizeOfPlayerList = allCards.size() - 1;
        int sizeOfOponentList = allCards.size() / 2 - 1;
        for (int i = 0; i < iterate; i++) {
            int playerRandom = random.nextInt((sizeOfPlayerList - 0) + 1) + 0;

            playerCardsinHand.offer(allCards.get(playerRandom));
            allCards.remove(playerRandom);
            sizeOfPlayerList--;
        }
        for (int i = 0; i < iterate; i++) {
            int oponentRandom = random.nextInt((sizeOfOponentList - 0) + 1) + 0;
            oponentCardsinHand.offer(allCards.get(oponentRandom));
            allCards.remove(oponentRandom);
            sizeOfOponentList--;
        }
//
//        System.out.println("player");
//        System.out.println(playerCardsinHand);
//        System.out.println(playerCardsinHand.size());
//        System.out.println("oponent");
//        System.out.println(oponentCardsinHand);
//        System.out.println(oponentCardsinHand.size());
//

    }
}







