/**
 * Created by Mori on 2017-07-04.
 */
public class Card {
    int name;
    int cardValue;

    public Card(int name, int cardValue) {
        this.name = name;
        this.cardValue = cardValue;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public int getCardValue() {
        return cardValue;
    }

    public void setCardValue(int cardValue) {
        this.cardValue = cardValue;
    }

    @Override
    public String toString() {
        return  name +" "+cardValue;
    }
}
