import java.util.*;

/**
 * Created by Mori on 2017-07-04.
 */
public class Main {

       public static void main(String[] args) {
           Game game = new Game();
           game.generateCards();  // klory 0,1,2,3 figury 2 do 14(AS)
           game.shuffleCardsFirstTime();

           try {
               Scanner scanner = new Scanner(System.in);
//               System.out.println("Ile razy symulować wojne ?");
//               int numberOfWars = scanner.nextInt();

//               for (int i=0; i<numberOfWars; i++){
//                   game.run();
//               }
               game.run();

           }catch (java.util.InputMismatchException e){
               System.out.println(e.getMessage());
           }

    }


}
