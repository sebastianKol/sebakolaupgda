import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

/**
 * Created by Mori on 2017-07-04.
 */
public class Game {
    LinkedList<Card> allCards = new LinkedList<>();

    Queue<Card> playerCardsinHand = new LinkedList();
    Queue<Card> oponentCardsinHand = new LinkedList();
    Queue<Card> playerTakenCards = new LinkedList();
    Queue<Card> oponentTakenCards = new LinkedList();

    int counterPlayerWins;
    int counterOponentWins;
    int counterWarInGame;
    // dopisac licznik na rozne rodzeje wojen jakie moga zajsc w grze



    public  void run() {
        Card currentPlayerCard = playerCardsinHand.peek();
        Card currentOponent = oponentCardsinHand.peek();
        // pokazuje wartosi kart graczy
       // showCurrentCards(currentPlayerCard, currentOponent);

        int PlayerCardValue = currentPlayerCard.getCardValue();
        int OponentCardValue = currentOponent.getCardValue();

        war(PlayerCardValue, OponentCardValue);

    }
            // zrobic ograniczenia jak koncza sie kolejki
            // zrobic ograniczenia jak gracz traci wszystkie karty  przegrywa - koniec 1 rozgrywni
            // zrobic ograniczenie jak gracze wywolaja wojne do koncza kart - remis - koniec 1 rozgrywki
    private void war(int playerCardValue, int oponentCardValue) {
        if (playerCardValue < oponentCardValue) {
            offerOponent2Cards();
            if (playerCardsinHand == null){
                playerCardsinHand = shuffleCards(playerTakenCards);
            }else if (playerCardsinHand==null && playerCardsinHand ==null){
                System.out.println("Player Przegral Gre");
                // dodac ponowne uruchomnienie gdy on poczatku z tasowniaem kart i ich tworzeniem
                oponentCardsinHand.clear();
                oponentTakenCards.clear();
                Game game = new Game();
                game.generateCards();
                game.shuffleCardsFirstTime();

                // dodac licznik wygrana Oponenta
            }
            System.out.println("player ma wieksza karte");
        } else if (playerCardValue > oponentCardValue) {
            offerPlayer2Cards();
            if (oponentCardsinHand == null){
                oponentCardsinHand = shuffleCards(oponentTakenCards);
            }else if(oponentCardsinHand==null&& oponentTakenCards==null){
                System.out.println("Oponent Przegral Gre");
                // dodac ponowne uruchomnienie gdy on poczatku z tasowniaem kart i ich tworzeniem
                playerCardsinHand.clear();
                playerTakenCards.clear();
                Game game = new Game();
                game.generateCards();
                game.shuffleCardsFirstTime();

                // dodac licznik wygrana Playera
            }

            System.out.println("oponent ma wieksza karte");
        } else if (playerCardValue == oponentCardValue) {
            System.out.println("wojna!!");
            Queue<Card> warQueue = new LinkedList();
            warQueue.offer(playerCardsinHand.poll());
            warQueue.offer(oponentCardsinHand.poll());
//
//            //zakrywam 2 karty sprawdzic czy ma karty
            warQueue.offer(playerCardsinHand.poll());
            warQueue.offer(oponentCardsinHand.poll());

            // sprawdzic czy ma karty
//            // podejrzec 3 karte i dokonac odpowieniego dodania do kolejki
            int thirthCardPlayerValue = playerCardsinHand.peek().getCardValue();
            int thirthCardOponentValue = oponentCardsinHand.peek().getCardValue();
            System.out.println(thirthCardOponentValue);
            System.out.println(thirthCardPlayerValue);
            // zrobic zalozenia przy pobieraniu kart jesl sie koncza w kolejce
//
            if (thirthCardOponentValue==thirthCardPlayerValue){
                    war(thirthCardPlayerValue,thirthCardOponentValue);
            }else  if (thirthCardOponentValue>thirthCardPlayerValue){
                    war(thirthCardPlayerValue,thirthCardOponentValue);

                    for (int i=0; i<warQueue.size(); i++ ){
                        oponentTakenCards.offer(warQueue.poll());
                    }
                    //dodanie kart przeciwnikowi
            }else if (thirthCardOponentValue<thirthCardPlayerValue){
                war(thirthCardPlayerValue,thirthCardOponentValue);
                    //dodanie kart graczowi

            }

        }
    }

    private void showCurrentCards(Card currentPlayerCard, Card currentOponent) {
        CardDisplayer cardDysplier = new CardDisplayer();
        cardDysplier.cardDysplier(currentPlayerCard, "Karta Playera");
        cardDysplier.cardDysplier(currentOponent, "Karta Oponenta");
    }

    private void offerOponent2Cards() {
        oponentTakenCards.offer(playerCardsinHand.poll());
        oponentTakenCards.offer(oponentCardsinHand.poll());
    }

    private void offerPlayer2Cards() {
        playerTakenCards.offer(playerCardsinHand.poll());
        playerTakenCards.offer(oponentCardsinHand.poll());
    }

    public void  generateCards() {
        CardGenerator cardGenerator = new CardGenerator();
        cardGenerator.cardGenerate(allCards);

    }
    public Queue shuffleCards(Queue cardsToShuffle) {
        List<Card> shuffledCarts = new LinkedList();
        for(int i=0; i<cardsToShuffle.size(); i++){
            shuffledCarts.set(i, (Card) cardsToShuffle.poll());
        }
        Queue<Card> shuffledCartsQueue = new LinkedList();
        int size = shuffledCarts.size();
        Random random = new Random();
        for (int i=0; i<cardsToShuffle.size(); i++){
            int random4Shuffle = random.nextInt((size));
            shuffledCartsQueue.offer(shuffledCarts.get(random4Shuffle));
            size--;
        }
        return shuffledCartsQueue;
    }

    public void shuffleCardsFirstTime() {
        Random random = new Random();
        int iterate = allCards.size()/2;
        int sizeOfPlayerList = allCards.size()-1;
        int sizeOfOponentList = allCards.size()/2-1;
        for (int i=0; i<iterate;i++){
            int playerRandom =  random.nextInt((sizeOfPlayerList - 0) + 1) + 0;

            playerCardsinHand.offer(allCards.get(playerRandom));
            allCards.remove(playerRandom);
            sizeOfPlayerList--;
        }
        for (int i=0; i<iterate;i++){
            int oponentRandom = random.nextInt((sizeOfOponentList - 0) + 1) + 0;
            oponentCardsinHand.offer(allCards.get(oponentRandom));
            allCards.remove(oponentRandom);
            sizeOfOponentList--;
        }


//        System.out.println("player");
//        System.out.println(playerCardsinHand);
//        System.out.println(playerCardsinHand.size());
//        System.out.println("oponent");
//        System.out.println(oponentCardsinHand);
//        System.out.println(oponentCardsinHand.size());


    }
    }







