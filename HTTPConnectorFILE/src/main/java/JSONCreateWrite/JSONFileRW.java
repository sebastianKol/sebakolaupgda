package JSONCreateWrite;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;

/**
 * Created by Mori on 2017-07-16.
 */
public class JSONFileRW {
    public static void main(String[] args) {

//        //create JSON
//        JSONObject obj = new JSONObject();
//        obj.put("name","Sebasian");
//        obj.put("location","Gdansk");
//
//        JSONArray list = new JSONArray();
//        list.add("JAVA");
//        list.add("JSP");
//        list.add("Others");
//
//        obj.put("courses",list);
//
//        try(FileWriter file = new FileWriter("myJASON.json")){
//            file.write(obj.toString());
//            file.flush();
//        }
//        catch (IOException e){
//            e.printStackTrace();
//
//        }
//        System.out.println(obj);
//
//
//
//




        // READ JSON
        JSONParser parser = new JSONParser();


        try {
            Object object = parser.parse(new FileReader("myJASON.json"));
            JSONObject jsonObject = (JSONObject) object;

            String name = (String) jsonObject.get("name");
            String location = (String) jsonObject.get("location");

            System.out.println("NAME :"+name+" Location :"+location);
            //array loop
            JSONArray coursesArray = (JSONArray) jsonObject.get("courses");
            Iterator<String> iterator = coursesArray.iterator();

            while (iterator.hasNext()){
                System.out.println("Course :"+iterator.next());
            }

        }
        catch (FileNotFoundException e){e.printStackTrace();}
        catch (IOException e){e.printStackTrace();}
        catch (Exception e){e.printStackTrace();}








    }

}
