package pl.org.pfig.main;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ReadNumbers {
	
		public double readDouble(){
		double in;
	
		Scanner sc = new Scanner(System.in);
		System.out.println("podaj liczbe typu double");
		try{
		in = sc.nextDouble();
		}catch(InputMismatchException e){
			in=0;
		}
		return in;
	}

	
	public int readInt(){
		int tin;
		Scanner sc = new Scanner(System.in);
		System.out.println("podaj liczbe typu int");
		try{
		tin = sc.nextInt();
		}catch(InputMismatchException e){
			tin=0;
		}
		return tin;
		
	}
	public String readString(){
		
		String tin;
		Scanner sc = new Scanner(System.in);
		System.out.println("podaj Stringa");
		
		tin = sc.nextLine();
		
		
		return tin;
		
			
	}
}
