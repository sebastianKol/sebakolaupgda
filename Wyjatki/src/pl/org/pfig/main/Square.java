package pl.org.pfig.main;

public class Square extends Exception {
	public double square (int n) throws Exception{
		if(n<0){
			throw new IllegalArgumentException("n mniejsze od 0");
		}
		
		return Math.sqrt(n);
		
	}

}


//
//Utw�rz klas� Squarezawieraj�c� metod�:public static double square(int n);obliczaj�c� pierwiastek z 
//liczby n. Wykorzystaj do oblicze� metod� Math.sqrt();
//Je�eli jednak u�ytkownik przeka�e liczb� mniejsz� od 0 zwr�� wyj�tek IllegalArgumentException