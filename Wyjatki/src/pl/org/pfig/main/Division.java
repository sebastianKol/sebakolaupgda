package pl.org.pfig.main;

public class Division extends Exception {
	public static double divide(int a, int b)throws Exception{
		if(b==0){
			throw new IllegalArgumentException(" int b=0");
		}
		
		return a/b;
	}
	
	public static double divide(double a, double b)throws Exception{
		if(b==0){
			throw new IllegalArgumentException(" double b=0");
		}
		
		return a/b;
	}
	
//	Metoda divide()powinna zwraca� IllegalArgumentExceptionw przypadku podania 0 jako dzieln� (zmienna b). 
//Poinformuj kompilator o zwracanym b��dzie.

}