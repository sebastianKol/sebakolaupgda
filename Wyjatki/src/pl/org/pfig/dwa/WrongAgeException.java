package pl.org.pfig.dwa;

public class WrongAgeException extends Exception {
	
	public WrongAgeException(){
	
	}
	public WrongAgeException(String message){
		super(message);
	}
}



//powinny wywoływać konstruktor klasy nadrzędnej.
//W przypadku braku przekazania wiadomości, konstruktor powinien przekazywać informację
//"Wrong age exception."
