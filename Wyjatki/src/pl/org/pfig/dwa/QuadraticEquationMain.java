package pl.org.pfig.dwa;

import  pl.org.pfig.dwa.QuadraticEquation;

public class QuadraticEquationMain {

	public static void main(String[] args) {
	
		QuadraticEquation qua = new QuadraticEquation(1,-3,2);
		
		try{
			qua.solve();
		}catch(ArithmeticException e){
			System.out.println(e.getMessage());
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		
		System.out.println(qua.calcX1());
		System.out.println(qua.calcX2());
		

	}
}   // do sprawdzenia