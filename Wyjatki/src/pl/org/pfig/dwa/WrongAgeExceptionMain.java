package pl.org.pfig.dwa;

public class WrongAgeExceptionMain {

	public static void main(String[] args) throws WrongAgeException, BadEyesException, BadHairException ,BadShoeException {

		Person osoba = new Person();
		try {
			osoba.setAge(2);
		} catch (WrongAgeException e) {
			System.out.println(e.getMessage());
		}
		

		System.out.println(osoba);

		// throw new WrongAgeException( "Wrong age exception.");

	}

}
