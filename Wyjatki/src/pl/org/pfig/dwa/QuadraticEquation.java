package pl.org.pfig.dwa;

import java.util.InputMismatchException;
import java.util.Scanner;

public class QuadraticEquation {
	private Integer a;
	private Integer b;
	private Integer c;

	public QuadraticEquation() {

	}

	public QuadraticEquation(int a, int b, int c) {
		this.a = a;
		this.b = b;
		this.c = c;

	}

	public Integer getA() {
		return a;
	}

	public void setA(Integer a) {
		this.a = a;
	}

	public Integer getB() {
		return b;
	}

	public void setB(Integer b) {
		this.b = b;
	}

	public Integer getC() {
		return c;
	}

	public void setC(Integer c) {
		this.c = c;
	}

	private int getNumber() {
		int tin;
		Scanner sc = new Scanner(System.in);
		System.out.println("podaj liczbe calkowita");
		try {
			tin = sc.nextInt();
		} catch (InputMismatchException e) {
			tin = 0;
		}
		return tin;

	}

	public void solve() throws Exception, ArithmeticException {

		if (a == 0 && b != 0 && c != 0) {
			throw new Exception("funkcja jest liniowa");
		} 
		if (a==0 || b==0 || c==0 ){
			getNumber();
		}
		
		if(a == null || b == null || c == null) {
			getNumber();
			
			
		}
		
		if (calcDelta() < 0) {
			throw new ArithmeticException(" delta jest ujemna");
		}
		

		
	}

	public double calcDelta() {
		return b * b - (4 * a * c);

	}

	public double calcX1() {
		return (-b - Math.sqrt(calcDelta())) / (2 * a);

	}

	public double calcX2() {
		return (-b + Math.sqrt(calcDelta())) / (2 * a);

	}
}

// public double[2] solve();-
// rozwi�zuj�c� r�wnanie kwadratoweMetoda solve()w przypadku braku
// zainicjalizowania zmiennych a, b, c
// lub podania 0 dla wszystkich trzech parametr�w) powinna wywo�a� metod�
// getNumber()przed dokonaniem oblicze�.
// Metoda powinna zwraca� tablic� z rozwi�zaniami [x1, x2]b�d� [x1, x1] i
// zg�asza� wyj�tek ArithmeticExceptionw
// przypadku ujemnej delty. Zwr�� uwag� na przypadek, kiedy a = 0,b!=0,c != 0.
// Obs�u� wyj�tek w metodzie main()