var app = angular.module('RESTApp', ['ngRoute']);
var url = 'http://localhost:8088/';

app.config(function($routeProvider) {
    var path = './views/';
    $routeProvider
        .when('/', {
            templateUrl: path + 'main.html'
        })
        .when('/elephant', {
            templateUrl: path + 'elephant.html',
            controller: 'elephantController'
        })
        .when('/crocodile', {
            templateUrl: path + 'crocodile.html',
            controller: 'crocodileController'
        })
        .when('/animals', {
            templateUrl: path + 'animals.html',
            controller: 'animalsController'
        })
        .when('/reptiles', {
            templateUrl: path + 'reptiles.html',
            controller: 'reptilesController'
        })
        .when('/add', {
            templateUrl: path + 'add.html',
            controller: 'addController'
        })
        .when('/show', {
            templateUrl: path + 'animals.html',
            controller: 'showController'
        })
        .when('/show/:id', {
            templateUrl: path + 'animal.html',
            controller: 'animalController'
        });
});

app.controller('elephantController', function($scope, $http) {
    $scope.message = "Jestem słoniem i jestem duży :)";
    $http({
        url: url + 'elephant',
        dataType: 'json',
        params: {}
    }).then(function(success) {
        $scope.name = success.data.name;
    }, function(error) {
        console.error(error);
    });
});


app.controller('crocodileController', function($scope, $http) {
    $scope.message = "Jestem krokodylem i zjadam ludzi :(";
    $http({
        url: url + 'crocodile',
        dataType: 'json',
        params: {}
    }).then(function(success) {
        $scope.name = success.data.name;
    }, function(error) {
        console.error(error);
    });
});

app.controller('animalsController', function($scope, $http) {
    $http({
        url: url + 'animals',
        dataType: 'json',
        params: {}
    }).then(function(success) {
        var root = $('.animals');
        var animals = success.data;
        for(let i = 0; i < animals.length; i++) {
            $('<li/>').html(animals[i].name).appendTo(root);
        }
    }, function(error) {
        console.error(error);
    });
});
//
//app.controller('reptilesController', function($scope, $http) {
//    $http({
//        url: url + 'reptiles',
//        dataType: 'json'
//    }).then(function(success) {
//        var root = $('.reptiles');
//        var keys = Object.keys(success.data);
//        for(let i = 0; i < keys.length; i++) {
//            $('<li/>').html('<strong>' + keys[i] + '</strong> ' + success.data[keys[i]].name).appendTo(root);
//        }
//    }, function(error) {
//        console.error(error);
//    });
//});
//
//app.controller('addController', function($scope, $http) {
//    $scope.add = function() {
//        $http({
//            url: url + 'add',
//            method: 'GET',
//            dataType: 'json',
//            params: {
//                name: $scope.animalName
//            }
//        }).then(function(success) {
//            console.log(success);
//            $scope.message = "Dodano poprawnie zwierzątko.";
//        }, function(error) {
//            console.error(error);
//        });
//    }
//});
//
//app.controller('showController', function($scope, $http) {
//    $http({
//        url: url + 'show',
//        dataType: 'json'
//    }).then(function(success) {
//        var root = $('.animals');
//        for(let i = 0; i < success.data.length; i++) {
//            $('<li/>').html('<a href="/#!show/' + success.data[i].id + '">'+success.data[i].name+'</a>').appendTo(root);
//        }
//    }, function(error) {
//        console.error(error);
//    });
//});
//
//app.controller('animalController', function($scope, $http, $routeParams) {
//    var id = $routeParams.id;
//    $http({
//        url: url + 'show/' + id,
//        dataType: 'json'
//    }).then(function(success) {
//        $scope.name = success.data.name;
//    }, function(error) {
//        console.error(error);
//    });
//});
























