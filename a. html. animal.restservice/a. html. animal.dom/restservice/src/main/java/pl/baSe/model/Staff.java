package pl.baSe.model;

/**
 * Created by RENT on 2017-07-25.
 */
public class Staff {
    int Id;
    String name;
    String lastname;
    double salary;

    public Staff() {
    }

    public Staff(int id, String name, String lastname, double salary) {
        Id = id;
        this.name = name;
        this.lastname = lastname;
        this.salary = salary;
    }

    public int getId() {
        return Id;
    }

    public Staff setId(int id) {
        Id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Staff setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastname() {
        return lastname;
    }

    public Staff setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public double getSalary() {
        return salary;
    }

    public Staff setSalary(double salary) {
        this.salary = salary;
        return this;
    }
}
