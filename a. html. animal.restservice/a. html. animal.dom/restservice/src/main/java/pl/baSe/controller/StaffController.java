package pl.baSe.controller;

import org.springframework.web.bind.annotation.*;
import pl.baSe.JsonResponse;
import pl.baSe.model.Staff;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by RENT on 2017-07-25.
 */


@CrossOrigin
@RestController
public class StaffController {
    private final AtomicLong counter = new AtomicLong();
    List<Staff> staff = new LinkedList<>();

    private AtomicInteger ai = new AtomicInteger();

    @RequestMapping("/addstaff")
    public JsonResponse addStaff(@RequestParam(value = "name") String name,
                                 @RequestParam(value = "lastname")String lastname,
                                 @RequestParam(value = "price")double price){
        this.staff.add(new Staff(ai.incrementAndGet(),name,lastname,price));
        return new JsonResponse();  //??
    }

    @RequestMapping("/showstaff")
    public List<Staff> showStaff(){
        return this.staff;
    }

    @RequestMapping("/showstaff/{id}")
    public  Staff showStaff (@PathVariable("id") String id){
        int myId = Integer.valueOf(id);
        for (int i=0; i<this.staff.size();i++){
            if ((this.staff.get(i).getId())==myId){
                return this.staff.get(i);
            }
        }
        return null;
    }

    @RequestMapping("/removestaff/{id}")
    public boolean removeStaff(@PathVariable("id") String id){
        int myId = Integer.valueOf(id);
        for (int i=0; i<this.staff.size();i++){
            if ((this.staff.get(i).getId())==myId){
                staff.remove(i);
                return true;
            }
        }
        return false;
    }



}
