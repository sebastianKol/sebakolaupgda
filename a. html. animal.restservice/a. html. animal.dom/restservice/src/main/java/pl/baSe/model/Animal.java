package pl.baSe.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by RENT on 2017-07-25.
 */
public class Animal {
//    @JsonIgnore             // flaga ignorujaca to pole przy tworzeniu JSON {- naprzyklad pass-uzytkownik mozna dodac date to bedzie bardziej bezpieczne}
    private int id;
    private String name;

    public Animal() {
    }

    public Animal(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public Animal setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Animal setName(String name) {
        this.name = name;
        return this;
    }
}
