var app = angular.module('RESTApp', ['ngRoute']);
var url = 'http://localhost:8088/';

app.config(function($routeProvider) {
    var path = './views/';
    $routeProvider
        .when('/',{
              templateUrl: path + 'main.html',
        })
        .when('/add', {
            templateUrl: path + 'add.html',
            controller: 'addController'
        })
        .when('/show', {
            templateUrl: path + 'animals.html',
            controller: 'showController'
        })
        .when('/show/:id', {
            templateUrl: path + 'animal.html',
            controller: 'animalController'
        })
        .when('/species', {
            templateUrl: path + 'species.html',
            controller: 'speciesController'
            })
        .when('/specie/:id', {
            templateUrl: path + 'specie.html',
            controller: 'specController'
        });
});


app.controller('addController', function($scope, $http) {
    
    $scope.add = function() {
        $http({
            url: url + 'animal/add',
            method: 'GET',
            dataType: 'json',
            params: {
                name: $scope.animalName,
                description: $scope.desc,
                image: $scope.image
            }
        }).then(function(success) {
            console.log(success);
            $scope.message = "Dodano poprawnie zwierzątko.";
        }, function(error) {
            console.error(error);
        });
    }
});

app.controller('showController', function($scope, $http) {
    $http({
        url: url + 'animal/show',
        dataType: 'json'
    }).then(function(success) {
        
         $scope.animals = success.data;
    }, function(error) {
        console.error(error);
    });
});

app.controller('animalController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'animal/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.animal ={
            name : success.data.name,
            description: success.data.description,
            image: success.data.image
        }
//        $scope.name = success.data.name;
    }, function(error) {
        console.error(error);
    });
});

app.controller('speciesController', function($scope, $http) {
    
    
    $http({
            url: url + 'species/show',
        }).then(function(success) {
                 $scope.species = success.data;
        }, function(error) {
            console.error(error);
        });
    
    

     $scope.add = function() {
        $http({
            url: url + 'species/add',
            method: 'GET',
            dataType: 'json',
            params: {
                name: $scope.specieName,

            }
        }).then(function(success) {
            if(success.data.id > 0)
                  $scope.species.push(success.data);
                 $scope.message = "Dodano poprawnie Gatunek."
             else 
                 $scope.message = "Cos poszlo ZleE.";
        }, function(error) {
            console.error(error);
        });
    }
    
});


app.controller('specController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'species/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        
        $scope.specie = success.data;
        
    }, function(error) {
        console.error(error);
    });
});






















