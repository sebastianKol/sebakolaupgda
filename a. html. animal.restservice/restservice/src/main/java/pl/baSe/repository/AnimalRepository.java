package pl.baSe.repository;

import org.springframework.data.repository.CrudRepository;
import pl.baSe.entity.Animal;

/**
 * Created by RENT on 2017-07-26.
 */
public interface AnimalRepository extends CrudRepository<Animal,Long> {
}
