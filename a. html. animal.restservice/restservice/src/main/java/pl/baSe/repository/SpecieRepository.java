package pl.baSe.repository;

import org.springframework.data.repository.CrudRepository;
import pl.baSe.entity.Specie;

/**
 * Created by RENT on 2017-07-26.
 */
public interface SpecieRepository extends CrudRepository<Specie,Long> {

}
