package pl.baSe.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.List;

/**
 * Created by RENT on 2017-07-27.
 */
@Entity
public class Profession {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;

    @JsonBackReference   // wskazanie zgodne z ta druga z Animala
    @OneToMany(mappedBy = "profession")   // to bedzie zmienna w animalu + get-set
    private List<Staff> staff;


    public Profession() {
    }

    public Profession(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public Profession setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Profession setName(String name) {
        this.name = name;
        return this;
    }

    public List<Staff> getStaff() {
        return staff;
    }

    public Profession setStaff(List<Staff> staff) {
        this.staff = staff;
        return this;
    }
}
