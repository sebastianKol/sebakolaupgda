package pl.baSe.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

/**
 * Created by RENT on 2017-07-27.
 */
@Entity
public class Staff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String lastname;
    private double salary;

    @JsonManagedReference
    @ManyToOne(cascade = CascadeType.ALL)
    private Profession profession;

    public Staff() {
    }

    public Staff(long id, String name, String lastname, double salary) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.salary = salary;
    }

    public long getId() {
        return id;
    }

    public Staff setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Staff setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastname() {
        return lastname;
    }

    public Staff setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public double getSalary() {
        return salary;
    }

    public Staff setSalary(double salary) {
        this.salary = salary;
        return this;
    }

    public Profession getProfession() {
        return profession;
    }

    public Staff setProfession(Profession profession) {
        this.profession = profession;
        return this;
    }
}
