package pl.baSe.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.List;

/**
 * Created by RENT on 2017-07-26.
 */
@Entity
public class Specie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String description;

    // powiazanie  tabeli
    @JsonBackReference   // wskazanie zgodne z ta druga z Animala
    @OneToMany(mappedBy = "specie")   // to bedzie zmienna w animalu + get-set
    private List<Animal> animals;

    public Specie(){

    }

    public Specie(long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description=description;
    }


    public long getId() {
        return id;
    }

    public Specie setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Specie setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Specie setDescription(String description) {
        this.description = description;
        return this;
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    public Specie setAnimals(List<Animal> animals) {
        this.animals = animals;
        return this;
    }
}
