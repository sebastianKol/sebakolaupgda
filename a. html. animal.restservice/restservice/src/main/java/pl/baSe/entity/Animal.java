package pl.baSe.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

/**
 * Created by RENT on 2017-07-25.
 */
@Entity
public class Animal {
//    @JsonIgnore             // flaga ignorujaca to pole przy tworzeniu JSON {- naprzyklad pass-uzytkownik mozna dodac date to bedzie bardziej bezpieczne}
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String description;
    private String image;

    // powiazanie  tabeli
    @JsonManagedReference               // unika infinity look  - oznacza ze jest glowna - lista nie bedzie sie wyswietlala w specie
    @ManyToOne(cascade = CascadeType.ALL) // Right- null    Left - nic nie pokaze + get-set
    private Specie specie;

    public Animal() {
    }

    public Animal(long id, String name, String description, String image) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
    }

    public long getId() {
        return id;
    }

    public Animal setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Animal setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Animal setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getImage() {
        return image;
    }

    public Animal setImage(String image) {
        this.image = image;
        return this;
    }

    public Specie getSpecie() {
        return specie;
    }

    public Animal setSpecie(Specie specie) {
        this.specie = specie;
        return this;
    }
}
