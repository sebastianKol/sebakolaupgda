package pl.baSe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.baSe.entity.Animal;
import pl.baSe.entity.Profession;
import pl.baSe.entity.Specie;
import pl.baSe.entity.Staff;
import pl.baSe.repository.ProfessionRepository;
import pl.baSe.repository.StaffRepository;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-07-27.
 */
@CrossOrigin
@RestController
@RequestMapping("/staff")
public class StaffController {

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private ProfessionRepository professionRepository;

    @RequestMapping("/")
    public String staff() {
        return "";
    }

    @RequestMapping("/show")
    public List<Staff> listStaff() {
        return (List<Staff>) staffRepository.findAll();
    }



    @RequestMapping("/add")
    public Staff addStaff (@RequestParam(name = "name") String name,
                            @RequestParam(name = "lastname") String lastname,
                            @RequestParam(name = "salary") String salary,
                           @RequestParam(name = "profession") String profesion ) {
        long proId = Long.valueOf(profesion);
        Profession p = professionRepository.findOne(proId);
        Staff a = new Staff();
        a.setName(name);
        a.setLastname(lastname);
        a.setSalary(Double.parseDouble(salary));
        a.setProfession(p);
        return staffRepository.save(a);
    }

    @RequestMapping("/show/{id}")
    public Staff showStaffByID(@PathVariable("id") String id) {
        return staffRepository.findOne(Long.valueOf(id));
    }

}


