package pl.baSe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.baSe.entity.Profession;
import pl.baSe.repository.ProfessionRepository;

import java.util.List;

/**
 * Created by RENT on 2017-07-27.
 */
@CrossOrigin
@RestController
@RequestMapping("/profession")
public class ProfessionController {

    @Autowired
    private ProfessionRepository professionRepository;

    @RequestMapping("/add")
    public Profession add(@RequestParam(name="name") String name){
        Profession profession = new Profession();
        profession.setName(name);
        return professionRepository.save(profession);
    }

    @RequestMapping({"/show", "/"})
    public List<Profession> showAll(){
        return (List<Profession>) professionRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Profession showById(@PathVariable(name = "id") String id){
        long myId = Long.valueOf(id);
        return professionRepository.findOne(myId);
    }
}



