package pl.baSe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.baSe.entity.Specie;
import pl.baSe.repository.SpecieRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by RENT on 2017-07-26.
 */
@CrossOrigin
@RestController
@RequestMapping("/species")             // request mapping dla calego kontrolera
public class SpecieController {

    // tak musi sie nazywac
    @Autowired
    private SpecieRepository specieRepository;

    // dodawanie do bazy danych
    @RequestMapping("/add")
    public Specie add(@RequestParam(name="name") String name,
                      @RequestParam(name = "description") String desc){
        Specie spec = new Specie();
        spec.setName(name);
        spec.setDescription(desc);
        return specieRepository.save(spec);
    }

    @RequestMapping({"/show", "/"})
    public  List<Specie> showAll(){
        return (List<Specie>) specieRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Specie showById(@PathVariable(name = "id") String id){
        long myId = Long.valueOf(id);
            return specieRepository.findOne(myId);
        }
    }

