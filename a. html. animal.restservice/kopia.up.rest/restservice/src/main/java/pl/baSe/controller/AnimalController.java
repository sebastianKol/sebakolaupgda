package pl.baSe.controller;

import org.springframework.web.bind.annotation.*;
import pl.baSe.model.Animal;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by RENT on 2017-07-25.
 */

@CrossOrigin                                   // oznaczenie pozwala na uzywanie back z innej domeny
@RestController                                 // oznacza komunikacje JSON
public class AnimalController {
    private final AtomicLong counter = new AtomicLong();
    private List<Animal> myAnimals = new LinkedList<>();
    private AtomicInteger ai = new AtomicInteger();

    @RequestMapping({"/","/animal"})          // "/" - odwolanie na strone glowna - rozne strony kozystaja z tej samej instancj kontrolera
    public  String animal(){
        return counter.incrementAndGet() +   ". Witaj, Pawian Testowy";
    }
    @RequestMapping("/elephant")
    public Animal elephant(){                   // automatycznie tworzy JSON
        Animal elephant = new Animal(0,"Elephant");
        return  elephant;
    }
    @RequestMapping("/crocodile")
    public Animal crocodile(){
        Animal crocodile = new Animal(1,"Krokodyl");
        return crocodile;
    }
    @RequestMapping("/animals")     // zwraca tablice obiektow
    public List<Animal> animals() {
        List<Animal> animals = new LinkedList<>();
        animals.add(new Animal(1,"Jezyk"));
        animals.add(new Animal(2,"Słonik"));
        animals.add(new Animal(3,"Krecik"));
        return animals;
    }
    @RequestMapping("/mamals")
    public Set<Animal> mamals(){
        Set<Animal> mamals = new HashSet<>();    // elementy sie nie powtarzaja
        mamals.add(new Animal(4,"Koza"));
        mamals.add(new Animal(5,"Lew"));
        mamals.add(new Animal(6,"Kon"));
        return mamals;
    }
    @RequestMapping("/reptiles")
    public Map<String,Animal> reptiles(){
        Map<String,Animal> reptiles = new HashMap<>();
        reptiles.put("ZabaAA",new Animal(7,"Zaba"));
        reptiles.put("AligatorRR",new Animal(8,"Aligator"));
        return reptiles;
    }



    @RequestMapping("/show")
    public List<Animal> listAnimals (){
        return this.myAnimals;
    }
    @RequestMapping("/add")     // adnotacja przy parametrze , tworzymy
    public boolean addAnimal(@RequestParam(value ="name") String name){
        this.myAnimals.add(new Animal(ai.incrementAndGet(),name));
        return true;
    }
    @RequestMapping("/show/{id}")
    public Animal showAnimalByID(@PathVariable("id") String id){   //adnotacja po id - pochodzi ze sciezki URL
        for (int i=0; i<this.myAnimals.size();i++){
            if(this.myAnimals.get(i)).getID() ==myId){
                return this.
            }
        }
    }

}
