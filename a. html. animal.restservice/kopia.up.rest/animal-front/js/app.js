var app = angular.module('RESTApp', ['ngRoute']);
var url = 'http://localhost:8088/';
app.config(function($routeProvider) {
    var path = './views/';
    $routeProvider
        .when('/', {
            templateUrl: path + 'main.html'
        })
        .when('/elephant', {
            templateUrl: path + 'elephant.html',
            controller: 'elephantController'
        })
        .when('/crocodile',{
            templateUrl: path + 'crocodile.html',
            controller: 'crocodileController'
        })
        .when('/animals',{
            templateUrl: path + 'animals.html',
            controller: 'animalsController'
        })
        .when('/mamals',{
            templateUrl: path + 'mamals.html',
            controller: 'mamalsController'
        })
         .when('/reptiles',{
            templateUrl: path + 'reptiles.html',
            controller: 'reptilesController'  
        });
});

app.controller('elephantController', function($scope, $http) {
    $scope.message = "Jestem słoniem i jestem duży :)";
    $http({
        url: url + 'elephant',
        dataType: 'json',
        params: {}
    }).then(function(success) {
        $scope.name = success.data.name;
    }, function(error) {
        console.error(error);
    });
});

app.controller('crocodileController', function($scope, $http) {
    $scope.message = "Jestem Krok i jestem glodny :)";
    $http({
        url: url + 'crocodile',
        dataType: 'json',
        params: {}
    }).then(function(success) {
        $scope.name = success.data.name;
    }, function(error) {
        console.error(error);
    });
});

app.controller('animalsController', function($scope, $http) {
    $scope.message = "Lista zwierzat :)";
    $http({
        url: url + 'animals',
        dataType: 'json',
        params: {}                  
    }).then(function(success) {
        var root = $('.animals');
        var animals = success.data;
        for(let i=0; i<animals.length;i++){
            $('<li/>').html(animals[i].name).appendTo(root);
        }
    }, function(error) {
        console.error(error);
    });
});

app.controller('mamalsController', function($scope, $http) {
    $scope.message = "Zbior ssakow";
    $http({
        url: url + 'mamals',
        dataType: 'json',
        params: {}                  
    }).then(function(success) {
        var root = $('.mamals');
        var mamals = success.data;
        for(let i=0; i<mamals.length;i++){
            $('<li/>').html(mamals[i].name).appendTo(root);
        }
    }, function(error) {
        console.error(error);
    });
});

app.controller('reptilesController', function($scope, $http) {
    $scope.message = "Mapa Czegos";
    $http({
        url: url + 'reptiles',
        dataType: 'json',
        params: {}                  
    }).then(function(success) {
        var root = $('.reptiles');
        
        var reptiles = success.data;
        var keys = Object.keys(reptiles);
        

         for(let key of keys){
             $('<li/>').html(key +" " +reptiles[key].name).appendTo(root);
      
            }
        

            
    
    }, function(error) {
        console.error(error);
    });
});




