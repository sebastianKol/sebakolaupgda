package pl.org.pfig.data;

public class Dog implements AnimalInterface {
	private String name;

	public Dog(String name) {
		super(); // nie trzeba dodawac w tym przykladzie
		this.name = name;
	}
	
	public Dog newDog(String name){     // nadpisanie do klasy my time
		return new Dog(name);
		
	}
	
	public String getName(){
		return this.name;
	}

	@Override
	public String toString() {   // reprezentuje obiekt jako ciag znakow tostring
		return "Dog:"+this.name;  // nadpisanie jako imie - name
	}
	
	
	
	
	
}
