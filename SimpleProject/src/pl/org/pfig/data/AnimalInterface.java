package pl.org.pfig.data;


public interface AnimalInterface {
	public String getName();    // netoda nie posiada ciala w tej klasie tylko w innej

	public default int getLegs(){
		return 4;
	}
}
