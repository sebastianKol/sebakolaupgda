package pl.org.pfig.data;

public class Cat implements AnimalInterface, Soundable {  // implementujemy 2 interfejsy np.
	private String name;

	public Cat(String name) {

		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}

	@Override
	public String getSound() {
		return "mial mial";
	}
	
	
}
