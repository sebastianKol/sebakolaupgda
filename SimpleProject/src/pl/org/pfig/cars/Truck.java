package pl.org.pfig.cars;

	public class Truck extends Car {
		private String name;
		private int tires;
		private String engine;
		
		
		public Truck (String name, int tires,String engine) {
			this.name = name;
			this.tires = tires;
			this.engine = engine;
		}
		
		public String getName(){
			return name;
		}
		public int getTires(){
			return tires;
		}
		public String getEngine(){
			return engine;
		}
		
		
		
		
}
