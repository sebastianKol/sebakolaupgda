package ppl.org.pfig.main;

import pl.org.pfig.data.*;

public class Main {
		public static void main(String[] args){
			Dog d1 = new Dog ("Burek");
			Cat c1 = new Cat ("Mruczek");
			Llama l1 = new Llama ("Super Lama");
			
			System.out.println("Piesek to :" + d1.getName());
			System.out.println("Kotek to : " + c1.getName());
			System.out.println("Lama to : " + l1.getName());
			
			AnimalInterface[] animals = new AnimalInterface[3];  // tworze tablice i dodaje do niej zwierzeta
			animals[0] = d1;
			animals[1] = c1;
			animals[2] = l1;
			
			for (AnimalInterface ai : animals){						// drukujemy cala tablice foreach petla ai- nazwa zmiennej ktora przyjmuje kazde zwierze
				System.out.println("Zwierze nazywa si�: "+ ai.getName()+"("+ai.getLegs()+")");
	
				
			if(ai instanceof Soundable){					// sprawdzanie czy dana zmienna powiazana jest z klasa inferfejsem
				Soundable sound = (Soundable)ai;			// czy zwierze ma soundable - rzutowanie na interface soundable
				System.out.println("\trobi "+sound.getSound());  		// wyswietlamu 
			}
		
			System.out.println(d1);
			
			
			}
			
	
			
		}

	
	
	
}
