package pl.org.pfig.tdd;

import java.util.Arrays;

public class Exercises {

	public int absoluteValue(int num) {
		if (num < 0) {
			num = -num;
		}
		return num;
	}

	public String determineDayOfWeek(int day) throws IllegalArgumentException { // powinna
																				// zwracac
																				// illegal
		// argument jesli podana liczba
		// jest poza zakrestem
		if (day > 7 || day < 1) {
			throw new IllegalArgumentException("Bad intput value.");
		}
		String[] days = { "Niedziela", "Poniedzialek", "Wtorek", "Sroda", "Czwartek", "Piatek", "Sobota" };
		return days[day - 1];
	}

	public String determinateMonths(int month) throws IllegalArgumentException {
		if (month > 12 || month < 1) {
			throw new IllegalArgumentException("Bad intput value.");
		}

		String[] months = { "Styczen", "Luty", "Marzec", "Kwiecien", "Maj", "Czerwiec", "Lipiec", "Sierpien",
				"Wrzesien", "Pazdziernik", "Listopad", "Grudzien" };
		return months[month - 1];
	}

	public int euclideanAlgorithm(int a, int b) {
		if (a == 0 || b == 0) {
			return 0;
		} else if (a < 0) {
			a = -a;
		}
		if (b < 0) {
			b = -b;
		}

		while (a != b) {
			while (a > b) {
				a = a - b;
			}
			while (b > a) {
				b = b - a;
			}
		}

		return a;
	}

	public int countTime(int h, int m, int s) throws IllegalArgumentException {
		if (h > 23 || h < 0 || m > 59 || m < 0 || s > 59 || s < 0) {
			throw new IllegalArgumentException("Bad intput value.");

		}
		return (s) + (m) * 60 + (h) * 3600;

	}

	public  int[] numbersPrinting(int a, int b) throws IllegalArgumentException {

		int[] arr = new int [1];
		int j=0;
		for (int i = a; i <= b; i++) {
			if (i % 2 == 0) {
				arr[j++] = i;
				arr = Arrays.copyOf(arr, arr.length+1);
			}
		}
		for (int i = b; i >= a; i--) {
			if (i % 2 == 1) {
				arr[j++] = i;
				arr = Arrays.copyOf(arr, arr.length+1);
			}
		}
		if (a == b) {
			throw new IllegalArgumentException("Bad intput value.");
		}
		
		return arr;
	
	}
		

	

	//
	// 6. Użytkownik podaje dwie liczby całkowite a, b. algorytm ma za zadanie
	// wypisać wszystkie
	// parzyste liczby w kolejności rosnącej, a następnie wszystkie liczby
	// nieparzyste w kolejności
	// malejącej z przedziału <a;b>. niech a, b –liczby całkowite z zakresu
	// 0-255. Np. dla danych
	// wejściowych a=3, b=8,otrzymujemy plik wynikowy: 4, 6, 8, 7, 5, 3
	//

}
