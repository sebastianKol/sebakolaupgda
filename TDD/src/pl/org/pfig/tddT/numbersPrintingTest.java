package pl.org.pfig.tddT;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pl.org.pfig.tdd.Exercises;

public class numbersPrintingTest {

	Exercises e;

	@Before
	public void init() {
		e = new Exercises();
	}

	@Test
	public void whenOutOfRangeArgumentFirstIsGivenExceptionExpected() { // A
		IllegalArgumentException ex = null;
		int[] arrayOfA = { -1, 256, 1000, -2222 };
		int b = 8;
		for (int a : arrayOfA) {
			try {
				e.numbersPrinting(a, b);
			} catch (IllegalArgumentException e) {
				ex = e;
			}
			assertTrue(ex != null);
			ex = null;
		}
	}

	@Test
	public void whenOutOfRangeArgumentSecondIsGivenExceptionExpected() { // B
		IllegalArgumentException ex = null;
		int a = 6;
		int[] arrayOfB = { -1, 256, 1000, -2222 };
		for (int b : arrayOfB) {
			try {
				e.numbersPrinting(a, b);
			} catch (IllegalArgumentException e) {
				ex = e;
			}
			assertNotNull("wyjatek nie wystapil",ex);
			ex = null;
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void whenArgumentsAreInIncorectOrderGivenExceptionExpected() {
		int a = 8;
		int b = 3;
		e.numbersPrinting(a, b);

	}

	@Test
	public void whenProperValueIsGivenProperResultExpected() {
		int a=3;
		int b=8;
		int[] expected = { 4, 6, 8, 7, 5, 3 };
		assertArrayEquals(expected, e.numbersPrinting(a, b));

	}

	@Test
	public void whenArgumentsAreTheSameOneArgumentExpected() {
		int arg =3;

		int[] expected = { 3 };
		assertEquals(expected, e.numbersPrinting(arg, arg));

	}
}

//
// 6. Użytkownik podaje dwie liczby całkowite a, b. algorytm ma za zadanie
// wypisać wszystkie
// parzyste liczby w kolejności rosnącej, a następnie wszystkie liczby
// nieparzyste w kolejności
// malejącej z przedziału <a;b>. niech a, b –liczby całkowite z zakresu 0-255.
// Np. dla danych
// wejściowych a=3, b=8,otrzymujemy plik wynikowy: 4, 6, 8, 7, 5, 3
//