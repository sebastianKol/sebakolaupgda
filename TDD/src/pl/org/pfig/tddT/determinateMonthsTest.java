package pl.org.pfig.tddT;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import pl.org.pfig.tdd.Exercises;

public class determinateMonthsTest {

	Exercises e;
	
	@Before
	public void init(){
		e = new Exercises();
	}

	@Test
	public void whenProperValueIsGivenProperMonthResultExpected() {
		int[] data = {1,12};
		String[] expected = {"Styczen","Grudzien"};
		
		for (int i=0; i<data.length; i++){
			assertEquals(expected[i], e.determinateMonths(data[i]));
		}
	}
	@Test							
	public void whenOutOfRangeArgumentIsGivenExceptionExpected() {
		int data=13;
		IllegalArgumentException iae=null;
		try{
			e.determinateMonths(data);
		}catch(IllegalArgumentException e){
			iae = e;
		}
		assertNotNull("Wyjatek nie wystapil", iae);
	}
	@Test
	public void whenProperMediumValueIsGivenProperResultExpecteg(){
		int data = 5;
		String expected = "Maj";
		assertEquals(expected,e.determinateMonths(data));
	}
}
