package pl.org.pfig.tddT;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import pl.org.pfig.tdd.Exercises;

public class euclideanAlgorithmTest {
Exercises e;
	
	@Before
	public void init(){
		e = new Exercises();
	}

	@Test
	public void whenPositiveValueAreGivenProperResultExpected() {
		int a = 21;
		int b = 6;
		int expected = 3;
		assertEquals(expected, e.euclideanAlgorithm(a, b));
	}
	@Test
	public void whenNegativeValueaAreGivenProperResultExpected() {
		int a = -21;
		int b = -6;
		int expected = 3;
		assertEquals(expected, e.euclideanAlgorithm(a, b));
	}
	@Test
	public void whenOneNegativeValueaOtherPositiveValueAreGivenProperResultExpected() {
		int a = -21;
		int b = 6;
		int expected = 3;
		assertEquals(expected, e.euclideanAlgorithm(a, b));
	}
	@Test
	public void whenOnePositiveValueaOtherNegativeValueAreGivenProperResultExpected() {
		int a = 21;
		int b = -6;
		int expected = 3;
		assertEquals(expected, e.euclideanAlgorithm(a, b));
	}
	@Test
	public void wheZeroValueaIsGivenZeroExpected() {
		int a = 0;
		int b = -6;
		int expected = 0;
		assertEquals(expected, e.euclideanAlgorithm(a, b));
	}
	@Test
	public void whenRandomDataExpectetProperResult(){
		int[][] dataSet = {
				{48,12,12},
				{210,6,6},
				{-9,3,3},
				{21,-7,7},
				{36,-6,6}
		};
		for(int[] data : dataSet){
			assertEquals(data[2], e.euclideanAlgorithm(data[0], data[1]));
		
		}
	}
}
