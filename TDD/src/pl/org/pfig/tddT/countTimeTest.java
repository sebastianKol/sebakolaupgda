package pl.org.pfig.tddT;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import pl.org.pfig.tdd.Exercises;

public class countTimeTest {
	Exercises e;

	@Before
	public void init() {
		e = new Exercises();
	}

	@Test(expected = IllegalArgumentException.class) // ujemna godzina
	public void whenHourIsNegativeExceptionExpected() {
		int[] data = { -3, 12, 15 };
		e.countTime(data[0], data[1], data[2]);
	}

	@Test(expected = IllegalArgumentException.class) // ujemna minuta
	public void whenMinutesIsNegativeExceptionExpected() {
		int[] data = { 3, -12, 15 };
		e.countTime(data[0], data[1], data[2]);
	}

	@Test(expected = IllegalArgumentException.class) // ujemna sekunda
	public void whenSecondsIsNegativeExceptionExpected() {
		int[] data = { 3, 12, -15 };
		e.countTime(data[0], data[1], data[2]);
	}

	@Test
	public void whenProperHourIsGivenProperTimeResultExpected() { // skrajne
																	// godziny
		int[] data = { 0, 23 };
		int[] expected = { 0, 23 * 3600 };
		for (int i = 0; i < data.length; i++) {
			assertEquals(expected[i], e.countTime(data[i], 0, 0));
		}
	}

	@Test
	public void whenProperMediumHourIsGivenProperTimeExpecteg() { // srodkowa
																	// godzina
		int data = 4;
		int expected = 4 * 3600;
		assertEquals(expected, e.countTime(data, 0, 0));
	}

	@Test
	public void whenProperMinuteIsGivenProperTimeResultExpected() { // skrajne
																	// minuty
		int[] data = { 0, 59 };
		int[] expected = { 0, 59 * 60 };
		for (int i = 0; i < data.length; i++) {
			assertEquals(expected[i], e.countTime(0, data[i], 0));
		}
	}

	@Test
	public void whenProperMediumMinuteIsGivenProperTimeExpecteg() { // srodkowe
																	// minuty
		int data = 4;
		int expected = 4 * 60;
		assertEquals(expected, e.countTime(0, data, 0));
	}

	@Test
	public void whenProperSecondIsGivenProperTimeResultExpected() { // skrajne
																	// sekundy
		int[] data = { 0, 59 };
		int[] expected = { 0, 59 };
		for (int i = 0; i < data.length; i++) {
			assertEquals(expected[i], e.countTime(0, 0, data[i]));
		}
	}

	@Test
	public void whenProperSecondIsGivenProperTimeExpecteg() { // srodkowe
																// sekundy
		int data = 4;
		int expected = 4;
		assertEquals(expected, e.countTime(0, 0, data));
	}

	@Test
	public void whenTimezeroIsGivenProperTimeExpecteg() { // czas 00:00:00
		int h = 0;
		int m = 0;
		int s = 0;
		int expected = 0;
		assertEquals(expected, e.countTime(0, 0, 0));
	}

	@Test
	public void whenProperIsGivenProperTimeExpecteg() { // czas poprawny
		int h = 3;
		int m = 12;
		int s = 33;
		int expected = s + 12 * 60 + 3 * 3600;
		assertEquals(expected, e.countTime(3, 12, 33));
	}
}
