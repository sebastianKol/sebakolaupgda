package pl.org.pfig.tddT;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import pl.org.pfig.tdd.Exercises;

public class determineDayOfWeekTest {
	

	Exercises e;
	
	@Before
	public void init(){
		e = new Exercises();
	}
	
	@Test
	public void whenProperValueIsGivenProperDayResultExpected(){  // testuje wartosci brzegowe
		int[] data = {1,7};
		String[] expected = {"Niedziela","Sobota"};
		
		for (int i=0; i<data.length; i++){
			assertEquals(expected[i], e.determineDayOfWeek(data[i]));
		}
	}

	@Test							
	public void whenOutOfRangeArgumentIsGivenExceptionExpected() {
		int data=9;
		IllegalArgumentException iae=null;
		try{
			e.determineDayOfWeek(data);
		}catch(IllegalArgumentException e){
			iae = e;
		}
		assertNotNull("Wyjatek nie wystapil", iae);
		
	}
	@Test(expected=IllegalArgumentException.class)
	public void whenOutOfRangePositiveArgumentIsGivenExeptionExpected(){
		int data = 123;
		e.determineDayOfWeek(data);
	}

	@Test
	public void whenProperMediumValueIsGivenProperResultExpecteg(){
		int data = 4;
		String expected = "Sroda";
		assertEquals(expected,e.determineDayOfWeek(data));
	}
	
	
}
