package pl.org.pfig.tddT;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import pl.org.pfig.tdd.Exercises;

public class absoluteValueTest {
	
	Exercises e;
	
	@Before
	public void init(){
		e = new Exercises();
	}

	@Test
	public void whenPositiveValueIsGivenPositiveValueIsExpected(){
		int arg = 5;
		int expected =5;
		assertEquals(expected, e.absoluteValue(arg));
	}

	@Test 
	public void whenNegativeValueIsGivenPositiveValueIsExpected(){
		int arg = -4;
		int expected = 4;
		assertEquals(expected, e.absoluteValue(arg));
	}
	@Test 
	public void whenZeroValueIsGivenZeroValueIsExpected(){
		int arg = 0;
		int expected = 0;
		assertEquals(expected, e.absoluteValue(arg));
	}
	
}
