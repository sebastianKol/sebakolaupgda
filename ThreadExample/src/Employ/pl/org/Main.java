package Employ.pl.org;

import java.util.LinkedList;

public class Main {

	public static void main(String[] args) {
		
		 LinkedList <Employee> emp = new LinkedList<>();
		
		 emp.add(new Employee("Paweł","Testowy",18,3000));
		 emp.add(new Employee("Piotr","Przykladowy",32,10000));
		 emp.add(new Employee("Julia","Doe",41,4300));
		 emp.add(new Employee("Przemysław","Wietrak",56,4500));
		 emp.add(new Employee("Zofia","Zaspa",37,3700));
		 
		
		Company c = new Company();
		c.setEmp(emp);
		
		c.filter(n -> n.startsWith("Z"), t->t.toUpperCase());
		
		
		//c.oper(17, 3, (a,b)-> (a-b) ));
	}

}


//
//Zadania:
//
//1. Wydrukuj tylko pracowników, których imię zaczyna się od „P"
//2. Wydrukuj sumę płac pracowników, mających nieparzystą liczbę lat.

//

