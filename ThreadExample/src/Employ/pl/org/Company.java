package Employ.pl.org;

import java.util.LinkedList;

public class Company {

	private LinkedList<Employee> emp = new LinkedList<>();

	public LinkedList<Employee> getEmp() {
		return emp;
	}

	public void setEmp(LinkedList<Employee> emp) {
		this.emp = emp;
	}

	public void filter(FilterInterface f, Transform t) {   
		
		
			for(Employee e : emp){
				if(f.test(e.getName())){
				System.out.println(t.transform(e.getName()));
				}
			
				
		}
		
	}

}

//
// Następnie utwórz klasę Company, w której znajdą się pola i metody:
//
// -employees:List<Employee>
//
// +filter(FilterInterface f, Transform t);
//
// Metoda filter, powinna filtrować naszą listę, abyśmy pracowali tylko na
// rekordach zwróconych przez nią.