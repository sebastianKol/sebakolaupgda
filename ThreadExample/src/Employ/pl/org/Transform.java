package Employ.pl.org;

@FunctionalInterface
public interface Transform {
	public String transform(String s);

}




//Oraz interfejs Transform zawierający deklarację:
//
//+transform(String s):String
//
//Interfejs Transform, będzie służył do zmiany parametru na jego odpowiednik (np. toUpperCase() dla Stringa).