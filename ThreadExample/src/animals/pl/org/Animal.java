package animals.pl.org;

import java.util.LinkedList;
import java.util.List;

public class Animal {
	private List<String> animals = new LinkedList<>();

	public Animal(){}
	
	public void addAnimal(String animal, AnimalInterface ai){ // dodaje zwierze do listy
		String[] animals = ai.make(animal);
		//animal.add(animal);
		for(String a : animals){
			this.animals.add(a);
		}
	}
	
	public String getAnimal(int index){ // zwraca zwierze z indexu
		
		
		return (animals.size()>index) ?  animals.get(index) :"";
		// warunek jak prawda ? warunek jak falsz
	}
	
	public boolean containtAnimal(String animal, AnimalInterface ai){ // sprawdza czy znajduje sie na liscie
		
//		return animals.contains(animal);
		String[] animals = ai.make(animal);
		for(String a : animals){
			if(!this.animals.contains(a)){
				return false;
			}
		}
		return true;
		

	
	}
	
	
}
//Dostosuj metodę addAnimal w klasie Animal, tak aby przyjmowała dwa parametry:
//
//public addAnimal(String animal, AnimalInterface ai), która powinna wywoływać metodę add z interfejsu funkcyjnego.
//
//