package pl.org.pfig.calc;

@FunctionalInterface 
// nie pozwalna na dopisanie metod do interfejsu funkcyjnego

public interface CalculatorInterface {
	public double doOperation(int a,int b);

}
