package pl.org.pfig.main;

import java.util.LinkedList;
import java.util.List;

import animals.pl.org.Animal;
import animals.pl.org.AnimalInterface;
import pl.org.pfig.calc.Calc;
import pl.org.pfig.calc.CalculatorInterface;

public abstract class Main {

	public static void main(String[] args) {
		Thread t = new Thread(new PrzykladowyWatek());
		
		t.start();
		
		new Thread (new InnyWatek(), " Jakis Watek").start();
		System.out.println("Koniec");
		
		new Thread(new Runnable(){
			public void run(){
				System.out.println("pochodze z runable");
			}
		}).start();
		
		new Thread( () -> {
			int a =1;
			int b =2;
			System.out.println("Suma wynosi: "+ (a+b));
			}).start();
		
		
		
		new Thread(  ()  -> System.out.println("Wiadomosci z FI")  ).start();
		
		
		
		
		List <Person> p = new LinkedList<>();
		p.add(new Person(1));
		
		Calc c = new Calc();
//		System.out.println(c.add(3, 4));
		
		System.out.println(c.oper(5, 6, new CalculatorInterface () {
			
			@Override
			public double doOperation(int a, int b) {
				// tworzenie instancji klasy ktora implementuje interfejs
				return a+b;
			}
		}));

			System.out.println(c.oper(17, 3, (a,b)-> (a-b) ));
			System.out.println(c.oper(17, 3, (a,b)-> (a*b) ));
			
			Animal a = new Animal();
	//		a.addAnimal("Slon");
	//		System.out.println(a.containtAnimal("Slon"));
			
			
			
			a.addAnimal("Kot", (anims) -> (anims.split(" ")));
			a.addAnimal("pies|kot|zaba", (anims) -> (anims.split("\\|")));
			a.addAnimal("aligator_waz", (anims) -> (anims.split("_")));
			
			a.addAnimal("katp", (anims)-> {
				String[] ret = new String[1];
				ret[0]="";
				for(int i=anims.length() - 1 ; i>=0 ; i--){
					ret[0] += anims.charAt(i) + "";
				}
					return ret;
			});
			
		//	System.out.println(a.containtAnimal("ptak")); // znajduje sie na liscie
		//	System.out.println(a.containtAnimal("katp")); // to nie bo zostal zmieniony
			System.out.println(a.containtAnimal("aligator_kot", (s) -> (s.split("_")) ));
			
			
	}

}
