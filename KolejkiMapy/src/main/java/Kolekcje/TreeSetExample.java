package Kolekcje;

import java.util.Arrays;
import java.util.TreeSet;

public class TreeSetExample {

	public static void main(String[] args) {
	
		TreeSet<Integer> treeSet = new TreeSet<>(Arrays.asList(1,2,3,4,5));
		
		System.out.println("Caly zbior");
		for(int integer : treeSet){
			System.out.println(integer);
		}


		System.out.println("Caly headSet(3)" );
		for(Integer integer : treeSet.headSet(3)){
			System.out.println(integer);
		}
		

		System.out.println("Caly trialSet(3)" );
		for(Integer integer : treeSet.tailSet(3)){
			System.out.println(integer);
		}
	}

}
