package Kolekcje;

import java.util.HashSet;
import java.util.Set;

public class StringDuplicates {
	
	public static boolean containDuplicates(String text){
		
		char[]chrt = text.toCharArray();
	
		Set<Character> set = new HashSet<>();
		for(char i :chrt){
			set.add(i);
		}
		return set.size() != chrt.length;
		
	}
	
	

}
