package Kolekcje;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class SetsExample {

	public static void main(String[] args) {

		Integer[] tab = { 5000, 2000, 1000, 3000, 4000, 2000, 2000, 3000, 2000, 4000, 5000 };

		Set<Integer> set = new TreeSet<>();

		// set.addAll(Arrays.asList(tab)); // mozna dodac w petli

		for (int i : tab) {
			set.add(i);
		}

		System.out.println("Rozmiar zbioru " + set.size()); // ma 5 elementow
		for (int i : set) {
			System.out.println(i);

		}
		set.remove(1000);
		set.remove(2000);

		System.out.println("Rozmiar zbioru " + set.size()); // ma 5 elementow
		for (int i : set) {
			System.out.println(i);
			
		}
		Iterator<Integer> iterator = set.iterator();  // w 2 krokach
		System.out.println("Pojedynczy element");
		System.out.println(iterator.next());		// w 2 krokach
		System.out.println(set.iterator().next()); // w jednej linni
		
		
		// wypisanie zbioru iteratorem
		System.out.println("Caly zbior");
		Iterator<Integer> it = set.iterator();  // w 2 krokach
		while(it.hasNext()){		
		System.out.println(it.next());
		
		
		}
		
	}
}
