package kolejki.kolejki;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortowanieListy {

	public static void main(String[] args) {
		List<Person> persons = new ArrayList<>();
		
		

		persons.add(new Person("Stefan", 22));
		persons.add(new Person("Ula", 33));
		persons.add(new Person("Adam", 45));
		persons.add(new Person("Tomasz", 33));


		System.out.println("Przed sortowaniem");
		for(Person person : persons){
			System.out.println(person);
		}
		
		Collections.sort(persons);
	
		
		System.out.println("Po sortowaniu");
		for(Person person : persons){
			System.out.println(person);
		}
		
		Collections.shuffle(persons);
		System.out.println("Tasowanie - shuffle za kazdym razem inaczej");
		for(Person person : persons){
			System.out.println(person);
		}
		
		
	}

}
