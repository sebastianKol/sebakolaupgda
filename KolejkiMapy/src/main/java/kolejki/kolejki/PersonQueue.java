package kolejki.kolejki;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Comparator;

public class PersonQueue {
	public static void main(String[] args) {

		Queue<Person> persons = new PriorityQueue<>(new Comparator<Person>(){
			public int compare(Person o1, Person o2){
				//  return 0;  // po wieku
				return o1.getName().compareTo(o2.getName());   // po nazwisku
				
			}
		});

		persons.offer(new Person("Stefan", 22));
		persons.offer(new Person("Ula", 33));
		persons.offer(new Person("Adam", 45));
		persons.offer(new Person("Tomasz", 33));
		
		

		while (!persons.isEmpty()) {
			System.out.println(persons.poll());

		}

	}

}
