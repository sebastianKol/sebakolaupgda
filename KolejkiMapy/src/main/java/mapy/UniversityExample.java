package mapy;

public class UniversityExample {

	public static void main(String[] args) {
	
		University university = new University();
		
		university.addStudent(11111, "Antoni", "Adamski");
		university.addStudent(22222, "Bob", "Budowniczy");
		university.addStudent(33333, "Cezary", "Cejrowski");
		university.addStudent(44444, "Damian", "Dyl");
		
		System.out.println(university.studentExist(11111)); //czy istnieje
		System.out.println(university.getStudent(22222));  // wypisz studenta
		System.out.println(university.studentsNumber());  // liczba studentow
		System.out.println("");
		university.showAll();  // wypisz wszytkich

	}

}
