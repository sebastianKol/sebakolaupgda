package mapy;

public class Student{
	
	private final int index;
	private final String name;
	private final String surname;
	
	
	public Student(int index, String name, String surname) {
		this.index = index;
		this.name =name;
		this.surname = surname;
		
	}


	public int getIndex() {
		return index;
	}


	public String getName() {
		return name;
	}


	public String getSurname() {
		return surname;
	}
	@Override
	public String toString(){
		return "("+index+","+","+name+","+surname+")";
	}
	
}