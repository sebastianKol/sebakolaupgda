package mapy;

import java.util.HashMap;
import java.util.Map;

public class MapsExamples {
	
	public static void main(String[] args) {
		
		Map<String, String> slownik = new HashMap<>();
		slownik.put("Kot","Cat");
		slownik.put("Pies","Dog");
		
		System.out.println("Tlumaczenie kota");
		System.out.println(slownik.get("Kot"));
		
		
		
		
		
		
		System.out.println("Klucze :");
		for (String key : slownik.keySet()){
			System.out.println(key);
		}
		
		System.out.println("Wartosci");
		for(String value : slownik.values()){
			System.out.println(value);
		}
		System.out.println("Zbior encji-par");
		for(Map.Entry<String,String> pair : slownik.entrySet()){
			System.out.println(pair.getKey()+pair.getValue());
		}
		
		slownik.remove("Pies");
		
		System.out.println(slownik.getOrDefault("Pies", "Brak tlumaczenia"));
		
		
		
	}

}
