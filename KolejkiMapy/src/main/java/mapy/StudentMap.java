package mapy;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class StudentMap {

	public static void main(String[] args) {
		Map<Integer,Student> studenci = new TreeMap<>();
		
		studenci.put(1111,new Student(1111,"Jan","Kowalski"));
		studenci.put(2222,new Student(2222,"Adam","Nowak"));
		studenci.put(3333,new Student(3333,"Ula","Miodek"));
		studenci.put(4444,new Student(4444,"Stefan","Taran"));

	
		System.out.println(studenci.containsKey(1111)); // czy zawiera studenta 111
		
		System.out.println(studenci.size()); 
		System.out.println(studenci); // wypisuje wszystkich
	}

}
