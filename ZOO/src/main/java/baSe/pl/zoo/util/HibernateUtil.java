package baSe.pl.zoo.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by RENT on 2017-07-12.
 */
public class HibernateUtil {
    private static final SessionFactory sessionFactory = buildSessionFactory();  // nie nadpiszemy tego pola
    private static SessionFactory buildSessionFactory(){   // singleton wykozystany w inny sposob
        return new Configuration().configure().buildSessionFactory();
    }
    public static SessionFactory getSessionFactory(){
        return sessionFactory;
    }
    public static Session openSession(){
        return  getSessionFactory().openSession();
    }
}
