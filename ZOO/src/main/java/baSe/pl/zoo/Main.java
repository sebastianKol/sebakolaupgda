package baSe.pl.zoo;

import baSe.pl.zoo.dao.AnimalDAO;
import baSe.pl.zoo.dao.AnimalFeedDAO;
import baSe.pl.zoo.dao.StaffDAO;
import baSe.pl.zoo.entity.Animal;
import baSe.pl.zoo.entity.AnimalFeed;
import baSe.pl.zoo.entity.Staff;
import baSe.pl.zoo.util.HibernateUtil;
import org.hibernate.*;
import org.hibernate.query.Query;
import org.hibernate.cfg.Configuration;

import javax.persistence.metamodel.EntityType;

import java.util.List;
import java.util.Map;

/**
 * Created by RENT on 2017-07-12.
 */
public class Main {
    public static void main(String[] args) { // musimy miec stworzana baze danych
//        // otworzenie sesjii Hibernate
//        //Session session = new Configuration().configure().buildSessionFactory().openSession();
//        // nie kozystamy z tego bo mamy hibernate util
//
//        Session session = HibernateUtil.openSession();
//        // begin create
//        Transaction t = session.beginTransaction();
//        Animal a1 = new Animal();  // konstruktor bez parametrow uzyty
//        a1.setName("Słoń");
//        session.save(a1);
//
//        session.save(new Animal("Zyrafa"));   // konstruktor z 1 parametrem
//        session.save(new Animal("Zolw"));
//
//        t.commit();
//        session.close();
//        // end - create
//
//        // session.close();
//// sesja powinna byc otwierana i zamykana dla kazdej transakcji
//// otwarata tez dla kazdej transakcji
//        // Session session = new Configuration().configure().buildSessionFactory().openSession();
//
//
//
//        //begin - retrive
//        session = HibernateUtil.openSession();  // dodane z hibernate utils
//        Animal animal = null;
//        t = session.beginTransaction();
//        String query = "from Animal where id= :id";
//        Query q = session.createQuery(query);
//        q.setParameter("id",1);
//        animal = (Animal) q.uniqueResult();
//        System.out.println(animal.getId()+"|"+animal.getName());
//        t.commit();
//        session.close();
//
//        // end retrive
//
//        //begin update
//        session = HibernateUtil.openSession();
//        t=session.beginTransaction();
//        animal.setName("Krokodyl");
//        session.update(animal);
//        t.commit();
//        session.close();
//        // end update
//
//
//
//        //begin delate
//        session = HibernateUtil.openSession();
//        t = session.beginTransaction();
//        Animal animalToDelate = session.load(Animal.class, new Integer(2));
//        session.delete(animalToDelate);
//        t.commit();
//        session.close();
//        //end delate
//
//        //begin retrive all  - pobranie calosci z tabeli do listy
////        session = HibernateUtil.openSession();
////        List<Animal> users = session.createQuery("from Animal ").list();  // zwraca jako liste gotowa metoda
////                                                                              //"Animal" nazwa encji-clasy nie nazwa tabeli- zapytania operuja na obiektach
////        for (Animal anim : users){
////            System.out.println(anim.getId()+"_"+anim.getName());
////        }
////        session.close();
//        //end retrive all


        //ANIMAL
        AnimalDAO animalDAO = new AnimalDAO();
        AnimalFeedDAO animalFeedDAO = new AnimalFeedDAO();

        Animal a = new Animal();
        a.setName("Słoń");

        AnimalFeed af = new AnimalFeed("Paweł", 8, a);

        animalFeedDAO.insert(af);
        animalFeedDAO.insert(new AnimalFeed("Karolina", 3, new Animal("Krokodyl")));
        animalFeedDAO.insert(new AnimalFeed("Michał", 5, a));


        System.out.println("Nakarmiono: " + animalFeedDAO.get(1).getAnimal().getName());



//        // BEGIN: insert
//        animalDAO.insert(a);
//        animalDAO.insert(new Animal("Orzeł"));
//        animalDAO.insert(new Animal("Gołąb"));
//
//        Animal slon = new Animal();
//
//        // BEGIN: retrieve
//        slon = animalDAO.get(1);
//        System.out.println("Pobrales id = " + slon.getId() + " " + slon.getName());
//
//        // BEGIN: update
//        slon.setName("PrzerobionySlon");
//        animalDAO.update(slon);
//
//        // BEGIN: delete
//        animalDAO.delete(1); // delete: gołąb
//
//        Animal secondAnimal = new Animal();
//        secondAnimal.setId(2);
//        animalDAO.delete(secondAnimal); // delete: orzeł
//
//        // BEGIN: retrieve all
//        for(Animal anim : animalDAO.get()) {
//            System.out.println(anim.getId() + ". " + anim.getName());
//
//        ///STAFF
//
//            //insert
//            StaffDAO staffDAO = new StaffDAO();
//            staffDAO.insert(new Staff("Tom","Kowalski",33));





    }
}