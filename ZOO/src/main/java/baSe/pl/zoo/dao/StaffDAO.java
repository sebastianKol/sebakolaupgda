package baSe.pl.zoo.dao;

import baSe.pl.zoo.entity.Animal;
import baSe.pl.zoo.entity.Staff;
import baSe.pl.zoo.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Created by RENT on 2017-07-12.
 */
public class StaffDAO implements AbstractDAO<Staff> {
    public boolean insert(Staff type) {

        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.save(type);
        t.commit();
        session.close();

        return true;
    }

    public boolean delete(Staff type) {

        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.load(Staff.class,new Integer(type.getId()));
        t.commit();
        if (this.get(type.getId())==null){
            return true;
        }
        session.close();

        return true;
    }

    public boolean delete(int id) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(this.get(id));
        t.commit();

        return true;
    }

    public boolean update(Staff type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.update(type);
        t.commit();
        session.close();

        return true;
    }

    public Staff get(int id) {
        Staff staff;
        Session session = HibernateUtil.openSession();
        staff = session.load(Staff.class,id);
        session.close();
        return staff;
    }

    public List<Staff> get() {
        Session session = HibernateUtil.openSession();
        List<Staff> staff;
        staff = session.createQuery("from staff").list();
        session.close();
        return staff;
    }
}
