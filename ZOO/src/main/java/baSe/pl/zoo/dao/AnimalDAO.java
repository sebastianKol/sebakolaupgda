package baSe.pl.zoo.dao;

import baSe.pl.zoo.entity.Animal;
import baSe.pl.zoo.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Created by RENT on 2017-07-12.
 */
public class AnimalDAO implements AbstractDAO<Animal> {
    public boolean insert(Animal type) {

            Session session = HibernateUtil.openSession();
            Transaction t = session.beginTransaction();
            session.save(type);   // konstruktor z 1 parametrem

            //int newID = Integer.valueOf(session.save(type)+"");
        //System.out.println("umiescilem rekord o ID ="+newID);
            t.commit();
            session.close();

        return true;
    }

    public boolean delete(Animal type) {

            Session session = HibernateUtil.openSession();
            Transaction t = session.beginTransaction();
            session.load(Animal.class,new Integer(type.getId()));
            t.commit();
        if (this.get(type.getId())==null){                /// sposob na ustawienie flagi - probojemy pobrac skasowany element jak  jest null to flaga true
            return true;
        }
            session.close();

        return true;
    }

    public boolean delete(int id) {
            Session session = HibernateUtil.openSession();
            Transaction t = session.beginTransaction();
            session.delete(this.get(id));
            t.commit();

        return true;
    }

    public boolean update(Animal type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.update(type);
        t.commit();
        session.close();

        return true;
    }

    public Animal get(int id) {
        Animal animal;
        Session session = HibernateUtil.openSession();
        animal = session.load(Animal.class,id);
        session.close();
        return animal;
    }

    public List<Animal> get() {
        Session session = HibernateUtil.openSession();
        List<Animal> animals;
        animals = session.createQuery("from Animal").list();
        session.close();
        return animals;
    }
}
