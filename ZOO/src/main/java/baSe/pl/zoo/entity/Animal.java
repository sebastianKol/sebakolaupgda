package baSe.pl.zoo.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by RENT on 2017-07-12.
 */
@Entity
@Table(name="animal")  // jesli nie uzyjemy parametru to nazwa tabeli bedzie jak nazwa klasy - nazwa kolumny jak nazwa pola
public class Animal {

    @Id                                                            // oznaczamy jako id dla bazy danych
    @GeneratedValue(strategy = GenerationType.IDENTITY)         //   auto increment info dla bazy danych
    @Column(name="id")
    private int id;
    @Column(name="name")
    private String name;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AnimalFeed> animalFeedSet = new LinkedList<AnimalFeed>();
    // jedno karmienie karmi tylko 1 zwierze

    public List<AnimalFeed> getAnimalFeedSet() {
        return animalFeedSet;
    }

    public Animal setAnimalFeedSet(List<AnimalFeed> animalFeedSet) {
        this.animalFeedSet = animalFeedSet;
        return this;
    }

    public Animal() {
    }

    public Animal(String name) {
        this.name = name;
    }

    public Animal(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public Animal setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Animal setName(String name) {
        this.name = name;
        return this;
    }
}
