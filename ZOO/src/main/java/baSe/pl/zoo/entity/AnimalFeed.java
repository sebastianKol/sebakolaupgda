package baSe.pl.zoo.entity;

import javax.persistence.*;

/**
 * Created by RENT on 2017-07-13.
 */
@Entity
public class AnimalFeed {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column
    private String who;

    @Column
    private  int amount;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Animal animal;
    // Animal moze byc karmiony wiele razy

    public AnimalFeed(String who, int amount, Animal animal) {
        this.who = who;
        this.amount = amount;
        this.animal = animal;
    }

    public AnimalFeed() {
    }

    public int getId() {
        return id;
    }

    public AnimalFeed setId(int id) {
        this.id = id;
        return this;
    }

    public String getWho() {
        return who;
    }

    public AnimalFeed setWho(String who) {
        this.who = who;
        return this;
    }

    public int getAmount() {
        return amount;
    }

    public AnimalFeed setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public Animal getAnimal() {
        return animal;
    }

    public AnimalFeed setAnimal(Animal animal) {
        this.animal = animal;
        return this;
    }

    @Override
    public String toString() {
        return "AnimalFeed{" +
                "id=" + id +
                ", who='" + who + '\'' +
                ", amount=" + amount +
                ", animal=" + animal +
                '}';
    }
}

