package enMovie;

import javax.persistence.*;

/**
 * Created by Mori on 2017-07-12.
 */
@Entity
@Table(name="movie")
public class Movie {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY )
    @Column(name="id")
    private int id;
    @Column(name = "name")
    private String name;

    public Movie(String name) {
        this.name = name;
    }

    public Movie() {
    }

    public int getId() {
        return id;
    }

    public Movie setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Movie setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return name;
    }
}
