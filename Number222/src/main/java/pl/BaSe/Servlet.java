package pl.BaSe;

import enMovie.Movie;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by RENT on 2017-07-24.
 */


@WebServlet(name = "Servlet")
public class Servlet extends HttpServlet {
    SessionFactory sf = new Configuration().configure().buildSessionFactory();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if(action.equals("add")) {
            String name = request.getParameter("movieName");
            Session session = sf.openSession();
            Movie m = new Movie(name);
            Transaction t = session.beginTransaction();
            session.save(m);
            t.commit();
            session.close();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Session session = sf.openSession();
        String action = request.getParameter("action");

        if (action.equals("show")) {

            Transaction t = session.beginTransaction();
            List<Movie> movies = session.createQuery("from Movie where id > 0").list();
            t.commit();
            session.close();
            request.setAttribute("movies", movies);
            request.getRequestDispatcher("movies.jsp").forward(request, response);

        }else if (action.equals("delete")){
            int id = Integer.parseInt(request.getParameter("id"));
            Movie movie = new Movie();
            movie.setId(id);
            Transaction t = session.beginTransaction();
            session.delete(movie);
            t.commit();
            request.getRequestDispatcher("Servlet?action=show").forward(request,response);


        }else if (action.equals("edit")){
        int id = Integer.parseInt(request.getParameter("id"));
        Movie movie = session.get(Movie.class,id);
        request.setAttribute("movie",movie);
        request.getRequestDispatcher("edit.jsp")

    }
        session.close();


    }
}