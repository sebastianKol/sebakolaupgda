<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<c:import url="header.jsp"/>
<section>

    <h2>movies - przegladaj</h2>




    <section>
        <c:forEach items="${movies}" var="movie">
            <p><c:out value="${movie.id}" />.<c:out value="${movie.name}" /></p>
        </c:forEach>
    </section>

    <table width="100%" border="1">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nazwa</th>
            <th>Akcje</th>
        </tr>
        </thead>
        <tbody>
                <section>
                <c:forEach items="${movies}" var="movie">
                     <tr>
                             <td><c:out value="${movie.id}" /></td>
                             <td><c:out value="${movie.name}" /></td>
                             <td><a href="/Servlet?action=edit&id=<c:out value="${movie.id}" />">Edytuj</a>&bull;<a href="/Servlet?action=delete&id=<c:out value="${movie.id}" />">Usun</a> </td>
                     </tr>
                </c:forEach>
                </section>

        </tbody>
    </table>



</section>

<c:import url="footer.jsp"/>