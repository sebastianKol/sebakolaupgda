package urzad.pl.org;

/**
 * Created by RENT on 2017-06-29.
 */
public class Main {
    public static void main(String[] args) {
        Department department = new Department();

        department.addOffice();

    }
}



//    Stwórz system obsługi. Zakładamy istnienie urzędu w którym znajdują się pokoje, w których można załatwić naszą sprawę.
// Klient powinien mieć do dyspozycji linię poleceń, która przyjmuje ich rejestracje.
//        1. Zaczniemy od stworzenia prostego enuma, który będzie reprezentować typ sprawy którą klient chce załatwić w urzędzie.
// Dostępne typy to REGISTER (rejestracja), UNREGISTER(wyrejestrowanie), CHECK_STATUS(stan klienta)
//        2. Stwórzmy klasę Client. Każdy klient posiada typ sprawy który musi podać oraz swój pesel, dlatego:
//        - stwórz konstruktor w klasie klient, która jako parametr przyjmuje typ sprawy oraz pesel i ustawia wartości tych pól w klasie.
//        - stwórz gettery oraz settery tych pól.
//        3. Stwórz klasę Office która reprezentuje pojedyncze biuro, w którym będą obsługiwani klienci.
//        - w klasie office dodaj metodę handle która obsługuje klienta. Parametrem tej metody będzie klient.
//        - w metodzie ma się pojawiać komunikat w formacie: "Klient pesel: {pesel} załatwia sprawę {typ_sprawy}"
//        4. Stwórz klasę Department która posiada kolekcję (możesz użyć listy lub mapy) biur.
//        - dodaj metodę addOffice która jako parametr przyjmuje biuro.
//        - dodaj metodę getOffice która służy do pobierania biura. Jeśli użyłeś/łaś listy to parametrem tej metody powinien być indeks,
// natomiast jeśli użyto mapy, to parametrem powinien być typ klucza tej mapy.
//        5. Stwórz maina. W mainie stwórz department, dodaj do niego dwa biura. Zweryfikuj działanie metod dodania i pobrania biura.
// Następnie stwórz obsługiwanie tej funkcjonalności z linii poleceń/konsoli. W konsoli powinniśmy mieć możliwość wpisania "dodaj_biuro"
// które spowoduje dodanie nowego biura.
//        -- Jeśli użyto mapy, to zaleca się dopisanie nazwy po "dodaj_biuro" która będzie kluczem, po którym będziemy odnajdować biura.
// Zamysł jest taki, aby - jeśli używamy mapy, to każde biuro ma swoją nazwę i jest po tej nazwie dodawane, a następnie wyszukiwane.
//        6. Dodaj w mainie czytanie ze scannera nowego klienta:
//        obsluga {PESEL} {TYP_SPRAWY} {ID_POKOJU}
//        która tworzy nowego klienta, a następnie wykorzystuje klasę department aby pobrać pokój identyfikowany przez ID_POKOJU i
// obsługuje go metodą handle.
