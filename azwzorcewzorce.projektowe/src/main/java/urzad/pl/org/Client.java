package urzad.pl.org;

/**
 * Created by RENT on 2017-06-29.
 */
public class Client {
    String caseType;
    int pesel;

    public Client(String caseType, int pesel) {
        this.caseType = caseType;
        this.pesel = pesel;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public int getPesel() {
        return pesel;
    }

    public void setPesel(int pesel) {
        this.pesel = pesel;
    }

    @Override
    public String toString() {
        return "Client{" +
                "caseType='" + caseType + '\'' +
                ", pesel=" + pesel +
                '}';
    }
}




 //2. Stwórzmy klasę Client. Każdy klient posiada typ sprawy który musi podać oraz swój pesel, dlatego:
//        - stwórz konstruktor w klasie klient, która jako parametr przyjmuje typ sprawy oraz pesel i ustawia wartości tych pól w klasie.
//        - stwórz gettery oraz settery tych pól.