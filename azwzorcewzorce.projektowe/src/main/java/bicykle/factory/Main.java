package bicykle.factory;

/**
 * Created by RENT on 2017-06-29.
 */
public class Main {
    public static void main(String[] args) {

            BikeFactory factory = new BikeFactory();
            Bike b = factory.create5GearKrossBike();
            Bike b2 = new Bike("awdf", 1, 2, BIKE_TYPE.TANDEM);
            Bike b3 = factory.create6GearMeridaBike();
        System.out.println(b);
        }


    }




//    Należy stworzyć aplikację symulującą fabrykę rowerów. Klasa Bike: ma markę, ilość miejsc, ilość przerzutek, oraz typ rowera (bicycle, tandem).
//        BIKE_TYPE: BICYCLE, TANDEM
//        Stwórz i przetestuj fabrykę abstrakcyjną (BikeFactory) która pozwala na tworzenie:
//        Rowerów jednoosbowych, marki KROSS, które mają 5 przerzutek
//        Rowerów jednoosbowych, marki MERIDA, które mają 6 przerzutek
//        Tandemów, marki INIANA, które mają 3 przerzutki
//        Rowerów jednoosbowych, marki FELT, które mają 6 przerzutek
//        Tandemów, marki GOETZE, które mają jedną przerzutkę

