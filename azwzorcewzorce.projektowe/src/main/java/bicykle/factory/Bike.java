package bicykle.factory;

/**
 * Created by RENT on 2017-06-29.
 */
 class Bike {
    private String bikeCompanyName;
    private int seats;
    private int switches;
    private BIKE_TYPE type;

    public Bike(String bikeCompanyName, int seats, int switches, BIKE_TYPE type) {
        this.bikeCompanyName = bikeCompanyName;
        this.seats = seats;
        this.switches = switches;
        this.type = type;
    }

    public String getBikeCompanyName() {
        return bikeCompanyName;
    }

    public void setBikeCompanyName(String bikeCompanyName) {
        this.bikeCompanyName = bikeCompanyName;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public int getSwitches() {
        return switches;
    }

    public void setSwitches(int switches) {
        this.switches = switches;
    }

    public BIKE_TYPE getType() {
        return type;
    }

    public void setType(BIKE_TYPE type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Bike{" +
                "bikeCompanyName='" + bikeCompanyName + '\'' +
                ", seats=" + seats +
                ", switches=" + switches +
                ", type=" + type +
                '}';
    }
}


