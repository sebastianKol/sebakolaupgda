package screenRecorder.org;

import java.util.Scanner;

/**
 * Created by RENT on 2017-06-29.
 */
public class Main {
    public static void main(String[] args) {
        ScreenRecorder screenRecorder = new ScreenRecorder();


        // Extension.values(); // wypisanie wartosci w enuma - w petli z iteratorem
        boolean working = true;
        while (working) {
            menuList();
            int select;
            try {
                select = getSelect();
                switch (select) {
                    case 1:
                        Extension extension = UserSelectExtension();
                        Codec codec = UserSelectCodec();
                        Resolution resolution = UserSelectResolution();
                        Profile profile = new Profile(codec, extension, resolution);
                        screenRecorder.addProfile(profile);
                        break;
                    case 2:
                        System.out.println("Lista dostepnych profili");
                        screenRecorder.listProfiles();
                        System.out.println("");
                        break;
                    case 3:
                        screenRecorder.startRecording();
                        break;
                    case 4:
                        screenRecorder.stopRecording();
                        break;
                    case 5:
                        int selectedProfile;
                        System.out.println("Wybierz profil ");
                        Scanner scanner = new Scanner(System.in);
                        selectedProfile = scanner.nextInt();
                        screenRecorder.setProfile(selectedProfile);
                        break;
                    case 6:
                        working =false;
                        break;
                }
            } catch (Exception e) {
                //print the exception
                System.out.println(e.getMessage());
            }
        }

    }

    private static Extension UserSelectExtension() {
        System.out.println("Extension Type");
        for (int i = 0; i < Extension.values().length; i++) {
            System.out.println((i + 1) + " " + Extension.values()[i]);
        }
        Scanner scanner = new Scanner(System.in);
        int ext = scanner.nextInt();
        switch (ext) {
            case 1:
                return Extension.values()[0];
            case 2:
                return Extension.values()[1];
            case 3:
                return Extension.values()[2];
            case 4:
                return Extension.values()[3];
        }
        return null;
    }

    private static Codec UserSelectCodec() {
        System.out.println("Codec Type");
        for (int i = 0; i < Codec.values().length; i++) {
            System.out.println((i + 1) + " " + Codec.values()[i]);
        }
        Scanner scanner = new Scanner(System.in);
        int ext = scanner.nextInt();
        switch (ext) {
            case 1:
                return Codec.values()[0];
            case 2:
                return Codec.values()[1];
            case 3:
                return Codec.values()[2];
            case 4:
                return Codec.values()[3];
        }
        return null;
    }

    private static Resolution UserSelectResolution() {
        System.out.println("Resolution");
        for (int i = 0; i < Resolution.values().length; i++) {
            System.out.println((i + 1) + " " + Resolution.values()[i]);
        }
        Scanner scanner = new Scanner(System.in);
        int ext = scanner.nextInt();
        switch (ext) {
            case 1:
                return Resolution.values()[0];
            case 2:
                return Resolution.values()[1];
        }
        return null;
    }


    private static void menuList() {
        System.out.println("1. Add ");
        System.out.println("2. List");
        System.out.println("3. Start Recording");
        System.out.println("4. Stop Recording");
        System.out.println("5. Set");
        System.out.println("6. Exit");
    }

    private static int getSelect() {
        int select;
        Scanner scanner = new Scanner(System.in);
        select = scanner.nextInt();
        return select;
    }
}


//    Mamy klasę ScreenRecorder oraz Profile.
//        add  - powoduje dodanie nowego profilu.
//        list - listuje wszystkie profile (numery przy opisach profili)
//        startRecording - rozpoczyna nagrywanie (tylko gdy jest ustawiony profil)
//        stopRecording - zatrzymuje nagrywanie (tylko gdy było rozpoczęte)
//        setProfile - powoduje ustawienie aktualnego profilu (parametrem jest numer profilu z listy) - zabezpiecz metodę przed nieistniejącymi profilami
//        Przykłady użycia komend:
//        1.add h264 1920x1080 mp4^dodaj   ^codec ^resol  ^ extension
//        2.list^listuje
//        3.startRecording
//        4.stopRecording
//        5.setProfile 1^ustaw     ^nr_profilu
//ScreenRecorder - klasa którą zarządzamy.
//        Profile - klasa którą ustawiamy/tworzymy.
//        ScreenRecorder powinien posiadać listę profili, oraz aktualnie ustawiony profil (pole w klasie).
//        Profile - klasa którą tworzymy i dodajemy.
//        ScreenRecorder powinien mieć metodę
//        - addProfile(Profile nowyProfil) - dodaje profil do listy
//        - setProfile(int numerProfilu) - ustawia profil (pole) na wybrany z listy
//        - listProfiles() - wylistowuje profile
//        - startRecording() - rozpoczyna nagrywanie (tylko gdy jeszcze nie nagrywamy)
//        - stopRecording() - zatrzymuje nagrywanie (tylko gdy już nagrywamy)