package mutable.types;

import org.junit.Test;

public class IntigerTest {

	@Test
	public void test() {
		
		
		Integer a = 10;
		Integer b = a;
		a = 5;

		assert a == 5;
		assert b == 10;
		
		a++;

		assert a == 6;
		assert b == 10;
		
		Double c = 2.5;
		Double d = 2.16;
		c=d;
		
	
		c=c+1;
		
		System.out.println(c);
		
		assert c == 3.16;
		assert  d == 2.16;
		
		
	
		
		
	}
	
	

}
