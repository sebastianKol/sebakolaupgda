package mutable.types;

public class MutableInteger {

	private int value;

	public MutableInteger() {
	}

	public MutableInteger(int value) {
		this.value = value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	public void increment() {
		value++;
	}
	
}
